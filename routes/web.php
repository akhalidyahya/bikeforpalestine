<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Response as Response;

Route::get('/','Frontend\PageController@index')->name('home');

Route::get('/email/racepack/{id}', function ($id) {
    $user = \App\User::with('expedition')->where('id',$id)->first();

    return (new \App\Notifications\RacepackSent($user))
                ->toMail($user->email);
});

Route::get('/merchandise','Frontend\PageController@merchandise')->name('merchandise');

Route::get('/donasi','Frontend\PageController@donation')->name('donation');
Route::post('/donasi','Frontend\PageController@saveDonation')->name('donation.save');

Route::get('/leaderboard','Frontend\PageController@leaderboard')->name('leaderboard');
Route::get('/leaderboard/data','Frontend\PageController@leaderboardData')->name('fe.leaderboard.data');


Route::get('/login','AuthController@loginIndex')->name('login.index');
Route::get('/reset-password','AuthController@resetPasswordIndex')->name('resetPassword.index');
Route::post('/reset-password/send','AuthController@resetPasswordSendForm')->name('resetPassword.sendForm');
Route::get('/reset-password/{token}','AuthController@resetPasswordDetail')->name('resetPassword.detail');
Route::post('/reset-password','AuthController@resetPasswordProcess')->name('resetPassword.process');
Route::get('/register','AuthController@registerIndex')->name('register.index');
Route::get('/activation/{user_id}/{token}','AuthController@activation')->name('activation');
Route::get('/sendActivation/{id}','AuthController@sendActivation')->name('activation.send');
Route::post('/register','AuthController@saveData')->name('register.save');
Route::post('/login','AuthController@loginProcess')->name('login.process');
Route::post('/logout','AuthController@logout')->name('logout');

//bib
Route::get('/bib/{token}','User\ProfileController@generateBib')->name('user.bib.generate');

//sertif
Route::get('/sertifikat/{token}','User\ProfileController@generateSertifikat')->name('user.sertif.generate');

// Strava Webhook
// Route::post('/webhook', 'StravaController@storeEvent')->name('strava.storeEvent');
Route::post('/webhook', function(){
    return response('EVENT_RECEIVED', 200);
})->name('strava.storeEvent');

Route::get('/webhook', function (Request $request) {
    $mode = $request->query('hub_mode'); // hub.mode
    $token = $request->query('hub_verify_token'); // hub.verify_token
    $challenge = $request->query('hub_challenge'); // hub.challenge

    // Checks if a token and mode is in the query string of the request
    if ($mode && $token) {
        // Verifies that the mode and token sent are valid
        if ($mode === 'subscribe' && $token === config('services.strava.webhook_verify_token')) {
            // Responds with the challenge token from the request
            return response()->json(['hub.challenge' => $challenge]);
        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            return response('', Response::HTTP_FORBIDDEN);
        }
    }

    return response('', Response::HTTP_FORBIDDEN);

    // return app(StravaWebhookService::class)->validate($mode, $token, $challenge);
});

Route::get('/reload-captcha','AuthController@reloadCaptcha')->name('captcha.reload');
Route::get('/deliveryInformationTemp','ParticipantController@deliveryInformationTemp')->name('deliveryInformationTemp');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

    Route::group(['prefix' => 'admin','middleware'=>['admin']], function () {
        //peserta
        Route::get('/participants-data', 'ParticipantController@indexData')->name('participants.data.index');
        Route::get('/participants-payment', 'ParticipantController@indexPayment')->name('participants.payment.index');
        Route::get('/participants/getAccountById', 'ParticipantController@getAccountById')->name('participants.getAccountById');
        Route::post('/participants', 'ParticipantController@saveAccount')->name('participants.saveAccount');
        Route::delete('/participants', 'ParticipantController@deleteParticipant')->name('participants.delete');
        Route::post('/participantsChangeStatus/{user_id}', 'ParticipantController@changeStatus')->name('participants.changeStatus');
        Route::get('/participants/accountDataTable', 'ParticipantController@accountDataTable')->name('participants.accountDataTable');
        Route::get('/participants/profileDataTable', 'ProfileController@profileDataTable')->name('participants.profileDataTable');
        Route::get('/participants/deliveryInformation', 'ParticipantController@deliveryInformation')->name('participants.deliveryInformation');
        Route::post('/sendReminder/{id}','ParticipantController@sendReminderEmail')->name('reminder.send');
        Route::post('/participantsSendResi', 'ParticipantController@sendResi')->name('participants.sendResi');
        Route::get('/export-delivery','ParticipantController@exportDelivery')->name('export.delivery');
        Route::get('/export-profile','ParticipantController@exportProfile')->name('export.profile');

        //profile
        Route::get('/profile/getProfileByAccountId/{account_id}', 'ProfileController@getProfileByAccountId')->name('profile.getProfileByAccountId');

        //result
        Route::get('/results', 'ResultController@index')->name('result.index');
        Route::get('/results/dataTable', 'ResultController@dataTableActivity')->name('result.dataTableActivity');
        Route::post('/results/changeStatus/{id}', 'ResultController@changeStatus')->name('result.changeStatus');

        //leaderboard
        Route::get('/leaderboard', 'LeaderboardController@index')->name('leaderboard.index');
        Route::get('/leaderboard/dataTable', 'LeaderboardController@dataTableActivity')->name('leaderboard.dataTableActivity');

        //summary
        Route::get('/summary', 'SummaryController@index')->name('summary.index');

        //Config
        Route::get('/config', 'ConfigController@index')->name('config.index');
        Route::post('/config/faq', 'ConfigController@saveFaq')->name('config.saveFaq');

        // Complaints
        Route::get('/complaint', 'ComplaintController@index')->name('admin.complaint.index');
        Route::get('/complaint/dataTable', 'ComplaintController@complaintDataTable')->name('admin.complaint.dataTable');
        Route::post('/complaint/changeStatus', 'ComplaintController@changeStatus')->name('admin.complaint.changeStatus');
    });

    Route::group(['prefix' => 'user','middleware'=>['user']], function () {
        Route::get('/kilas-balik', 'DashboardController@recapIndex')->name('kilasBalik.index');

        Route::post('/pembayaranBukti', 'User\PembayaranController@uploadBukti')->name('pembayaran.bukti');

        //profile
        Route::get('/profile', 'User\ProfileController@index')->name('user.profile.index');
        Route::post('/save', 'User\ProfileController@save')->name('user.profile.save');

        Route::get('/submit', 'User\ResultController@index')->name('user.submit.index');
        Route::post('/submit', 'User\ResultController@save')->name('user.submit.save');
        Route::get('/submit/dataTable', 'User\ResultController@dataTableActivity')->name('user.submit.dataTableActivity');
        // Route::get('/results/dataTable', 'User\ResultController@dataTableActivity')->name('result.dataTableActivity');

        //leaderboard
        Route::get('/leaderboard', 'User\LeaderboardController@index')->name('user.leaderboard.index');
        Route::get('/leaderboard/dataTable', 'LeaderboardController@dataTableActivity')->name('user.leaderboard.dataTableActivity');

        // Connect to Strava
        Route::get('auth-to-strava','StravaController@authToStrava')->name('user.strava.auth');
        Route::get('auth-to-disconnect','StravaController@disconnectFromStrava')->name('user.strava.disconnect');
        Route::get('sync-to-strava','StravaController@syncToStrava')->name('user.strava.sync');

        // Strava Activities
        Route::get('/history', 'User\HistoriesController@index')->name('user.history.index');
        Route::get('/history/dataTable', 'User\HistoriesController@historyTableActivity')->name('user.history.historyTableActivity');
        // Complaints
        Route::get('/complaint/dataTable', 'User\HistoriesController@complaintDataTable')->name('user.history.complaintDataTable');
        Route::post('/complaint', 'User\HistoriesController@storeComplaint')->name('user.history.save');
    });
});
