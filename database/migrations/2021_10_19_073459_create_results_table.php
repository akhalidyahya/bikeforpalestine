<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('activity_date')->nulllable();
            $table->string('distance',10)->nulllable();
            $table->time('duration')->nulllable();
            $table->string('screenshoot')->nulllable();
            $table->string('link')->nulllable();
            $table->string('instagram')->nulllable();
            $table->text('notes')->nulllable();
            $table->string('status',50)->nulllable(); // NEW / APPROVED / REJECT
            $table->integer('profile_id')->nulllable();
            $table->timestamps();
            $table->index('profile_id');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
