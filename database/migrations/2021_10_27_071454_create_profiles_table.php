<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('gender',10)->nullable();
            $table->string('age',5)->nullable();
            $table->string('phone_number',20)->nullable();
            $table->string('bib_number',20)->nullable();
            $table->string('total_distance',10)->nullable();
            $table->string('total_time',10)->nullable();
            $table->string('jersey',10)->nullable();
            $table->string('registration_category',10)->nullable();
            $table->integer('user_id')->nulllable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
