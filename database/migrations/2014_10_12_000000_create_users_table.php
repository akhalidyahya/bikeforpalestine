<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('token')->unique();
            $table->string('registration_category',25)->nullable(); //RUN / BIKE
            $table->string('registration_type',25)->nullable(); // SINGLE / DOUBLE / FAMILY
            $table->string('status_payment',25)->nullable(); //PAID / UNPAID / ON_CONFIRMATION / REJECT
            $table->string('proof_of_payment')->nullable(); //image
            $table->timestamp('payment_date')->nullable();
            $table->text('payment_remark')->nullable();
            $table->string('include_jersey',25)->nullable(); //YES / NO
            $table->string('role'); // ADMIN / USER
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->index('role');
            $table->index('registration_category');
            $table->index('registration_type');
            $table->index('status_payment');
            $table->index('include_jersey');
            $table->index('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
