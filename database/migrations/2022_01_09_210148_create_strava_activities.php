<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStravaActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strava_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aspect_type')->nullable();
            $table->string('event_time')->nullable();
            $table->string('object_id')->nullable();
            $table->string('object_type')->nullable();
            $table->string('owner_id')->nullable();
            $table->string('subscription_id')->nullable();
            $table->string('updates')->nullable();
            $table->timestamps();
            $table->index('aspect_type');
            $table->index('object_type');
            $table->index('owner_id');
            $table->index('object_id');
            $table->index('subscription_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strava_activities');
    }
}
