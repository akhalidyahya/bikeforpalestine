<?php

use App\Profile;
use App\User;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'              => 'Admin',
            'email'             => 'admin@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'          => bcrypt('1234'),
            'role'              => 'ADMIN',
            'status_payment'    => 'PAID',
            'token'             => Str::random(50)
        ]);
    }
}
