<?php

use App\Expedition;
use App\Profile;
use App\User;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ExpeditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Expedition::create([
            'expedition_name'           => 'Anteraja',
            'expedition_tracking_url'   => 'https://anteraja.id/tracking',
        ]);
        Expedition::create([
            'expedition_name'           => 'Wahana Express',
            'expedition_tracking_url'   => 'https://www.wahana.com/#lacakkiriman',
        ]);
        Expedition::create([
            'expedition_name'           => 'Tiki',
            'expedition_tracking_url'   => 'https://www.tiki.id/id/tracking',
        ]);
    }
}
