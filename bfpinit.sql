-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_tryout.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_tryout.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table db_tryout.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_tryout.migrations: ~8 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(115, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(116, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(117, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(118, '2021_07_26_152851_create_tryouts_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(119, '2021_07_26_152929_create_questions_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(120, '2021_07_26_153216_create_answers_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(121, '2021_07_26_153331_create_results_table', 1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(122, '2021_08_01_134646_create_files_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table db_tryout.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_tryout.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table db_tryout.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_pendaftaran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pembayaran` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_token_unique` (`token`),
  KEY `users_role_index` (`role`),
  KEY `users_jurusan_index` (`jurusan`),
  KEY `users_token_user_id_index` (`token`,`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_tryout.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `token`, `ttl`, `role`, `jurusan`, `jenis_kelamin`, `nomor_pendaftaran`, `status_pembayaran`, `bukti_pembayaran`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$vQZw3nlw9HX/ow3FkkjZVe6WI0H08/urR1B6W2l3TuzlbFBC/uixO', 'bRUDr2NVHWwYH2BK6l3rqOiiYkx17xA0PqM6qWP3lPyhElWMn5', '-', 'ADMIN', '-', 'MALE', '-', 'PAID', '-', NULL, '2021-07-28 07:45:36', '2021-07-28 07:45:36', NULL);
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `token`, `ttl`, `role`, `jurusan`, `jenis_kelamin`, `nomor_pendaftaran`, `status_pembayaran`, `bukti_pembayaran`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 'Abdullah Khalid Yahya', 'user@gmail.com', '2021-07-31 01:48:41', '$2y$10$bG77EMHCDEj4EIQ3Lei38eZMl1KCFt2GvP.x3vhdBiNIyVYREMhYi', 'Jr9S46hlNuSqtEGee5sacaHNKcLvAoS9Y7WQGYDSJH9XjHhKLO', '-', 'USER', 'DIII AKUNTANSI', 'MALE', '20210728074536', 'PAID', 'public/storage/registration/1627681877.jpg', NULL, '2021-07-28 07:45:36', '2021-07-31 04:51:17', NULL);
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `token`, `ttl`, `role`, `jurusan`, `jenis_kelamin`, `nomor_pendaftaran`, `status_pembayaran`, `bukti_pembayaran`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(3, 'Zaid Hilmi', 'user2@gmail.com', NULL, '$2y$10$5bnN3PyLwN8/FlmDIcljFeP9dVKZ7AelSBJXE4KoQWnrGx.UiQizO', 'lcNCy420R3x7B3GSjp5RuEKw4uBIWcL3CPVi9Hyh0U0sdFcD1b', '-', 'USER', 'DIII PAJAK', 'FEMALE', '20210728074537', 'UNPAID', '', NULL, '2021-07-28 07:45:37', '2021-07-31 02:51:18', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
