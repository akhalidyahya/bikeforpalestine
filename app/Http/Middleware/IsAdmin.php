<?php

namespace App\Http\Middleware;

use App\Utilities\Constants;
use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user() &&  \Auth::user()->role == Constants::ROLE_ADMIN) {
            return $next($request);
       }

       return redirect('dashboard')->with('error','You have not admin access');
    }
}
