<?php

namespace App\Http\Middleware;

use App\Utilities\Constants;
use Closure;

class IsPaidUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user() &&  \Auth::user()->status_payment == Constants::REGISTRATION_STATUS_PAID) {
            return $next($request);
        }

       return redirect('dashboard')->with('error','You are not paid user');
    }
}
