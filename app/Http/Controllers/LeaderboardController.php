<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use Carbon\CarbonInterval;
use DataTables;

class LeaderboardController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }
    
    public function index()
    {
        $data['sidebar'] = 'leaderboard';
        return view('pages/leaderboard', $data);
    }

    public function dataTableActivity(Request $request)
    {
        $datas = Profile::query();
        $datas->with('user');
        $datas->whereHas('user',function($q)use($request){
            $q->where('registration_category',$request->registration_category);
            $q->paidUser();
        });
        $datas->whereNotNull('total_distance');
        $datas->orderByRaw('CAST(total_distance AS float) DESC');
        $datas->orderByRaw('CAST(total_time AS float) ASC');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            // ->editColumn('name', function ($datas) {
            //     return GeneralHelper::censorName($datas->name);
            // })
            ->editColumn('gender',function($data){
                return Constants::GENDER_LIST[$data->gender];
            })
            ->escapeColumns([])->make(true);
    }
}
