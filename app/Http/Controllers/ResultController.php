<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Result;
use App\Services\CommonService;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use DataTables;
use Response;

class ResultController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }
    
    public function index()
    {
        $data['sidebar'] = 'hasil';
        return view('pages/results', $data);
    }

    public function dataTableActivity(Request $request)
    {
        $datas = Result::query();
        $datas->with('profile');
        $datas->whereHas('profile',function($query)use($request){
            $query->whereHas('user',function($q)use($request){
                $q->where('registration_category',$request->registration_category);
            });
        });

        if($request->has('search') && !empty($request->search)) {
            $datas->where(function($query)use($request){
                $query->whereHas('profile',function($q)use($request){
                    $q->where('name',$request->search);
                    $q->orWhere('bib_number',$request->search);
                });
            });
        }

        if($request->has('start_date') && !empty($request->start_date)) {
            $datas->where('activity_date','>=',$request->start_date);
        }
        if($request->has('end_date') && !empty($request->end_date)) {
            $datas->where('activity_date','<=',$request->end_date);
        }

        $datas->orderBy('id','desc');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('link',function($data){
                return '<a target="_blank" href="'.$data->link.'">Klik Disini</a>';
            })
            ->editColumn('status',function($data){
                $status = Constants::RESULT_STATUS_LIST[$data->status];
                if($data->status == Constants::RESULT_STATUS_APPROVED) {
                    $status = '<b class="text-success">'.$status.'</b>';
                }
                return $status;
            })
            ->addColumn('name',function($data){
                return $data->profile->name;
            })
            ->addColumn('bib_number',function($data){
                return $data->profile->bib_number;
            })
            ->addColumn('action', function ($data) {
                $actionBtn = 
                '<li><a onclick="changeStatus('.$data->id.' , \''. Constants::RESULT_STATUS_APPROVED .'\')" href="javascript:void(0);"><i class="fa fa-check"></i> Approve</a></li>
                <li><a onclick="reject('.$data->id.',\''.Constants::RESULT_STATUS_REJECT.'\')" href="javascript:void(0);"><i class="fa fa-times"></i> Reject</a></li>';
                if(in_array($data->status,[Constants::RESULT_STATUS_APPROVED,Constants::RESULT_STATUS_REJECT])) {
                    return $actionBtn = '-';
                }
                return '<div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Manage <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        '.$actionBtn.'
                                    </ul>
                                </div>';
            })
            ->escapeColumns([])->make(true);
    }

    public function changeStatus(Request $request,$id)
    {
        $response = [
            'success'   => false,
            'message'   => 'Status gagal diubah'
        ];
        $result = Result::where('id',$id)->first();
        $result->status = $request->status;

        if($request->status == Constants::RESULT_STATUS_APPROVED) {
            $profile = Profile::find($result->profile_id);
            $existing_total_distance = !empty($profile->total_distance) ? $profile->total_distance : 0;
            $existing_total_time     = !empty($profile->total_time) ? $profile->total_time : '00:00:00';
            $total_distance          = number_format((floatval($existing_total_distance) + floatval($result->distance)),2);
            $total_time              = '00:00:00';
            $total_time              = CommonService::addTime($existing_total_time, $result->duration);

            $profile->total_distance    = $total_distance;
            $profile->total_time        = $total_time;

            if($profile->registration_category == Constants::REGISTRATION_CATEGORY_BIKE && ($profile->total_distance >= Constants::BIKE_DISTANCE && empty($profile->finished_at))) {
                $profile->finished_at   = $profile->total_distance;
                $profile->finished_time = $profile->total_time;
            } elseif($profile->registration_category == Constants::REGISTRATION_CATEGORY_RUN && ($profile->total_distance >= Constants::RUN_DISTANCE && empty($profile->finished_at))) {
                $profile->finished_at   = $profile->total_distance;
                $profile->finished_time = $profile->total_time;
            }
            
            $profile->save();
        }

        if($request->status == Constants::RESULT_STATUS_REJECT) {
            $result->notes = $request->notes;
        }

        if ($result->save()) {
            return Response::json(array('success' => true, 'message' => 'Status berhasil diupdate'));
        } else {
            return Response::json($response);
        }
    }
}
