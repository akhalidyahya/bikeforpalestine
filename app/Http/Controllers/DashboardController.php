<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Services\CommonService;
use App\User;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }

    public function index(Request $request)
    {
        $data['sidebar']    =   'dashboard';

        if(\Auth::user()->role == Constants::ROLE_USER) {
            $data['profiles'] = Profile::with('user')->where('user_id',\Auth::user()->id)->orderBy('id','asc')->get();
            return view('user-pages/dashboard',$data);
        }

        $data['account_count'] = User::where('role',Constants::ROLE_USER)->count();
        $data['paid_account_count'] = User::paidUser()->where('role',Constants::ROLE_USER)->count();
        $collection = Profile::whereHas('user',function($q){
                            $q->paidUser()->where('role',Constants::ROLE_USER);
                        })->get();
        $data['profile_count']  = $collection->count();
        $data['data_submit']    = $collection->where('total_distance','>',0)->count();
        $userByRegistrationType = User::select('include_jersey','registration_type',DB::raw('count(registration_type) as registration_type_count'))
                                            ->groupBy('registration_type')
                                            ->groupBy('include_jersey')
                                            ->having('registration_type_count','>',0)
                                            ->paidUser()
                                            ->get();
        $data['total_revenue'] = 0;
        foreach ($userByRegistrationType as $user) {
            $data['total_revenue']+= ($user->registration_type_count * Constants::REGISTRATION_TYPE_PRICE_LIST[$user->registration_type][$user->include_jersey]);
        }
        
        return view('pages/dashboard',$data);
    }
    
    public function recapIndex() {
        $userId = \Auth::user()->id;
        $data['user'] = \Auth::user();
        return view('pages/kilas-balik',$data);
    }
}
