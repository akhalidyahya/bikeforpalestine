<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class ConfigController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }

    public function index()
    {
        $data['sidebar']    = 'config';
        $data['faq']        = json_decode(\Storage::disk('public')->get('json/faq.json'));

        return view('pages/config', $data);
    }

    public function saveFaq(Request $request) {
        $data = json_encode($request->faqs);
        Storage::disk('public')->put('faq.json', $data);
        return view('pages/config', $data);
    }
}
