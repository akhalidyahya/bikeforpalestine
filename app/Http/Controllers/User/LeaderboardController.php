<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use DB;

class LeaderboardController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
        $this->middleware('isPaidUser')->only('index');
    }
    
    public function index()
    {
        $data['sidebar'] = 'leaderboard';
        $data['topTen']  = $this->topTen(\Auth::user()->registration_category);
        $data['currentUser'] = Profile::where('user_id',\Auth::user()->id)->first();
        $data['currentRank'] = $this->currentUserRankData(\Auth::user()->registration_category,\Auth::user()->id);
        return view('user-pages/leaderboard', $data);
    }

    public function topTen($category)
    {
        $datas = Profile::query();
        $datas->with('user');
        $datas->whereHas('user',function($q)use($category){
            $q->where('registration_category',$category);
            $q->paidUser();
        });
        $datas->whereNotNull('total_distance');
        $datas->orderByRaw('CAST(total_distance AS float) DESC');
        $datas->orderByRaw('CAST(total_time AS float) ASC');
        $datas->limit(10);
        return $datas->get();
    }

    public function currentUserRankData($category,$userId) 
    {
        // Set variable row_number in mysql
        DB::statement('SET @row_number = 0');
        $data = DB::table(DB::raw('
                        (SELECT user_id,registration_category,total_distance,id,bib_number
                            FROM profiles 
                            WHERE registration_category = "'.$category.'"
                            AND deleted_at IS NULL 
                            AND bib_number IS NOT NULL
                            ORDER BY CAST(total_distance AS float) DESC) as profiles'
                ))
                ->selectRaw('(@row_number:=@row_number + 1) AS rank, user_id,total_distance,registration_category,id,bib_number')
                ->orderByRaw('CAST(total_distance AS float) DESC')
                ->get();
        @$collection = collect($data)->where('user_id',$userId)->pluck('rank');

        if(!empty($collection)) {
            return $collection[0];
        }
        
        return 0;
    }
}
