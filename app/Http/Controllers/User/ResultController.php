<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Profile;
use App\Result;
use App\Services\CommonService;
use App\StravaActivity;
use App\StravaAuth;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Log;
use Response;
use Strava;

class ResultController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
        $this->middleware('isPaidUser')->only('index');
    }
    
    public function index()
    {
        $data['sidebar']    =   'submit';
        $data['profiles']   =   Profile::where('user_id',\Auth::user()->id)->get();
        return view('user-pages/submit',$data);
    }

    public function dataTableActivity(Request $request)
    {
        $datas = Result::query();
        $datas->with('profile');
        $datas->whereHas('profile',function($query)use($request){
            $query->whereHas('user',function($q)use($request){
                $q->where('id',\Auth::user()->id);
                $q->where('registration_category',$request->registration_category);
            });
        });

        if($request->has('search') && !empty($request->search)) {
            $datas->where(function($query)use($request){
                $query->whereHas('profile',function($q)use($request){
                    $q->where('name',$request->search);
                    $q->orWhere('bib_number',$request->search);
                });
            });
        }

        if($request->has('start_date') && !empty($request->start_date)) {
            $datas->where('activity_date','>=',$request->start_date);
        }
        if($request->has('end_date') && !empty($request->end_date)) {
            $datas->where('activity_date','<=',$request->end_date);
        }

        if($request->has('profile_id') && !empty($request->profile_id)) {
            $datas->where('profile_id','=',$request->profile_id);
        }

        $datas->orderBy('id','desc');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('link',function($data){
                return '<a target="_blank" href="'.$data->link.'">Klik Disini</a>';
            })
            ->editColumn('status',function($data){
                $status = Constants::RESULT_STATUS_LIST[$data->status];
                if($data->status == Constants::RESULT_STATUS_APPROVED) {
                    $status = '<b class="text-success">'.$status.'</b>';
                }
                return $status;
            })
            ->addColumn('name',function($data){
                return $data->profile->name;
            })
            ->addColumn('bib_number',function($data){
                return $data->profile->bib_number;
            })
            ->addColumn('action', function ($data) {
                $actionBtn = 
                '<li><a onclick="changeStatus('.$data->id.' , \''. Constants::RESULT_STATUS_APPROVED .'\')" href="javascript:void(0);"><i class="fa fa-check"></i> Approve</a></li>
                <li><a onclick="changeStatus('.$data->id.',\''.Constants::RESULT_STATUS_REJECT.'\')" href="javascript:void(0);"><i class="fa fa-times"></i> Reject</a></li>';
                if($data->status == Constants::RESULT_STATUS_APPROVED) {
                    return $actionBtn = '-';
                }
                return '<div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Manage <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        '.$actionBtn.'
                                    </ul>
                                </div>';
            })
            ->escapeColumns([])->make(true);
    }

    public function save(Request $request)
    {
        
        $response = [
            'success'   => false,
            'message'   => 'Submit Data gagal!'
        ];

        if($request->method == 'add'){
            $result = new Result();
            $result->status = Constants::RESULT_STATUS_REQUEST_APPROVAL;
        } else {
            $result = Result::find($request->result_id);
        }

        $result->activity_date  = $request->activity_date;
        $result->distance       = str_replace(',','.',$request->distance);
        $result->duration       = $request->duration;
        $result->profile_id     = $request->profile_id;
        $result->link           = $request->link;
        
        if($result->save()){
            $response['success'] = true;
            $response['message'] = 'Submit Data berhasil!';
        }
        return response()->json($response);
    }

}
