<?php

namespace App\Http\Controllers\User;

use App\Complaint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use App\StravaActivity;
use App\User;
use App\Utilities\Constants;
use DataTables;

class HistoriesController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }

    public function index()
    {
        $data['sidebar']    =   'history';
        $data['profiles']   =   Profile::where('user_id',\Auth::user()->id)->get();
        return view('user-pages/histories',$data);
    }

    public function historyTableActivity(Request $request)
    {
        $datas = StravaActivity::query();
        $datas->with('user');
        // Must input id and category 
        $datas->whereHas('user',function($query)use($request){
            $query->where('id',\Auth::user()->id);
            $query->where('registration_category',$request->registration_category);
        });
        $datas->where('distance','>',0)->where('moving_time','>',0);
        // $datas->whereRaw('DATE("start_date_local") >= "'.Constants::START_DATE.'"');
        $datas->orderBy('start_date_local','desc');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('strava_activity_link',function($data){
                return '<a target="_blank" href="'.$data->strava_activity_link.'">Klik Disini</a>';
            })
            // ->editColumn('status',function($data){
            //     $status = Constants::RESULT_STATUS_LIST[$data->status];
            //     if($data->status == Constants::RESULT_STATUS_APPROVED) {
            //         $status = '<b class="text-success">'.$status.'</b>';
            //     }
            //     return $status;
            // })
            ->addColumn('name',function($data){
                return $data->user->name;
            })
            ->escapeColumns([])->make(true);
    }

    public function complaintDataTable(Request $request)
    {
        $datas = Complaint::query();
        $datas->with('user');
        // Must input id and category 
        $datas->whereHas('user',function($query)use($request){
            $query->where('id',\Auth::user()->id);
        });

        $datas->orderBy('id','desc');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('link',function($data){
                if(empty($data->link)) {
                    return 'Masukan URL activity yang benar, silahkan buat laporan ulang';
                }
                return '<a target="_blank" href="'.$data->link.'">'.$data->link.'</a>';
            })
            ->editColumn('status',function($data){
                $status = Constants::RESULT_STATUS_LIST[$data->status];
                if($data->status == Constants::RESULT_STATUS_APPROVED) {
                    $status = '<b class="text-success">'.$status.'</b>';
                } elseif ($data->status == Constants::RESULT_STATUS_REJECT) {
                    $status = '<b class="text-danger">'.$status.'</b>';
                }
                return $status;
            })
            ->addColumn('name',function($data){
                return $data->user->name;
            })
            ->escapeColumns([])->make(true);
    }

    public function storeComplaint(Request $request)
    {
        $string = str_replace('https://www.strava.com/activities/','',$request->link);
        $string_array = explode('?',$string);
        $activity_id = $string_array[0];
        $link = $request->link;

        $data = Complaint::create([
            'user_id' => $request->user_id,
            'profile_id' => Profile::where('user_id',$request->user_id)->first()->id,
            'status' => Constants::RESULT_STATUS_REQUEST_APPROVAL,
            'activity_id' => $activity_id,
            'link' => $link,
        ]);

        if($data) {
            return response()->json(['success'=>true,'message'=>'Laporan berhasil disimpan dan akan diproses oleh admin']);
        } else {
            return response()->json(['success'=>false,'message'=>'Laporan gagal disimpan']);
        }
    }
}
