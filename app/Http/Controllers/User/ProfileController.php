<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use App\Utilities\Constants;
use Response;
use PDF;
use DB;

class ProfileController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
        $this->middleware('isPaidUser')->only('index');
    }

    public function index()
    {
        $data['sidebar']    =   'profile';
        $data['profiles']   =   Profile::where('user_id',\Auth::user()->id)->get();
        return view('user-pages/profile',$data);
    }

    public function save(Request $request)
    {
        $profile = Profile::find($request->id);
        if(empty($profile)) return Response::json(array('success' => false, 'message' => 'Data Gagal disimpan, coba lagi'));
        $profile->name          = $request->name;
        $profile->email         = $request->email;
        $profile->address       = $request->address;
        $profile->gender        = $request->gender;
        $profile->age           = $request->age;
        $profile->phone_number  = $request->phone_number;
        $profile->jersey        = $request->jersey;
        
        if ($profile->save()) {
            if($request->json) {
                return response()->json(['success'=>true,'message'=>'Data '.$request->name.' Berhasil Disimpan']);
            }
            return redirect()->route('user.profile.index')->with('success','Data Profil '.$profile->name.' berhasil disimpan!');
        } else {
            if($request->json) {
                return response()->json(['success'=>false,'message'=>'Data '.$request->name.' Gagal Disimpan']);
            }
            return redirect()->route('user.profile.index')->with('message','Data Profil '.$profile->name.' gagal disimpan! Silahkan Hubungi admin');
        }
    }

    public function generateBib($token) {
        $array_token = explode('=',$token);
        $id          = $array_token[0];
        $uniqueToken = $array_token[1];
        $data = Profile::select(['bib_number','name'])->where('id',$id)->whereHas('user',function($q)use($uniqueToken){
            $q->where('token',$uniqueToken);
        })->first();
        if(empty($data)) return response()->json(['message'=>'404 BIB not found'],404);
        $pdf = PDF::loadview('pdf/e-bib',['data'=>$data]);
        $pdf->setPaper('A4', 'potrait');
    	return $pdf->stream('RFP-BIB-'.$data->bib_number.'.pdf');
    }

    public function generateSertifikat($token) {
        $array_token = explode('=',$token);
        $id          = $array_token[0];
        $uniqueToken = $array_token[1];
        $data = Profile::where('id',$id)->whereHas('user',function($q)use($uniqueToken){
            $q->where('token',$uniqueToken);
        })->first();
        if(empty($data)) return response()->json(['message'=>'404 User not found'],404);

        $data['currentRank'] = $this->currentUserRankData(\Auth::user()->registration_category,\Auth::user()->id);

        $pdf = PDF::loadview('pdf/sertifikat',['data'=>$data]);
        $pdf->setPaper('A4', 'landscape');
    	return $pdf->stream('RFP-SERTIFIKAT-'.$data->bib_number.'.pdf');
    }

    public function currentUserRankData($category,$userId) 
    {
        // Set variable row_number in mysql
        DB::statement('SET @row_number = 0');
        $data = DB::table(DB::raw('
                        (SELECT user_id,registration_category,total_distance,id,bib_number
                            FROM profiles 
                            WHERE registration_category = "'.$category.'"
                            AND deleted_at IS NULL 
                            AND bib_number IS NOT NULL
                            ORDER BY CAST(total_distance AS float) DESC) as profiles'
                ))
                ->selectRaw('(@row_number:=@row_number + 1) AS rank, user_id,total_distance,registration_category,id,bib_number')
                ->orderByRaw('CAST(total_distance AS float) DESC')
                ->get();
        @$collection = collect($data)->where('user_id',$userId)->pluck('rank');

        if(!empty($collection)) {
            return $collection[0];
        }
        
        return 0;
    }
}
