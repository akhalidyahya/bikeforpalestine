<?php

namespace App\Http\Controllers;

use App\Helper\GeneralHelper;
use App\Mail\PaymentConfirmed;
use App\Notifications\PaymentConfirmed as NotificationsPaymentConfirmed;
use App\Notifications\PaymentReject;
use App\Notifications\PaymentReminder;
use App\Notifications\RacepackSent;
use Response;
use DataTables;
use App\Profile;
use App\Services\CommonService;
use App\User;
use Illuminate\Support\Str;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;
use App\Exports\ExportDelivery;
use App\Exports\ExportProfile;
use Maatwebsite\Excel\Facades\Excel;

class ParticipantController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }
    
    public function indexData()
    {
        $data['sidebar']    = 'participants-data';

        return view('pages/participants-data', $data);
    }

    public function indexPayment()
    {
        $data['sidebar']    = 'participants-payment';

        return view('pages/participants-payment', $data);
    }

    public function getAccountById(Request $request)
    {
        $id = $request->id;
        return User::find($id);
    }

    // For manual
    public function saveAccount(Request $request)
    {
        $response = [
            'success'   => false,
            'message'   => 'Data Peserta gagal disimpan'
        ];

        if(empty($request->id)){
            $participant                    = new User();
            $participant->token             = Str::random(50);
            $participant->role              = Constants::ROLE_USER;
            if($request->status_payment == Constants::REGISTRATION_STATUS_PAID) {
                $participant->email_verified_at = Carbon::now();
                $participant->payment_date      = Carbon::now();
            }
        } else {
            $participant = User::find($request->id);
        }
        
        $participant->status_payment        = $request->status_payment;
        $participant->name                  = $request->name;
        $participant->email                 = $request->email;
        $participant->registration_category = $request->registration_category;
        $participant->registration_type     = $request->registration_type;
        $participant->community_name        = $request->community_name;
        $participant->include_jersey        = $request->include_jersey;
        
        if (!empty($request->password)) {
            $participant->password    = bcrypt($request->password);
        }

        $save = $participant->save();

        if($save) {
            for($i = 0; $i < Constants::REGISTRATION_TYPE_COUNT_LIST[$request->registration_type]; $i++){
                $profile = new Profile();
                if($i == 0) {
                    $profile->name          = $request->name;
                    $profile->email         = $request->email;
                    $profile->address       = $request->address;
                    $profile->gender        = $request->gender;
                    $profile->age           = $request->age;
                    $profile->phone_number  = $request->phone_number;
                    $profile->jersey        = $request->jersey;
                }
                $profile->registration_category = $request->registration_category;
                $profile->user_id               = $participant->id;
                $profile->save();
                $profile->bib_number = CommonService::generateBibNumber(Profile::class, $participant->registration_category);
                $profile->save();
            }
        }
        
        if($save){
            $response['success'] = true;
            $response['message'] = 'Data akun berhasil disimpan';
        }
        return response()->json($response);
    }

    public function deleteParticipant(Request $request)
    {

        $participant = User::destroy($request->id);

        if ($participant) {
            return Response::json(array('success' => true, 'message' => 'Data berhasil dihapus'));
        } else {
            return Response::json(array('success' => false, 'message' => 'Data Gagal dihapus, coba lagi'));
        }
    }

    public function changeStatus(Request $request,$user_id)
    {
        $response = [
            'success'   => false,
            'message'   => 'Status gagal diubah'
        ];
        $participant = User::with('profiles')->where('id',$user_id)->first();

        $participant->status_payment = $request->status_pembayaran;

        if($request->status_pembayaran == Constants::REGISTRATION_STATUS_PAID) {
            foreach($participant->profiles as $profile) {
                $profile->bib_number = CommonService::generateBibNumber(Profile::class, $participant->registration_category);
                $profile->save();
            }
            try {
                Notification::send($participant, new NotificationsPaymentConfirmed($participant));
            } 
            catch(\Exception $e) {
                $participant->status_payment = Constants::REGISTRATION_STATUS_ON_CONFIRMATION;
                return Response::json($response);
            }
        }
        if($request->status_pembayaran == Constants::REGISTRATION_STATUS_REJECT) {
            $participant->payment_remark = $request->payment_remark;
            try {
                Notification::send($participant, new PaymentReject($participant));
            } 
            catch(\Exception $e) {
                $participant->status_payment = Constants::REGISTRATION_STATUS_ON_CONFIRMATION;
                return Response::json($response);
            }
        }

        if ($participant->save()) {
            return Response::json(array('success' => true, 'message' => 'Status berhasil diupdate'));
        } else {
            return Response::json($response);
        }
    }

    public function sendResi(Request $request)
    {
        $user = User::with('expedition')->where('id',$request->id)->first();
        
        if(!$user) {
            return Response::json([
                'success'   => false,
                'message'   => 'Data user tidak ditemukan'
            ]);
        }

        // if(empty($request->resi) && $request->use_expedition == Constants::COMMON_STATUS_YES) {
        //     return Response::json([
        //         'success'   => false,
        //         'message'   => 'Masukan nomor resi!'
        //     ]);
        // }

        if(!empty($request->resi)) {
            $user->resi = $request->resi;
        }
        if(!empty($request->expedition_id)) {
            $user->expedition_id = $request->expedition_id;
        }

        $user->use_expedition = $request->use_expedition;
        $user->delivery_message = $request->delivery_message;

        if ($user->save()) {
            $message = 'Data berhasil disimpan';
            if(env('APP_ENV') == 'production' && $user->use_expedition == Constants::COMMON_STATUS_YES) {
                try {
                    $user = User::with('expedition')->where('id',$request->id)->first();
                    Notification::send($user, new RacepackSent($user));
                    $message = 'Resi berhasil dikirim';
                } 
                catch(\Exception $e) {
                    $user = User::with('expedition')->where('id',$request->id)->first();
                    $user->resi = '';
                    $user->expedition_id = '';
                    $user->save();
                    return Response::json(array('success' => false, 'message' => 'Resi gagal dikirim'));
                }
            }
            return Response::json(array('success' => true, 'message' => $message));
        } else {
            return Response::json(array('success' => false, 'message' => 'Data gagal disimpan'));
        }
    }

    public function accountDataTable(Request $request)
    {
        $datas = User::query();
        $datas->with('profiles');
        $datas->where('role',Constants::ROLE_USER);

        if($request->has('registration_category') && !empty($request->registration_category)) {
            $datas->where('registration_category',$request->registration_category);
        }

        if($request->has('registration_type') && !empty($request->registration_type)) {
            $datas->where('registration_type',$request->registration_type);
        }

        if($request->has('status_payment') && !empty($request->status_payment)) {
            $datas->where('status_payment',$request->status_payment);
        }
        if($request->has('include_jersey') && !empty($request->include_jersey)) {
            $datas->where('include_jersey',$request->include_jersey);
        }

        $datas->orderBy('id','desc');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('proof_of_payment', function ($datas) {
                if(!empty($datas->proof_of_payment)) {
                    return '<img class="bukti-img" src="'.url($datas->proof_of_payment).'" height="50px" onclick="detailBukti(\''.url($datas->proof_of_payment).'\',\''.GeneralHelper::moneyFormat(Constants::REGISTRATION_TYPE_PRICE_LIST[$datas->registration_type][$datas->include_jersey]).'\')" />';
                }
                else {
                    return 'No image';
                }
            })
            ->editColumn('resi',function($datas){
                if($datas->use_expedition == Constants::COMMON_STATUS_NO) return 'Tidak menggunakan ekspedisi';
                return $datas->resi;
            })
            ->editColumn('include_jersey',function($datas){
                return Constants::COMMON_STATUS_BOOLEAN[$datas->include_jersey];
            })
            ->editColumn('status_payment', function ($datas) {
                $status = Constants::REGISTRATION_STATUS_COLOR_LIST[$datas->status_payment];
                return '<b>'.$status.'</b>';
            })
            ->addColumn('registration_date', function ($datas) {
                return Carbon::parse($datas->created_at)->format('d F Y');
            })
            ->editColumn('registration_category', function ($datas) {
                return Constants::REGISTRATION_CATEGORY_LIST[$datas->registration_category];
            })
            ->editColumn('registration_type', function ($datas) {
                return Constants::REGISTRATION_TYPE_LIST[$datas->registration_type];
            })
            ->addColumn('registration_amount', function ($datas) {
                return GeneralHelper::moneyFormat(Constants::REGISTRATION_TYPE_PRICE_LIST[$datas->registration_type][$datas->include_jersey]+Constants::UNIQUE_CODE_REGISTRATION);
            })
            ->addColumn('finish', function ($datas) {
                $data = $datas->profiles->first();
                if(empty($data)) return '-';
                return $data->finish_status;
            })
            ->addColumn('action_payment', function ($datas) {
                $actionBtn = '<li><a onclick="changeStatus('.$datas->id.' , \' '. Constants::REGISTRATION_STATUS_PAID .' \')" href="javascript:void(0);"><i class="fa fa-check"></i> Accept</a></li>
                <li><a onclick="reject('.$datas->id.',\' '.Constants::REGISTRATION_STATUS_REJECT.'\')" href="javascript:void(0);"><i class="fa fa-times"></i> Reject</a></li>
                <li><a onclick="sendReminder('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-envelope"></i> Reminder</a></li>';
                if(in_array($datas->status_payment,[Constants::REGISTRATION_STATUS_PAID,Constants::REGISTRATION_STATUS_REJECT])) {
                    return $actionBtn = '-';
                }
                return '<div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Manage <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        '.$actionBtn.'
                                    </ul>
                                </div>';
            })
            ->addColumn('action_data', function ($datas) {
                $deliveryBtn = '';
                $deliveryBtn = '<li><a onclick="delivery(\'SINGLE\','.$datas->id.')" href="javascript:void(0);"><i class="fa fa-truck"></i> Detail Pengiriman</a></li>
                <li><a onclick="inputResi('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-ticket"></i> Kirim Resi</a></li>';
                // if($datas->status_payment == Constants::REGISTRATION_STATUS_PAID && $datas->include_jersey == Constants::COMMON_STATUS_YES) {
                // }
                return '<div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Manage <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="profileData('.$datas->id.',\''.$datas->name.'\')" href="javascript:void(0);"><i class="fa fa-users"></i> Detail</a></li>
                                        <li><a href="https://wa.me/'.str_replace('0','62',@$datas->profiles[0]->phone_number).'"><i class="fa fa-whatsapp"></i> WA (profile pertama)</a></li>
                                        '.$deliveryBtn.'
                                        <li><a onclick="editData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-edit"></i> Edit</a></li>
                                        <li><a onclick="deleteData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-trash"></i> Delete</a></li>
                                    </ul>
                                </div>';
            })
            ->escapeColumns([])->make(true);
    }

    public function deliveryInformation(Request $request)
    {
        $id          = $request->id;
        $method      = $request->method;
        if($method == 'ALL') {
            $data = User::with('profiles')
                        ->paidUser()
                        ->whereNull('resi')
                        ->where('include_jersey',Constants::COMMON_STATUS_YES)
                        ->where('use_expedition',Constants::COMMON_STATUS_YES)
                        ->get();
        } else {
            $data = User::with('profiles')->where('id',$id)->get();
        }
        
        if(empty($data)) return response()->json(['message'=>'404 User not found'],404);
        $pdf = PDF::loadview('pdf/delivery',['data'=>$data,'method'=>$method]);
        $pdf->setPaper('A4', 'potrait');
        $title = $data[0]->name;
    	return $pdf->stream('Racepack Delivery '.$title);
    }

    public function sendReminderEmail($id)
    {
        $user = User::find($id);
        try {
            Notification::send($user, new PaymentReminder($user));
            return response()->json(['status'=>true,'message'=>'Email Berhasil Dikirim!']);
        }
        catch(\Exception $e) {
            return response()->json(['status'=>false,'message'=>'Email Gagal Dikirim!']);
        }
    }

    public function exportDelivery()
    {
        return Excel::download(new ExportDelivery, 'exportDelivery.xlsx');
    }

    public function deliveryInformationTemp()
    {
        $data = \DB::table('temp6')->select('name','address','jersey','phone')
                                // ->where('type','=','FINISH')
                                // ->whereRaw('id NOT IN (SELECT id FROM temp)')
                                ->orderBy('jersey','ASC')
                                ->get();
        
        if(empty($data)) return response()->json(['message'=>'404 User not found'],404);
        $pdf = PDF::loadview('pdf/deliverytemp',['data'=>$data,'method'=>'ALL']);
        $pdf->setPaper('A4', 'potrait');
        $title = $data[0]->name;
    	return $pdf->stream('Racepack Delivery '.$title);
    }

    public function exportProfile()
    {
        return Excel::download(new ExportProfile, 'exportPeserta.xlsx');
    }
}
