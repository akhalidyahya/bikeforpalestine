<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Services\CommonService;
use App\StravaActivity;
use App\StravaAuth;
use App\StravaEvent;
use App\User;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Response;
use Strava;

class StravaController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }

    public function storeEvent(Request $request)
    {
        $aspect_type = $request['aspect_type']; // "create" | "update" | "delete"
        $event_time = $request['event_time']; // time the event occurred
        $object_id = $request['object_id']; // activity ID | athlete ID
        $object_type = $request['object_type']; // "activity" | "athlete"
        $owner_id = $request['owner_id']; // athlete ID
        $subscription_id = $request['subscription_id']; // push subscription ID receiving the event
        $updates = $request['updates']; // activity update: {"title" | "type" | "private": true/false} ; app deauthorization: {"authorized": false}

        $event = json_decode(json_encode($request->all()));

        try {
            StravaEvent::firstOrCreate(
                ['object_id' => strval($event->object_id)],
                [
                    'aspect_type' => @$aspect_type,
                    'event_time' => @$event_time,
                    'object_id' => strval($event->object_id),
                    'object_type' => @$object_type,
                    'owner_id' => @$owner_id,
                    'subscription_id' => @$subscription_id,
                    'updates' => @json_encode($updates),
                    'raw' => @json_encode($request->all()),
                ]
            );

            $user = User::with('strava')
                ->whereHas('strava',function($q)use($owner_id){
                    $q->where('athlete_id',intval($owner_id));
                })->first();
            
            if(!empty($user->strava) && $object_type == Constants::STRAVA_ACTIVITY) {
                $activity = Strava::activity(@$user->strava->access_token, @$object_id);
                if(!empty($activity) && (@$activity->manual == false)) {
                    $activityExist = StravaActivity::where('activity_id',strval(@$object_id))->first();

                    if(empty($activityExist)) {
                        $profile = Profile::where('user_id',$user->id)->first();
                        $categories = @Constants::STRAVA_ACTIVITY_TYPE_ARRAY[$profile->registration_category];

                        // Check whether type is match with registration category, so that will not store other sport type
                        if(in_array(@$activity->type,$categories)) {     
                            $stravaActivity = StravaActivity::create(
                                [
                                    'activity_id' => strval(@$object_id),
                                    'athlete_id' => @$owner_id,
                                    'user_id' => @$user->id,
                                    'distance' => @$activity->distance,
                                    'elapsed_time' => $activity->elapsed_time,
                                    'moving_time' => $activity->moving_time,
                                    'type' => @$activity->type,
                                    'start_date' => Carbon::parse(@$activity->start_date),
                                    'start_date_local' => Carbon::parse(@$activity->start_date_local),
                                    'timezone' => @$activity->timezone,
                                    'raw' => json_encode($activity),
                                ]
                            );
                            
                            // try {
                            //     \DB::statement(
                            //         'UPDATE profiles 
                            //         SET total_distance = (SELECT SUM(distance) FROM strava_activities WHERE user_id = '.$user->id.' AND DATE(start_date_local) >= "'.Constants::START_DATE.'"),
                            //         total_time = (SELECT SUM(moving_time) FROM strava_activities WHERE user_id = '.$user->id.' AND DATE(start_date_local) >= "'.Constants::START_DATE.'")'
                            //     );
                            // } catch (\Throwable $th) {
                            // }
                            $profile->total_distance = $profile->total_distance + $stravaActivity->distance;
                            $profile->total_time = $profile->total_time + $stravaActivity->moving_time;
                            $profile->save();
                            
                            try {
                                if($profile->registration_category == Constants::REGISTRATION_CATEGORY_BIKE 
                                    && ($profile->total_distance >= Constants::BIKE_DISTANCE_KM && empty($profile->finished_at))
                                    ) {
                                    $profile->finished_at   = $profile->total_distance;
                                    $profile->finished_time = $profile->total_time;
                                } elseif($profile->registration_category == Constants::REGISTRATION_CATEGORY_RUN 
                                    && ($profile->total_distance >= Constants::RUN_DISTANCE_KM && empty($profile->finished_at))
                                    ) {
                                    $profile->finished_at   = $profile->total_distance;
                                    $profile->finished_time = $profile->total_time;
                                }
                                $profile->save();
                            } catch (\Throwable $th) {
                                //throw $th;
                            }
                        }

                    } else {
                        StravaActivity::where([ 
                                ['activity_id', '=' , strval(@$object_id)],
                                ['is_editable', '=', 1]
                            ])
                            ->update(
                                [
                                    'activity_id' => strval(@$object_id),
                                    'athlete_id' => @$owner_id,
                                    // 'user_id' => @$user->id,
                                    'distance' => @$activity->distance,
                                    'elapsed_time' => $activity->elapsed_time,
                                    'moving_time' => $activity->moving_time,
                                    'type' => @$activity->type,
                                    'start_date' => Carbon::parse(@$activity->start_date),
                                    'start_date_local' => Carbon::parse(@$activity->start_date_local),
                                    'timezone' => @$activity->timezone,
                                    'raw' => json_encode($activity),
                                ]
                            );
                    }

                }
            }

        } catch (\Throwable $th) {
            throw $th;
        }

        return response('EVENT_RECEIVED', 200);
    }

    public function syncToStrava(Request $request)
    {
        // Check if redirected after authenticated with Strava 
        if(!empty($request->code)) {
            $auth = CommonService::authStrava(\Auth::user()->id,$request->code);
            if($auth) return redirect()->route('user.strava.sync');
        }

        $data['sidebar']    = 'sync';
        $data['connected']  = false;

        $userStrava = StravaAuth::where('user_id',\Auth::user()->id)->first();

        if(!empty($userStrava)) {
            if(strtotime(Carbon::now()) < @$userStrava->expires_at) $data['connected'] = true;
        }

        return view('user-pages.sync',$data);
    }

    public function authToStrava()
    {
        return Strava::authenticate($scope='read_all,profile:read_all,activity:read_all');
    }

    public function disconnectFromStrava()
    {
        $userStrava = StravaAuth::where('user_id',\Auth::user()->id)->first();
        if(!empty($userStrava)) {
            $token = $userStrava->access_token;
            $disconnect = Strava::unauthenticate($token);
            if($disconnect) {
                $userStrava->delete();
            }
        }
        return redirect()->back();
    }
}
