<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Profile;
use App\StravaActivity;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use DataTables;
use Response;

class ComplaintController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }
    
    public function index()
    {
        $data['sidebar'] = 'complaint';
        return view('pages/complaint', $data);
    }

    public function complaintDataTable(Request $request)
    {
        $datas = Complaint::query();
        $datas->with('user');
        $datas->orderBy('id','desc');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('link',function($data){
                return '<a target="_blank" href="'.$data->link.'">'.$data->activity_id.'</a>';
            })
            ->editColumn('status',function($data){
                $status = Constants::RESULT_STATUS_LIST[$data->status];
                if($data->status == Constants::RESULT_STATUS_REQUEST_APPROVAL) {
                    $status = '<b class="text-info">'.$status.'</b>';
                }
                return $status;
            })
            ->addColumn('name',function($data){
                return $data->user->name;
            })
            ->addColumn('action',function($data){
                $actionBtn = '<li><a onclick="verify('.$data->id.' , \' '. Constants::RESULT_STATUS_APPROVED .' \')" href="javascript:void(0);"><i class="fa fa-check"></i> Verify</a></li>
                <li><a onclick="reject('.$data->id.',\' '.Constants::RESULT_STATUS_REJECT.'\')" href="javascript:void(0);"><i class="fa fa-times"></i> Reject</a></li>';

                return '<div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Manage <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                '.$actionBtn.'
                            </ul>
                        </div>';
            })
            ->escapeColumns([])->make(true);
    }

    public function changeStatus(Request $request)
    {
        $id = $request->id;

        $response = [
            'success'   => false,
            'message'   => 'Status gagal diubah'
        ];
        $data = Complaint::with('user.strava')->where('id',$id)->first();

        $data->status = $request->status;
        $activityId = !empty($request->activity_id) ? $request->activity_id : @$data->activity_id;

        if($request->status == Constants::RESULT_STATUS_APPROVED) {
            $type = @$data->user->registration_category == Constants::REGISTRATION_CATEGORY_RUN ? Constants::STRAVA_ACTIVITY_TYPE_RUN : Constants::STRAVA_ACTIVITY_TYPE_RIDE;
            $stravaActivity = StravaActivity::where('activity_id',$activityId)->first();
            if(empty($stravaActivity)) {
                StravaActivity::create(
                    [
                        'activity_id' => $activityId,
                        'athlete_id' => @$data->user->strava->athlete_id,
                        'user_id' => @$data->user_id,
                        'distance' => @$request->distance,
                        'elapsed_time' => @$request->moving_time,
                        'moving_time' => @$request->moving_time,
                        'type' => $type,
                        'start_date' => @$request->start_date,
                        'start_date_local' => @$request->start_date,
                        'timezone' => 'Asia/Jakarta',
                        'raw' => '{}',
                    ]
                );
            } else {
                StravaActivity::where('activity_id',$data->activity_id)
                    ->update([
                        'activity_id' => !empty($request->activity_id) ? $request->activity_id : @$data->activity_id,
                        'athlete_id' => @$data->user->strava->athlete_id,
                        'user_id' => @$data->user_id,
                        'distance' => @$request->distance,
                        'elapsed_time' => @$request->moving_time,
                        'moving_time' => @$request->moving_time,
                        'type' => $type,
                        'start_date' => @$request->start_date,
                        'start_date_local' => @$request->start_date,
                        'timezone' => 'Asia/Jakarta',
                        'raw' => '{}',
                    ]);
            }

            try {
                Profile::where('user_id',$data->user->id)
                    ->update([
                        'total_distance' => \DB::raw('(SELECT SUM(distance) FROM strava_activities WHERE user_id = '.@$data->user->id.' AND DATE(start_date_local) >= "'.Constants::START_DATE.'")'),
                        'total_time' => \DB::raw('(SELECT SUM(moving_time) FROM strava_activities WHERE user_id = '.@$data->user->id.' AND DATE(start_date_local) >= "'.Constants::START_DATE.'")'),
                    ]);
            } catch (\Throwable $th) {
                return Response::json($th);
            }
        }
        
        $data->remark = $request->remark;

        if ($data->save()) {
            return Response::json(array('success' => true, 'message' => 'Status berhasil diupdate'));
        } else {
            return Response::json($response);
        }
    }
}
