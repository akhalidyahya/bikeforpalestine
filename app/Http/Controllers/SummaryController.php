<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use DB;

class SummaryController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }
    
    public function index()
    {
        $userCollection = User::where('role',Constants::ROLE_USER)->get();
        $profileCollection = Profile::with('user')->orderBy('total_distance','desc')->orderBy('total_time','asc')->get();

        $data['sidebar'] = 'summary';
        $data['account_count'] = $userCollection->count();
        $data['active_account_count'] = $userCollection->where('status_payment',Constants::REGISTRATION_STATUS_PAID)->count();
        $data['profile_count'] = $profileCollection->where('user.status_payment',Constants::REGISTRATION_STATUS_PAID)->count();
        $userByRegistrationType = User::select('include_jersey','registration_type',DB::raw('count(registration_type) as registration_type_count'))
                                            ->groupBy('registration_type')
                                            ->groupBy('include_jersey')
                                            ->having('registration_type_count','>',0)
                                            ->paidUser()
                                            ->get();
        $data['total_revenue'] = 0;
        $data['revenue_by_registration_types'] = [];
        foreach ($userByRegistrationType as $user) {
            $data['total_revenue']+= ($user->registration_type_count * Constants::REGISTRATION_TYPE_PRICE_LIST[$user->registration_type][$user->include_jersey]);

            $data['revenue_by_registration_types'][$user->registration_type] = 0;

        }
        foreach($userByRegistrationType as $user) {
            $data['revenue_by_registration_types'][$user->registration_type] += ($user->registration_type_count * Constants::REGISTRATION_TYPE_PRICE_LIST[$user->registration_type][$user->include_jersey]);
        }

        $data['data_submit_count'] = $profileCollection->where('total_distance','>',0)->count();
        $data['jersey_s_count'] = $profileCollection->where('jersey','=',Constants::JERSEY_SIZE_S)->where('bib_number','!=',NULL)->count();
        $data['jersey_m_count'] = $profileCollection->where('jersey','=',Constants::JERSEY_SIZE_M)->where('bib_number','!=',NULL)->count();
        $data['jersey_l_count'] = $profileCollection->where('jersey','=',Constants::JERSEY_SIZE_L)->where('bib_number','!=',NULL)->count();
        $data['jersey_xl_count'] = $profileCollection->where('jersey','=',Constants::JERSEY_SIZE_XL)->where('bib_number','!=',NULL)->count();
        $data['peserta_bike'] = $profileCollection->where('registration_category','=',Constants::REGISTRATION_CATEGORY_BIKE)->where('bib_number','!=',NULL)->count();
        $data['peserta_run'] = $profileCollection->where('registration_category','=',Constants::REGISTRATION_CATEGORY_RUN)->where('bib_number','!=',NULL)->count();
        $data['peserta_male'] = $profileCollection->where('gender','=',Constants::GENDER_MALE)->where('bib_number','!=',NULL)->count();
        $data['peserta_female'] = $profileCollection->where('gender','=',Constants::GENDER_FEMALE)->where('bib_number','!=',NULL)->count();
        $data['winner_run_male'] = $profileCollection->where('gender','=',Constants::GENDER_MALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_RUN)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->splice(0,3);
        $data['winner_run_female'] = $profileCollection->where('gender','=',Constants::GENDER_FEMALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_RUN)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->splice(0,3);
        $data['winner_bike_male'] = $profileCollection->where('gender','=',Constants::GENDER_MALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_BIKE)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->splice(0,3);
        $data['winner_bike_female'] = $profileCollection->where('gender','=',Constants::GENDER_FEMALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_BIKE)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->splice(0,3);

        $data['winner_time_run_male'] = $profileCollection->where('gender','=',Constants::GENDER_MALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_RUN)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->where('total_distance','>=',Constants::RUN_DISTANCE)
                                                    ->sortBy('finished_time')
                                                    ->splice(0,3);
        $data['winner_time_run_female'] = $profileCollection->where('gender','=',Constants::GENDER_FEMALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_RUN)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->where('total_distance','>=',Constants::RUN_DISTANCE)
                                                    ->sortBy('finished_time')
                                                    ->splice(0,3);
        $data['winner_time_bike_male'] = $profileCollection->where('gender','=',Constants::GENDER_MALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_BIKE)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->where('total_distance','>=',Constants::BIKE_DISTANCE)
                                                    ->sortBy('finished_time')
                                                    ->splice(0,3);
        $data['winner_time_bike_female'] = $profileCollection->where('gender','=',Constants::GENDER_FEMALE)
                                                    ->where('registration_category',Constants::REGISTRATION_CATEGORY_BIKE)
                                                    ->where('bib_number','!=',NULL)
                                                    ->where('total_distance','!=',NULL)
                                                    ->where('total_distance','>=',Constants::BIKE_DISTANCE)
                                                    ->sortBy('finished_time')
                                                    ->splice(0,3);
        $rawChartTicketData = $userCollection->where('status_payment',Constants::REGISTRATION_STATUS_PAID)
                                ->groupBy('registration_type')
                                ->map->count();
        $chartData = [];
        foreach (Constants::REGISTRATION_TYPE_LIST as $key => $value) {
            $chartData[$key] = 0;
        }
        if(!empty($rawChartTicketData)) {
            foreach($rawChartTicketData as $key => $value) {
                $chartData[$key] = $value;
            }
        }
        $data['chart_data'] = $chartData;

        return view('pages.summary',$data);
    }
}
