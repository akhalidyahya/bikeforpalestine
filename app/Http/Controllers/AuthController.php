<?php

namespace App\Http\Controllers;

use App\Mail\AccountActivation;
use App\Notifications\ResetPassword;
use App\Notifications\UserRegistered;
use App\Profile;
use App\Services\CommonService;
use App\User;
use Illuminate\Support\Str;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Validator;
use Captcha;
use Illuminate\Support\Facades\Notification;

class AuthController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
        $this->middleware('guest')->except('logout');
    }

    public function loginIndex()
    {
        return view('login');
    }

    public function resetPasswordIndex()
    {
        return view('reset-password-index');
    }

    public function resetPasswordSendForm(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'captcha' => ['required','captcha']
        ],[
            'captcha.captcha' => 'Invalid Captcha'
        ]);
        if($validation->fails()) {
            return redirect()->back()->withErrors($validation);
        }
        
        $user = User::where('email',$request->email)->first();
        if(empty($user)) return redirect()->route('resetPassword.index')->with('error','Email tidak ditemukan!');

        try {
            Notification::send($user, new ResetPassword($user));
        } 
        catch(\Exception $e) {
            return redirect()->route('resetPassword.index')->with('error','Gagal mengirim form reset password! silahkan coba lagi!');
        }
        return redirect()->route('resetPassword.index')->with('success','Form reset password berhasil dikirim! Silahkan cek pada inbox email untuk reset password. (Cek spam bila tidak ada pada inbox)');
    }

    public function resetPasswordDetail($token)
    {
        $token_array = explode('=',$token);
        $id          = $token_array[0];
        $userToken   = $token_array[1];
        $tokenId     = $token_array[2];
        $status      = true;

        if(md5($id) != $tokenId) $status = false;

        $user = User::where('id',$id)->where('token',$userToken)->first();
        if(empty($user)) $status = false;

        if(!$status) return redirect()->route('login.index')->with('message','Alamat url reset password tidak valid');

        $data['id']         = $id;
        $data['user_token'] = $userToken;
        $data['token_id']   = $tokenId;
        $data['email']      = $user->email;
        return view('reset-password',$data);
    }

    public function resetPasswordProcess(Request $request) {
        $validation = Validator::make($request->all(),[
            'captcha' => ['required','captcha']
        ],[
            'captcha.captcha' => 'Invalid Captcha'
        ]);
        if($validation->fails()) {
            return redirect()->back()->withErrors($validation);
        }

        $id          = $request->id;
        $userToken   = $request->user_token;
        $tokenId     = $request->token_id;
        $status      = true;

        if(md5($id) != $tokenId) $status = false;

        $user = User::where('id',$id)->where('token',$userToken)->first();
        if(empty($user)) $status = false;

        if(!$status) return redirect()->route('login.index')->with('message','Alamat url reset password tidak valid');

        $user->token    = Str::random(50);
        $user->password = bcrypt($request->password);
        if($user->save()) {
            return redirect()->route('login.index')->with('success','Reset Password Berhasil!');
        } else {
            return redirect()->route('login.index')->with('error','Reset Password Gagal! Silahkan coba lagi!');
        }
    }

    public function loginProcess(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'email' => 'required',
            'password' => 'required',
            // 'captcha' => ['required','captcha']
        ],
            // [
            //     'captcha.captcha' => 'Invalid Captcha'
            // ]
        );
        if($validation->fails()) {
            return redirect()->route('login.index')->withErrors($validation);
        }

        $credentials = $request->except(['_token','captcha']);

        $user = User::where('email',$request->email)->first();

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            //email verified only
            if(Auth::user()->email_verified_at == NULL && Auth::user()->role == Constants::ROLE_USER) {
                Auth::logout();
                session()->flash('message', 'Email belum diverifikasi!');
                return redirect()->back();
            }

            
            try {
                if(env('APP_ENV') != 'local') {
                    // Check Strava Auth and regenerate if expired
                    CommonService::checkStravaTokenExpire();
                    // CommonService::getLatestStravaActivities();
                }

            } catch (\Throwable $th) {
                //throw $th;
            }
            
            return redirect()->route('dashboard.index');

        }else{
            session()->flash('message', 'Email atau Password yang anda masukkan salah!');
            return redirect()->back();
        }
    }

    public function saveData(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users'
        ];
 
        $messages = [
            'email.unique'  => 'Email sudah terdaftar.',
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput( $request->all() );
        }

        $user = new User();
        $user->name                  = $request->name;
        $user->email                 = $request->email;
        $user->password              = bcrypt($request->password);
        $user->token                 = Str::random(50);
        $user->role                  = Constants::ROLE_USER;
        $user->status_payment        = Constants::REGISTRATION_STATUS_UNPAID;
        $user->registration_category = $request->registration_category;
        $user->registration_type     = $request->registration_type;
        $user->include_jersey        = $request->include_jersey;
        // Disable verified email
        $user->email_verified_at = Carbon::now();

        if(env('APP_ENV') != 'production') {
            $user->email_verified_at = Carbon::now();
            $user->status_payment    = Constants::REGISTRATION_STATUS_PAID;
            $user->payment_date      = Carbon::now();
        }

        if($user->save()) {
            for($i = 0; $i < Constants::REGISTRATION_TYPE_COUNT_LIST[$request->registration_type]; $i++){
                $profile = new Profile();
                if($i == 0) {
                    $profile->name          = $request->name;
                    $profile->email         = $request->email;
                    $profile->address       = $request->address;
                    $profile->gender        = $request->gender;
                    $profile->age           = $request->age;
                    $profile->phone_number  = $request->phone_number;
                }
                $profile->registration_category = $request->registration_category;
                $profile->user_id               = $user->id;
                if(env('APP_ENV') != 'production') {
                    $profile->bib_number = CommonService::generateBibNumber(Profile::class, $request->registration_category);
                }
                $profile->save();
            }

            try {
                if(env('APP_ENV') == 'production') Notification::send($user, new UserRegistered($user));
            } 
            catch(\Exception $e) {
                // $message = 'Pendaftaran berhasil, namun email verifikasi gagal dikirim ke email kamu. Silahkan kontak admin untuk aktivasi manual';
                $message = 'Pendaftaran berhasil! Silahkan login!';
                return redirect()->route('login.index')->with('message',$message);
            }
        }
        
        // $message = 'Pendaftaran Berhasil! Silahkan cek pada inbox email untuk aktivasi akun. (Cek spam bila tidak ada pada inbox)';
        $message = 'Pendaftaran berhasil! Silahkan login!';
        return redirect()->route('login.index')->with('success',$message);
    }
    
    public function registerIndex()
    {
        return view('register');
    }

    public function sendActivation($id)
    {
        $user = User::find($id);
        try {
            Notification::send($user, new UserRegistered($user));
            return response()->json(['status'=>true,'message'=>'Email Berhasil Dikirim!']);
        }
        catch(\Exception $e) {
            return response()->json(['status'=>false,'message'=>'Email Gagal Dikirim!']);
        }
    }

    public function activation($user_id,$token)
    {
        $user = User::where('id',$user_id)->where('token',$token)->first();
        if(empty($user)) {
            return redirect()->route('register.index')->with('message','Data Akun Tidak Ditemukan!');
        }
        if(!empty($user->email_verified_at)) {
            return redirect()->route('login.index')->with('message','Akun sudah diaktivasi, silahkan login dengan email dan password yang telah didaftarkan.');
        }
        $user->email_verified_at = Carbon::now();
        
        if($user->save()) {
            return redirect()->route('login.index')->with('success','Aktivasi akun berhasil! Silahkan login dengan email dan password yang telah didaftarkan.');
        } else {
            return redirect()->route('login.index')->with('message','Something went wrong!');
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->route('login.index');
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> Captcha::img('flat')]);
    }
}
