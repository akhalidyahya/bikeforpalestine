<?php

namespace App\Http\Controllers;

use App\Helper\GeneralHelper;
use Response;
use DataTables;
use App\Profile;
use App\User;
use Illuminate\Support\Str;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use PDF;

class ProfileController extends Controller
{
    public function __construct()
    {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $site_settings);
    }
    
    public function profileDataTable(Request $request)
    {
        $datas = Profile::query();
        $datas->with('user');
        $datas->whereNotNull('name');
        $datas->whereHas('user',function($q){
            $q->paidUser();
        });


        if($request->has('registration_category') && !empty($request->registration_category)) {
            $datas->whereHas('user',function($q)use($request){
                $q->where('registration_category',$request->registration_category);
            });
        }

        if($request->has('registration_type') && !empty($request->registration_type)) {
            $datas->whereHas('user',function($q)use($request){
                $q->where('registration_type',$request->registration_type);
            });
        }
        if($request->has('include_jersey') && !empty($request->include_jersey)) {
            $datas->whereHas('user',function($q)use($request){
                $q->where('include_jersey',$request->include_jersey);
            });
        }

        $datas->orderBy('id','desc')->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('gender',function($datas){
                return @Constants::GENDER_LIST[$datas->gender];
            })
            ->addColumn('account',function($datas){
                return '<a onclick="editData('.$datas->user->id.')" href="javascript:void(0);">'.$datas->user->name.'</a>';
            })
            ->addColumn('registration_category', function ($datas) {
                return @Constants::REGISTRATION_CATEGORY_LIST[$datas->user->registration_category];
            })
            ->addColumn('registration_type', function ($datas) {
                return @Constants::REGISTRATION_TYPE_LIST[$datas->user->registration_type];
            })
            ->addColumn('include_jersey',function($datas){
                return Constants::COMMON_STATUS_BOOLEAN[$datas->user->include_jersey];
            })
            ->escapeColumns([])->make(true);
    }

    public function getProfileByAccountId($account_id)
    {
        return Profile::with('user.expedition')->where('user_id',$account_id)->get();
    }

}
