<?php

namespace App\Http\Controllers\Frontend;

use App\Donation;
use App\Helper\GeneralHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\DonationPaid;
use App\Profile;
use App\Utilities\Constants;
use Carbon\CarbonInterval;
use DataTables;
use Illuminate\Support\Facades\Notification;
use Validator;

class PageController extends Controller
{
    protected $site_settings;
    public function __construct()
    {
        $this->site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        \View::share('CONF', $this->site_settings);
    }
    public function index()
    {
        if($this->site_settings->registrationDate > Date('Y-m-d')) return view('comingsoon');
        $data['faq'] = json_decode(\Storage::disk('public')->get('json/faq.json'));
        return view('frontend.pages.index.index',$data);
    }

    public function leaderboard()
    {
        return view('frontend.pages.leaderboard.leaderboard');
    }

    public function donation()
    {
        return view('frontend.pages.donation.donation');
    }

    public function saveDonation(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'captcha' => ['required','captcha']
        ],[
            'captcha.captcha' => 'Invalid Captcha'
        ]);

        if($validation->fails()) {
            return redirect()->route('donation')->withErrors($validation);
        }

        $donation = new Donation();
        $donation->name = $request->name;
        $donation->email = $request->email;
        $donation->phone_number = $request->phone_number;
        if($request->has('is_anonymous')) $donation->is_anonymous = $request->is_anonymous;
        $donation->donation_amount = str_replace('.','',$request->donation_amount);
        $donation->status = Constants::COMMON_STATUS_NEW;
        if($donation->save()) {
            $donation->transaction_code = Date('YmdHis').$donation->id;
            $donation->save();
            try {
                Notification::send($donation, new DonationPaid($donation));
            } 
            catch(\Exception $e) {
                return redirect()->route('donation')->with('message', 'Terimakasih atas konfirmasi donasinya :)');
            }
            return redirect()->route('donation')->with('success', 'Terimakasih atas konfirmasi donasinya :)');
        }
    }

    public function leaderboardData(Request $request)
    {
        $datas = Profile::query();
        $datas->with('user');
        $datas->whereHas('user',function($q)use($request){
            $q->where('registration_category',$request->registration_category);
            $q->paidUser();
        });
        // $datas->whereNotNull('total_distance');
        $datas->whereNotNull('bib_number');
        $datas->orderByRaw('CAST(total_distance AS float) DESC');
        $datas->orderByRaw('CAST(total_time AS float) ASC');
        $datas->get();

        return DataTables::of($datas)
            ->addIndexColumn()
            // ->editColumn('name', function ($datas) {
            //     return GeneralHelper::censorName($datas->name);
            // })
            ->editColumn('gender',function($data){
                return Constants::GENDER_LIST[$data->gender];
            })
            ->escapeColumns([])->make(true);
    }

    public function merchandise()
    {
        return view('frontend.pages.merchandise.merchandise');
    }
}
