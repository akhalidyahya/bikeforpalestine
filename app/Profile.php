<?php

namespace App;

use App\Services\CommonService;
use App\Utilities\Constants;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    protected $table = 'profiles';
    protected $fillable = [
        'name',
        'email',
        'address',
        'gender',
        'age',
        'phone_number',
        'bib_number',
        'jersey',
        'total_distance',
        'total_time',
        'finished_time',
        'finished_at',
        'registration_category',
        'user_id'
    ];

    const FORM_VALIDATION = [
        'name' => 'required',
        'email' => 'required|unique',
        'address' => 'required',
        'gender' => 'required',
        'age' => 'required',
        'phone_number' => 'required',
        'jersey' => 'required',
    ];
    
    protected $casts = [
        'total_distance' => 'float',
        'total_time' => 'float',
    ];
    
    protected $appends = [
        'total_time_time_format',
        'total_indonesia_format',
        'total_distance_km_format',
        'pace',
        'initial',
        'finish_status',
        'round_aqsha',
    ];

    public function getFinishStatusAttribute() {
        $distance = $this->attributes['total_distance'];
        $category = $this->attributes['registration_category'];
        $status = '-';

        if($category == Constants::REGISTRATION_CATEGORY_RUN) {
            if($distance >= Constants::RUN_DISTANCE_KM) {
                $status = Constants::FINISH_STATUS_FINISH;
            }
        } elseif($category == Constants::REGISTRATION_CATEGORY_BIKE) {
            if($distance >= Constants::BIKE_DISTANCE_KM) {
                $status = Constants::FINISH_STATUS_FINISH;
            }
        }

        return $status;
    }

    // Return hh:mm:ss
    public function getTotalTimeTimeFormatAttribute() {
        $oneDayInSeconds = 86399;
        $value = $this->attributes['total_time']; //seconds
        if(empty($value)) return '00:00:00';
        $dt = Carbon::now();
        $days = $dt->diffInDays($dt->copy()->addSeconds($value));
        $hours = $dt->diffInHours($dt->copy()->addSeconds($value)->subDays($days));
        $minutes = $dt->diffInMinutes($dt->copy()->addSeconds($value)->subDays($days)->subHours($hours));
        $seconds = $dt->diffInSeconds($dt->copy()->addSeconds($value)->subDays($days)->subHours($hours)->subMinutes($minutes));
        return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->format('%D : %H : %I : %S');
    }

    // Return hh:mm:ss
    public function getTotalIndonesiaFormatAttribute() {
        $oneDayInSeconds = 86399;
        $oneHourInSeconds = 3600;
        $value = $this->attributes['total_time']; //seconds
        if(empty($value)) return '00:00:00';
        $dt = Carbon::now();
        $days = $dt->diffInDays($dt->copy()->addSeconds($value));
        $hours = $dt->diffInHours($dt->copy()->addSeconds($value)->subDays($days));
        $minutes = $dt->diffInMinutes($dt->copy()->addSeconds($value)->subDays($days)->subHours($hours));
        $seconds = $dt->diffInSeconds($dt->copy()->addSeconds($value)->subDays($days)->subHours($hours)->subMinutes($minutes));
        if($value >= $oneDayInSeconds) {
            return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->format('%D Hari %H Jam %I Menit %S Detik');
        } elseif($value >= $oneHourInSeconds) {
            return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->format('%H Jam %I Menit %S Detik');
        } elseif($value >= 60) {
            return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->format('%I Menit %S Detik');
        } else {
            return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->format('%S Detik');
        }
    }

    public function getTotalDistanceKmFormatAttribute() {
        $value = $this->attributes['total_distance']; // meters
        if(empty($value)) return 0;
        return number_format(($value/1000),2,',','.');
    }

    public function getRoundAqshaAttribute() {
        $value = $this->attributes['total_distance']; // meters
        if(empty($value)) return 0;
        return round($value/12500);
    }

    public function getPaceAttribute() {
        $time = $this->attributes['total_time']; // seconds
        $distance = $this->attributes['total_distance']; // meters
        if(empty($time) || empty($distance)) return 0;

        $dis_pace = $distance / 1000; // convert to km
        //getting seconds per km
        $pace = $time / $dis_pace;

        //getting minutes from $pace
        $min = floor($pace / 60);

        //adding 0 before,  if lower than 10
        $min = ($min > 10) ? $min : '0'.$min;

        //getting remaining seconds
        $sec = $pace % 60;

        //adding 0 before, if lower than 10
        $sec = ($sec > 10) ? $sec : '0'.$sec;

        return $min.":".$sec;
    }

    public function getInitialAttribute() {
        return CommonService::getInitial($this->attributes['name']);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function results()
    {
        return $this->hasMany(Result::class, 'profile_id', 'id');
    }
}
