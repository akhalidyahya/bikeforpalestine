<?php

namespace App;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;

class StravaActivity extends Model
{
    protected $table = 'strava_activities';
    protected $fillable = [
        'activity_id',
        'athlete_id',
        'user_id',
        'distance',
        'elapsed_time',
        'moving_time',
        'type',
        'start_date',
        'start_date_local',
        'timezone',
        'raw',
    ];

    protected $appends = [
        'distance_km_format',
        'moving_time_format',
        'strava_activity_link'
    ];

    public function getStravaActivityLinkAttribute() {
        $value = $this->attributes['activity_id'];
        if(empty($value)) return '#';
        return 'https://strava.com/activities/'.$value;
    }

    // Return hh:mm:ss
    public function getMovingTimeFormatAttribute() {
        $value = $this->attributes['moving_time']; //seconds
        if(empty($value)) return '00:00:00';
        $dt = Carbon::now();
        $days = $dt->diffInDays($dt->copy()->addSeconds($value));
        $hours = $dt->diffInHours($dt->copy()->addSeconds($value)->subDays($days));
        $minutes = $dt->diffInMinutes($dt->copy()->addSeconds($value)->subDays($days)->subHours($hours));
        $seconds = $dt->diffInSeconds($dt->copy()->addSeconds($value)->subDays($days)->subHours($hours)->subMinutes($minutes));
        return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->format('%H:%I:%S');
    }

    public function getDistanceKmFormatAttribute() {
        $value = $this->attributes['distance']; // meters
        if(empty($value)) return 0;
        return floatval(number_format(($value/1000),2,'.',','));
    }

    /**
     * Get the user that owns the StravaActivity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
