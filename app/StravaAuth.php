<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StravaAuth extends Model
{
    protected $table = 'strava_auths';
    protected $fillable = [
        'user_id',
        'athlete_id',
        'token_type',
        'expires_at',
        'expires_in',
        'refresh_token',
        'access_token',
        'athlete',
    ];

    /**
     * Get the user that owns the StravaAuth
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
