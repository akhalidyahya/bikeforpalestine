<?php

namespace App;

use App\Utilities\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'token',
        'role',
        'registration_category',
        'registration_type',
        'status_payment',
        'proof_of_payment',
        'payment_date',
        'payment_remark',
        'include_jersey',
        'community_name',
        'delivery_message'
    ];

    const FORM_VALIDATION = [
        'email' => 'required',
        'registration_category' => 'required',
        'registration_type' => 'required',
        'include_jersey' => 'required',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profiles()
    {
        return $this->hasMany(Profile::class, 'user_id', 'id');
    }

    public function expedition()
    {
        return $this->belongsTo(Expedition::class, 'expedition_id', 'id');
    }

    public function strava()
    {
        return $this->hasOne(StravaAuth::class, 'user_id', 'id');
    }

    public function scopePaidUser($q)
    {
        return $q->where('status_payment',Constants::REGISTRATION_STATUS_PAID);
    }
}
