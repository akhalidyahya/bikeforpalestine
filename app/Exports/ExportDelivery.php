<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;


class ExportDelivery implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('profiles as p')
            ->select('u.name', 'p.email', 'p.phone_number', 'u.registration_category', 'u.registration_type', 'u.status_payment', 'u.include_jersey', 'p.address', 'p.jersey')
            ->leftjoin('users as u', 'u.id', '=', 'p.user_id')
            ->whereNull('u.deleted_at')
            ->whereNull('p.deleted_at')
            ->where('u.status_payment', '=', 'PAID') // Hapus utk di all user
            ->where('u.include_jersey', '=', 'YES') // Hapus utk di all user
            ->where('u.use_expedition', '=', 'YES') // Hapus utk di all user
            ->whereNull('u.resi') // Hapus utk di all user
            ->where('u.role', '=', 'USER')
            ->orderBy('jersey') // Hapus utk di all user
            ->get();

            return $data;
        
    }

    public function headings(): array
    {
        return[
            'name', 'email', 'phone_number', 'resgistration_category', 'registration_type', 'status_payment', 'include_jersey', 'address', 'jersey'
        ];
    }
}
