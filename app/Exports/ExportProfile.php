<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;


class ExportProfile implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('profiles as p')
            ->select('p.name', 'p.email', 'p.address', 'p.gender', 'p.age', 'p.bib_number', 'p.total_distance', 'p.total_time', 'p.jersey', 'p.phone_number', 'p.registration_category', 'u.registration_type', 'p.created_at', 'u.status_payment')
            ->leftjoin('users as u', 'u.id', '=', 'p.user_id')
            ->whereNull('u.deleted_at')
            ->whereNull('p.deleted_at')
            ->where('u.role', '=', 'USER')
            ->get();

            return $data;
        
    }

    public function headings(): array
    {
        return[
            'name', 'email', 'address', 'gender', 'age', 'bib_number', 'total_distance', 'total_time', 'jersey', 'phone_number', 'registration_category', 'registration_type', 'register_at', 'status_payment'
        ];
    }
}
