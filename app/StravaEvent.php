<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StravaEvent extends Model
{
    protected $table = 'strava_events';
    protected $fillable = [
        'aspect_type',
        'event_time',
        'object_id',
        'object_type',
        'owner_id',
        'subscription_id',
        'updates',
        'raw',
    ];
}
