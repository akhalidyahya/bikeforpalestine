<?php 
namespace App\Utilities;

class Constants {
    const COMMON_STATUS_YES = 'YES';
    const COMMON_STATUS_NO  = 'NO';
    const COMMON_STATUS_NEW = 'NEW';

    const COMMON_STATUS_BOOLEAN = [
        self::COMMON_STATUS_YES => 'Ya',
        self::COMMON_STATUS_NO  => 'Tidak',
    ];

    const ROLE_ADMIN    = 'ADMIN';
    const ROLE_USER     = 'USER';    

    const REGISTRATION_STATUS_PAID              = 'PAID';
    const REGISTRATION_STATUS_UNPAID            = 'UNPAID';
    const REGISTRATION_STATUS_ON_CONFIRMATION   = 'ON_CONFIRMATION';
    const REGISTRATION_STATUS_REJECT            = 'REJECT';
    // const REGISTRATION_STATUS_RACEPACK_SENT     = 'RACEPACK_SENT';
    const REGISTRATION_STATUS_LISTS = [
        self::REGISTRATION_STATUS_UNPAID            => 'Menunggu Pembayaran',
        self::REGISTRATION_STATUS_PAID              => 'Pembayaran Selesai',
        self::REGISTRATION_STATUS_ON_CONFIRMATION   => 'Sedang Dikonfirmasi',
        self::REGISTRATION_STATUS_REJECT            => 'Ditolak/Bukti Tidak Valid',
        // self::REGISTRATION_STATUS_RACEPACK_SENT     => 'Racepack Telah Dikirim'
    ];
    const REGISTRATION_STATUS_COLOR_LIST = [
        self::REGISTRATION_STATUS_PAID              => '<span class="col-green">Pembayaran Selesai</span>',
        self::REGISTRATION_STATUS_UNPAID            => 'Menunggu Pembayaran',
        self::REGISTRATION_STATUS_ON_CONFIRMATION   => '<span class="col-blue">Sedang Dikonfirmasi</span>',
        self::REGISTRATION_STATUS_REJECT            => '<span class="col-red">Ditolak/Bukti Tidak Valid</span>',
        // self::REGISTRATION_STATUS_RACEPACK_SENT     => '<span class="col-green">Racepack Telah Dikirim</span>'
    ];

    //gak kepake
    // const REGISTRATION_TYPE_SINGLE = 'SINGLE';
    // const REGISTRATION_TYPE_DOUBLE = 'DOUBLE';
    // const REGISTRATION_TYPE_FAMILY = 'FAMILY';
    // const REGISTRATION_TYPE_LIST = [
    //     self::REGISTRATION_TYPE_SINGLE => 'Single',
    //     self::REGISTRATION_TYPE_DOUBLE => 'Double',
    //     self::REGISTRATION_TYPE_FAMILY => 'Family',
    // ];
    // const REGISTRATION_TYPE_PRICE_LIST  = [
    //     self::REGISTRATION_TYPE_SINGLE => [
    //         self::COMMON_STATUS_YES => 150000,
    //         self::COMMON_STATUS_NO => 100000,
    //     ],
    //     self::REGISTRATION_TYPE_DOUBLE => [
    //         self::COMMON_STATUS_YES => 250000,
    //         self::COMMON_STATUS_NO => 150000,
    //     ],
    //     self::REGISTRATION_TYPE_FAMILY => [
    //         self::COMMON_STATUS_YES => 500000,
    //         self::COMMON_STATUS_NO => 250000,
    //     ]
    // ];
    // const REGISTRATION_TYPE_COUNT_LIST = [
    //     self::REGISTRATION_TYPE_SINGLE => 1,
    //     self::REGISTRATION_TYPE_DOUBLE => 2,
    //     self::REGISTRATION_TYPE_FAMILY => 5,
    // ];
    // const REGISTRATION_TYPE_COLOR_LIST = [
    //     '#03A9F4',
    //     '#FF5722',
    //     '#9C27B0',
    // ];
    
    const REGISTRATION_TYPE_NORMAL      = 'NORMAL';
    const REGISTRATION_TYPE_EARLY       = 'EARLYBIRD';
    const REGISTRATION_TYPE_COMMUNITY   = 'COMMUNITY';
    const REGISTRATION_TYPE_NON_RACEPACK   = 'NON_RACEPACK';
    
    const REGISTRATION_TYPE_LIST = [
        self::REGISTRATION_TYPE_NORMAL      => 'Normal',
        self::REGISTRATION_TYPE_EARLY       => 'Early Bird',
        self::REGISTRATION_TYPE_COMMUNITY   => 'Komunitas',
        self::REGISTRATION_TYPE_NON_RACEPACK   => 'Non Racepack'
    ];
    const REGISTRATION_TYPE_PRICE_LIST  = [
        self::REGISTRATION_TYPE_NORMAL => [
            //include race pack or no
            self::COMMON_STATUS_YES => 200000,
            self::COMMON_STATUS_NO => 100000,
        ],
        self::REGISTRATION_TYPE_EARLY => [
            //include race pack or no
            self::COMMON_STATUS_YES => 175000,
            self::COMMON_STATUS_NO => 100000,
        ],
        self::REGISTRATION_TYPE_COMMUNITY => [
            //include race pack or no
            self::COMMON_STATUS_YES => 150000,
            self::COMMON_STATUS_NO => 100000,
        ],
        self::REGISTRATION_TYPE_NON_RACEPACK => [
            //include race pack or no
            self::COMMON_STATUS_YES => 100000,
            self::COMMON_STATUS_NO => 100000,
        ],
    ];

    //jumlah profile
    const REGISTRATION_TYPE_COUNT_LIST = [
        self::REGISTRATION_TYPE_NORMAL      => 1,
        self::REGISTRATION_TYPE_EARLY       => 1,
        self::REGISTRATION_TYPE_COMMUNITY   => 1,
        self::REGISTRATION_TYPE_NON_RACEPACK   => 1,
    ];
    const EARLYBIRD_QUOTA   = 100;
    const EARLYBIRD_DAYS    = 5;
    const REGISTRATION_TYPE_COLOR_LIST = [
        '#03A9F4',
        '#FF5722',
        '#9C27B0',
        '#9D19C1',
    ];

    const REGISTRATION_CATEGORY_RUN     = 'RUN';
    const REGISTRATION_CATEGORY_BIKE    = 'BIKE';
    const REGISTRATION_CATEGORY_LIST    = [
        self::REGISTRATION_CATEGORY_RUN     => 'Run',
        self::REGISTRATION_CATEGORY_BIKE    => 'Ride',
    ];

    const GENDER_MALE   = 'MALE';
    const GENDER_FEMALE = 'FEMALE';
    const GENDER_LIST = [
        self::GENDER_MALE   => 'Laki-laki',
        self::GENDER_FEMALE => 'Perempuan',
    ];

    const JERSEY_SIZE_S     = 'S';
    const JERSEY_SIZE_M     = 'M';
    const JERSEY_SIZE_L     = 'L';
    const JERSEY_SIZE_XL    = 'XL';
    const JERSEY_SIZE_LIST  = [
        self::JERSEY_SIZE_S     => 'S',
        self::JERSEY_SIZE_M     => 'M',
        self::JERSEY_SIZE_L     => 'L',
        self::JERSEY_SIZE_XL    => 'XL',
    ];
    const JERSEY_SIZE_STOCK_LIST = [
        self::JERSEY_SIZE_S     => 200,
        self::JERSEY_SIZE_M     => 300,
        self::JERSEY_SIZE_L     => 300,
        self::JERSEY_SIZE_XL    => 200,
    ];

    const RESULT_STATUS_REQUEST_APPROVAL = 'REQUEST_APPROVAL';
    const RESULT_STATUS_APPROVED         = 'APPROVED';
    const RESULT_STATUS_REJECT           = 'REJECT';
    const RESULT_STATUS_LIST             = [
        self::RESULT_STATUS_APPROVED => 'Telah Diverifikasi',
        self::RESULT_STATUS_REJECT   => 'Ditolak',
        self::RESULT_STATUS_REQUEST_APPROVAL   => 'Sedang Diperiksa',
    ];

    const BIKE_DISTANCE = 125;
    const RUN_DISTANCE  = 12.5;

    const BIKE_DISTANCE_KM = 125000;
    const RUN_DISTANCE_KM  = 12500;

    const UNIQUE_CODE_REGISTRATION = 125;
    const UNIQUE_CODE_DONATION = 50;
    const UNIQUE_CODE_COMMUNITY = 12;

    const STRAVA_ACTIVITY = 'activity';

    const STRAVA_ACTIVITY_TYPE_RUN = 'Run';
    const STRAVA_ACTIVITY_TYPE_VIRTUAL_RUN = 'VirtualRun';
    const STRAVA_ACTIVITY_TYPE_RIDE = 'Ride';
    const STRAVA_ACTIVITY_TYPE_VIRTUAL_RIDE = 'VirtualRide';

    const STRAVA_ACTIVITY_TYPE_ARRAY = [
        self::REGISTRATION_CATEGORY_RUN => [
            self::STRAVA_ACTIVITY_TYPE_RUN, self::STRAVA_ACTIVITY_TYPE_VIRTUAL_RUN
        ],
        self::REGISTRATION_CATEGORY_BIKE => [
            self::STRAVA_ACTIVITY_TYPE_RIDE, self::STRAVA_ACTIVITY_TYPE_VIRTUAL_RIDE
        ],
    ];
    const STRAVA_ACTIVITY_TYPE_RUN_ARRAY = [
        self::STRAVA_ACTIVITY_TYPE_RUN, self::STRAVA_ACTIVITY_TYPE_VIRTUAL_RUN
    ];
    const STRAVA_ACTIVITY_TYPE_RIDE_ARRAY = [
        self::STRAVA_ACTIVITY_TYPE_RIDE, self::STRAVA_ACTIVITY_TYPE_VIRTUAL_RIDE
    ];

    const START_DATE = '2022-01-23';

    const FINISH_STATUS_FINISH = 'Finish';
}

?>