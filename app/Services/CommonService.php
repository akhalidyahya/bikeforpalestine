<?php
namespace App\Services;

use App\Profile;
use App\StravaActivity;
use App\StravaAuth;
use App\User;
use App\Utilities\Constants;
use Carbon\Carbon;
use Strava;

class CommonService {
    public static function generateBibNumber($model,$category) {
        $number = $model::where('registration_category',$category)->whereNotNull('bib_number')->count() + 1;
        return substr($category,0,1).str_pad($number, 5, '0', STR_PAD_LEFT);
    }

    /**
     * Time addition
     * @param int $time1
     * @param int $time2
     * @return string | NULL
     */
    public static function addTime($time1,$time2) {
        try {
            $time1_array = explode(':',strval($time1));
            $time2_array = explode(':',strval($time2));
            $hour   = intval($time1_array[0]) + intval($time2_array[0]);
            $minute = intval($time1_array[1]) + intval($time2_array[1]);
            $second = intval($time1_array[2]) + intval($time2_array[2]);
            if($minute >= 60) {
                $minute_mod = $minute%60;
                $hour   += round($minute/60);
                $minute = $minute_mod;
            }
            if($second >= 60) {
                $second_mod = $second%60;
                $minute += round($second/60);
                $second = $second_mod;
            }
            return $hour.':'.$minute.':'.$second;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Store Strava Auth response
     * @param int $userId
     * @param string $code
     * @return bool
     */
    public static function authStrava($userId,$code) {
        $token = Strava::token($code);
        if(empty($token)) {
            return false;
        }

        $athlete = json_decode(json_encode($token->athlete));
        
        $stravaAuth = StravaAuth::where('user_id',$userId)->first();
        if(!empty($stravaAuth)) {
            $stravaAuth->user_id = $userId;
            $stravaAuth->athlete_id = $token->athlete->id;
            $stravaAuth->token_type = $token->token_type;
            $stravaAuth->expires_at = $token->expires_at;
            $stravaAuth->expires_in = $token->expires_in;
            $stravaAuth->refresh_token = $token->refresh_token;
            $stravaAuth->access_token = $token->access_token;
            $stravaAuth->athlete = json_encode($token->athlete);
            $stravaAuth->save();
        } else {
            $stravaAuth = StravaAuth::create(
                [
                    'user_id' => $userId,
                    'athlete_id' => $token->athlete->id,
                    'token_type' => $token->token_type,
                    'expires_at' => $token->expires_at,
                    'expires_in' => $token->expires_in,
                    'refresh_token' => $token->refresh_token,
                    'access_token' => $token->access_token,
                    'athlete' => json_encode($token->athlete)
                ]
            );
        }

        if($stravaAuth) return true;
    }

    /**
     * Check and refresh token
     * @return void
     */
    public static function checkStravaTokenExpire() {
        // $user = User::with('strava')->where('id',\Auth::user()->id)->first();
        $stravaAuth = StravaAuth::where('user_id',\Auth::user()->id)->first();
        // Check if current token has expired
        if(strtotime(Carbon::now()) > @$stravaAuth->expired_at && !empty($stravaAuth)) {
            // Token has expired, generate new tokens using the currently stored user refresh token
            $refresh = Strava::refreshToken($stravaAuth->refresh_token);

            // Update the users tokens
            StravaAuth::where('user_id', $stravaAuth->user_id)->update([
                'access_token' => $refresh->access_token,
                'refresh_token' => $refresh->refresh_token,
                'expires_at' => @$refresh->expires_at,
                'expires_in' => @$refresh->expires_in,
            ]);
        }
    }

    public static function getInitial($string,$limit=2) {
        $stringArray = explode(" ",$string);
        $initial = '';
        if(count($stringArray) > 1) {
            for ($i=0; $i < $limit; $i++) { 
                $initial.= substr($stringArray[$i],0,1);
            }
        } else {
            $initial.= substr($stringArray[0],0,1);
        }

        return $initial;
    }

    /**
     * Update Strava Activity Table
     * @return void
     */
    public static function getLatestStravaActivities()
    {
        $user = User::find(\Auth::user()->id);
        $stravaAuth = StravaAuth::where('user_id',\Auth::user()->id)->first();
        if(empty($stravaAuth)) return NULL;
        $access_token = $stravaAuth->access_token;
        $page = 1;
        $perPage = 30;
        $before = NULL;
        $after = env('APP_ENV') != 'production' ? NULL : 1642870800; // Epoch for 2022-01-23 00:00:00
        $activities = Strava::activities($access_token,$page,$perPage,$before,$after);
        
        if(!empty($activities)) {
            foreach($activities as $activity) {
                if($activity->manual) continue;

                $activityExist = StravaActivity::where('activity_id',strval(@$activity->id))->first();

                if(empty($activityExist)) {
                    $categories = @Constants::STRAVA_ACTIVITY_TYPE_ARRAY[$user->registration_category];

                    // Check whether type is match with registration category, so that will not store other sport type
                    if(in_array(@$activity->type,$categories)) {     
                        $stravaActivity = StravaActivity::create(
                            [
                                'activity_id' => strval(@$activity->id),
                                'athlete_id' => strval(@$activity->athlete->id),
                                'user_id' => @$user->id,
                                'distance' => @$activity->distance,
                                'elapsed_time' => @$activity->elapsed_time,
                                'moving_time' => @$activity->moving_time,
                                'type' => @$activity->type,
                                'start_date' => Carbon::parse(@$activity->start_date),
                                'start_date_local' => Carbon::parse(@$activity->start_date_local),
                                'timezone' => @$activity->timezone,
                                'raw' => json_encode($activity),
                            ]
                        );
                        $profile = Profile::where('user_id',$user->id)->first();
                        // try {
                        //     \DB::statement(
                        //         'UPDATE profiles 
                        //         SET total_distance = (SELECT SUM(distance) FROM strava_activities 
                        //         WHERE user_id = '.$user->id.'
                        //         AND DATE(start_date_local) >= "'.Constants::START_DATE.'"),
                        //         total_time = (SELECT SUM(moving_time) FROM strava_activities 
                        //         WHERE user_id = '.$user->id.'
                        //         AND DATE(start_date_local) >= "'.Constants::START_DATE.'")'
                        //     );
                        // } catch (\Throwable $th) {
                        // }
                        $profile->total_distance = $profile->total_distance + @$activity->distance;
                        $profile->total_time = $profile->total_time + @$activity->moving_time;
                        $profile->save();
                        
                        try {
                            if($profile->registration_category == Constants::REGISTRATION_CATEGORY_BIKE 
                                && ($profile->total_distance >= Constants::BIKE_DISTANCE_KM && empty($profile->finished_at))
                                ) {
                                $profile->finished_at   = $profile->total_distance;
                                $profile->finished_time = $profile->total_time;
                            } elseif($profile->registration_category == Constants::REGISTRATION_CATEGORY_RUN 
                                && ($profile->total_distance >= Constants::RUN_DISTANCE_KM && empty($profile->finished_at))
                                ) {
                                $profile->finished_at   = $profile->total_distance;
                                $profile->finished_time = $profile->total_time;
                            }
                            $profile->save();
                        } catch (\Throwable $th) {
                            //throw $th;
                        }
                    }

                } else {
                    StravaActivity::where([ 
                            ['activity_id', '=' , strval(@$activity->id)],
                            ['is_editable', '=', 1]
                        ])
                        ->whereRaw('DATE(start_date_local) >= "'.Constants::START_DATE.'"')
                        ->update(
                            [
                                'activity_id' => strval(@$activity->id),
                                'athlete_id' => strval(@$activity->athlete->id),
                                // 'user_id' => @$user->id,
                                'distance' => @$activity->distance,
                                'elapsed_time' => @$activity->elapsed_time,
                                'moving_time' => @$activity->moving_time,
                                'type' => @$activity->type,
                                'start_date' => @Carbon::parse(@$activity->start_date),
                                'start_date_local' => @Carbon::parse(@$activity->start_date_local),
                                'timezone' => @$activity->timezone,
                                'raw' => json_encode($activity),
                            ]
                        );
                }
            }
        }
    }

    public static function getTotalParticipant($category) {
        return User::paidUser()->where('registration_category',$category)->count();
    }

    public static function getUserRank($category,$userId) {
        // Set variable row_number in mysql
        \DB::statement('SET @row_number = 0');
        $data = \DB::table(\DB::raw('
                        (SELECT user_id,registration_category,total_distance,id,bib_number
                            FROM profiles 
                            WHERE registration_category = "'.$category.'"
                            AND deleted_at IS NULL 
                            AND bib_number IS NOT NULL
                            ORDER BY CAST(total_distance AS float) DESC) as profiles'
                ))
                ->selectRaw('(@row_number:=@row_number + 1) AS rank, user_id,total_distance,registration_category,id,bib_number')
                ->orderByRaw('CAST(total_distance AS float) DESC')
                ->get();
        @$collection = collect($data)->where('user_id',$userId)->pluck('rank');

        if(!empty($collection)) {
            return $collection[0];
        }
        
        return 0;
    }
}

?>