<?php 
namespace App\Helper;

use App\Complaint;
use App\Expedition;
use App\Profile;
use App\Result;
use App\User;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SelectHelper {
    public static function getGender() {
        return Constants::GENDER_LIST;
    }

    public static function getRegistrationCategory() {
        return Constants::REGISTRATION_CATEGORY_LIST;
    }

    public static function getRegistrationType() {
        return Constants::REGISTRATION_TYPE_LIST;
    }

    public static function getRegistrationStatus() {
        return Constants::REGISTRATION_STATUS_LISTS;
    }

    public static function getJerseySize() {
        return Constants::JERSEY_SIZE_LIST;
    }

    public static function getTicketPrice($user,$moneyFormat = false,$uniqueCode = false) {
        $amount = Constants::REGISTRATION_TYPE_PRICE_LIST[$user->registration_type][$user->include_jersey];
        if($uniqueCode) {
            $amount += Constants::UNIQUE_CODE_REGISTRATION; 
        }
        if($moneyFormat) {
            return GeneralHelper::moneyFormat($amount);
        }
        return $amount;
    }

    public static function getJerseyStockByUser($user) {
        return Constants::JERSEY_SIZE_STOCK_LIST[$user->jersey];
    }

    public static function getJerseyRemainingStockByKey($key) {
        return Constants::JERSEY_SIZE_STOCK_LIST[$key] - Profile::where('jersey',$key)->whereHas('user',function($q){$q->paidUser();})->count();
    }

    public static function getCommonStatusBooleanList() {
        return Constants::COMMON_STATUS_BOOLEAN;
    }

    public static function getNewPaymentNotification() {
        return User::where('status_payment',Constants::REGISTRATION_STATUS_ON_CONFIRMATION)->count();
    }

    public static function getNewResultNotification() {
        return Result::where('status',Constants::RESULT_STATUS_REQUEST_APPROVAL)->count();
    }

    public static function getNewReportNotification() {
        return Complaint::where('status',Constants::RESULT_STATUS_REQUEST_APPROVAL)->count();
    }

    public static function getRegistrationTypeByDate($date) {
        $site_settings = json_decode(\Storage::disk('public')->get('json/web.json'));
        if(Carbon::parse($site_settings->registrationDate)->addDays(5) > $date
            && User::where('registration_type',Constants::REGISTRATION_TYPE_EARLY)->count() < Constants::EARLYBIRD_QUOTA) {
            return Constants::REGISTRATION_TYPE_EARLY;
        }
        else return Constants::REGISTRATION_TYPE_NORMAL;
    }

    public static function getExpeditionList() {
        $all = Expedition::all();
        $array = [];
        foreach($all as $data) {
            $array[$data->id] = $data->expedition_name;
        }
        return $array;
    }
}

?>