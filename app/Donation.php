<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Donation extends Model
{
    use Notifiable;

    protected $table = 'donations';
    protected $fillable = [
        'transaction_code',
        'name',
        'email',
        'phone_number',
        'is_anonymous',
        'donation_amount',
        'status',
        'remark'
    ];
}
