<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';
    protected $fillable = [
        'activity_result',
        'distance',
        'duration',
        'screenshoot',
        'strava',
        'instagram',
        'notes',
        'status',
        'reject_remark',
        'profile_id'
    ];

    const FORM_VALIDATION = [
        'distance' => 'required',
        'duration' => 'required',
        'strava' => 'required',
    ];

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id', 'id');
    }
}
