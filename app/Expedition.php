<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expedition extends Model
{
    use SoftDeletes;

    protected $table = 'master_expeditions';

    public function users()
    {
        return $this->hasMany(User::class, 'expedition_id', 'id');
    }
}
