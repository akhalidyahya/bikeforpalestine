@component('mail::message')
# Hai {{$user->name}}!

Silahkan tekan tombol dibawah ini untuk melakukan proses reset password ya.

@component('mail::button', ['url' => $resetUrl ,'color'=>'magenta'])
Reset Password
@endcomponent

Terimakasih :)<br>
{{ config('app.name') }}
@endcomponent
