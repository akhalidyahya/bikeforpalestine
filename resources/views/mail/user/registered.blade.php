@component('mail::message')
# Hai {{$user->name}}!

Pendaftaran kamu telah berhasil! Silahkan lakukan aktivasi akun dengan menekan tombol dibawah ini!

@component('mail::button', ['url' => $activationUrl ,'color'=>'magenta'])
Aktivasi Akun
@endcomponent

Jangan lupa untuk segera melengkapi data dan melakukan pembayaran registrasi ya..

Terimakasih :)<br>
{{ config('app.name') }}
@endcomponent
