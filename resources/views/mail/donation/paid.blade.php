@component('mail::message')
# Hai {{@$donation->name}}!

Infomasi Donasi
<table class="info-table">
    <tr>
        <td>Kode Transaksi</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{$donation->transaction_code}}</td>
    </tr>
    <tr>
        <td>Nominal</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ @\App\Helper\GeneralHelper::moneyFormat(@$donation->donation_amount) }}</td>
    </tr>
    <tr>
        <td>Tanggal Submit</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ @\Carbon\Carbon::parse(@$donation->created_at)->format('d F Y H:i:s') }}</td>
    </tr>
</table>

Terimakasih atas konfirmasi donasinya!
Semoga Allah memberi keberkahan atas apa yang Anda berikan.

{{-- Terimakasih :)<br> --}}
{{ config('app.name') }}
@endcomponent