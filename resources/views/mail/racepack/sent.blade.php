@component('mail::message')
# Halo {{$user->name}}!

Racepack kamu telah dikirim loh! Untuk memeriksa posisi pengiriman racepack kamu, silahkan akses <a href="{{@$user->expedition->expedition_tracking_url}}">{{@$user->expedition->expedition_name}} tracking</a> (atau klik tombol dibawah) dan masukan nomor resi kamu: <b>{{@$user->resi}}</b>
<br>

@component('mail::button', ['url' => @$user->expedition->expedition_tracking_url ,'color'=>'magenta'])
Lacak Pengiriman
@endcomponent

Jika ada pertanyaan, silahkan hubungi: <br>
Runchaa 🙋‍♂️ : <a href="https://wa.me/6281289282134">wa.me/6281289282134⁣⁣</a> <br>
Runchaa 🙋‍♀️ : <a href="https://wa.me/628999207972">wa.me/628999207972⁣⁣</a> <br>
📷 <a href="https://instagram.com/rfpofficial_">instagram.com/rfpofficial_</a> <br>
<br>
"Apapun harta yang kalian infakkan maka Allah pasti akan menggantikannya, dan Dia a\dalah sebaik-baik pemberi rezeki." – QS. Saba’: 39 <br>
&#35;SavePalestine <br>
&#35;RunRideForPalestine
@endcomponent