@component('mail::message')
# Hai {{$user->name}}!

Pembayaran pendaftaran kamu ditolak :( , dengan catatan: <br>
<div class="reject-remark">
    "{{@$user->payment_remark}}"
</div>

Silahkan upload bukti yang valid ya..

Terimakasih :)<br>
{{ config('app.name') }}
@endcomponent
