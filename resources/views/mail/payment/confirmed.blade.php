@component('mail::message')
# Hai {{$user->name}}!

Pembayaran kamu telah kami konfirmasi!

<table class="info-table">
    <tr>
        <td>Kategori Event</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[$user->registration_category]}}</td>
    </tr>
    <tr>
        <td>Tiket Pendaftaran</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{\App\Utilities\Constants::REGISTRATION_TYPE_LIST[$user->registration_type]}}</td>
    </tr>
    <tr>
        <td>Include Racepack</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ \App\Utilities\Constants::COMMON_STATUS_BOOLEAN[$user->include_jersey] }}</td>
    </tr>
    <tr>
        <td>Total</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ \App\Helper\GeneralHelper::moneyFormat(\App\Utilities\Constants::REGISTRATION_TYPE_PRICE_LIST[$user->registration_type][$user->include_jersey]) }}</td>
    </tr>
    <tr>
        <td>Tanggal Upload Bukti</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ \Carbon\Carbon::parse($user->payment_date)->format('d F Y H:i:s') }}</td>
    </tr>
</table>

Terimakasih :)<br>
{{ config('app.name') }}
@endcomponent
