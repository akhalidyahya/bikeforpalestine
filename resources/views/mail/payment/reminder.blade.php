@component('mail::message')
# Salam {{$user->name}}!

Selangkah lagi untuk join di acara RUN&RIDE FOR PALESTINE 2022. Ayo jadi yang terdepan untuk  bergabung bersama kami! Jangan lupa sertakan kode unik ({{\App\Utilities\Constants::UNIQUE_CODE_REGISTRATION}}) juga ya!

Selesaikan pembayaranmu dengan transfer ke :

<div class="col-md-4 col-md-push-4 text-center">
    <img src="{{asset('assets/images/bsi-logo.png')}}" width="150px">
    <h4 for="">Transfer BSI</h4>
    <div class="m-t-10">
        Nomor Rekening <br>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">2020206673</h1>
                A.n. PEMUDA NUSANTARA JUARA
            </div>
        </div>
    </div>
</div>

<br>

{{-- <table class="info-table">
    <tr>
        <td>Kategori Event</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[$user->registration_category]}}</td>
    </tr>
    <tr>
        <td>Tiket Pendaftaran</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{\App\Utilities\Constants::REGISTRATION_TYPE_LIST[$user->registration_type]}}</td>
    </tr>
    <tr>
        <td>Include Racepack</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ \App\Utilities\Constants::COMMON_STATUS_BOOLEAN[$user->include_jersey] }}</td>
    </tr>
    <tr>
        <td>Total</td>
        <td style="padding-left:25px;padding-right:25px;">:</td>
        <td class="bold">{{ \App\Helper\GeneralHelper::moneyFormat(\App\Utilities\Constants::REGISTRATION_TYPE_PRICE_LIST[$user->registration_type][$user->include_jersey]) }}</td>
    </tr>
</table> --}}

Jika ada pertanyaan, silahkan hubungi: <br>
Runchaa 🙋‍♂️ : <a href="https://wa.me/6281289282134">wa.me/6281289282134⁣⁣</a> <br>
Runchaa 🙋‍♀️ : <a href="https://wa.me/628999207972">wa.me/628999207972⁣⁣</a> <br>
📷 <a href="https://instagram.com/rfpofficial_">instagram.com/rfpofficial_</a> <br>
<br>
"Apapun harta yang kalian infakkan maka Allah pasti akan menggantikannya, dan Dia a\dalah sebaik-baik pemberi rezeki." – QS. Saba’: 39 <br>
&#35;SavePalestine <br>
&#35;RunRideForPalestine
@endcomponent