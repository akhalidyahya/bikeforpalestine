<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delivery Address {{$method}}</title>
    <style>
        body, html {
            margin: 10px;
            padding: 0;
        }
        * {
            box-sizing: border-box;
        }
        
        .bordered {
            border: 1px solid black;
        }
        .detail {
            padding: 5px;
        }
        .col-6 {
            width: 50%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .clearfix:before,
        .clearfix:after,
        .row:before,
        .row:after {
            content: " ";
            display: table;
        }

        .clearfix:after,
        .row:after {
            clear: both;
        }

        table tr td {
            vertical-align: top;
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <div style="max-width: 1000px; margin-left:auto; margin:right:auto;">
        <div class="row">
            @foreach($data as $receiver)
            <div class="col-6">
                <table class="bordered" style="padding: 15px; width:7.9cm; height:3.9cm; overflow:hidden;">
                    <tr>
                        <td style="text-align: center;"><img style="margin-bottom: 15px" src="{{asset('assets/images/logo-black.png')}}" width="75px" alt="Logo"></td>
                    </tr>
                    <tr>
                        <td>
                            <strong style="font-size: 1.15em;text-transform:capitalize;">{{@$receiver->profiles[0]->name}} ({{@$receiver->profiles[0]->jersey}})</strong> <br>
                            <strong style="margin-top: 5px; margin-bottom:5px;">{{@$receiver->profiles[0]->phone_number}}</strong> <br>
                            <span style="font-size: 0.9em">{{ str_replace('.','. ',@$receiver->profiles[0]->address) }}</span>
                        </td>
                    </tr>
                </table>
            </div>
            @if(($loop->index+1)%2 == 0)
            <div class="clearfix"></div>
            @endif
            @if(($loop->index+1)%10 == 0)
            <div class="page-break"></div>
            @endif
            @endforeach
        </div>
    </div>
</body>
</html>