<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RFP BIB {{$data->bib_number}}</title>
    <style>
        h1 {
            position: absolute;
            top: 20px;
            left: 50%;
            transform: translateX(-50%);
            font-size: 10 em;
            font-family: Arial, Helvetica, sans-serif;
        }

        p {
            position: absolute;
            top: 285px;
            left: 50%;
            transform: translateX(-50%);
            font-size: 1.3 em;
            font-family: Arial, Helvetica, sans-serif;
        }
        
        img {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <h1>{{$data->bib_number}}</h1>
    <p>{{$data->name}}</p>
    <img src="{{asset('admin/images/bib-template.jpg')}}" alt="" width="100%">
</body>
</html>