<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sertifikat {{$data->bib_number}}</title>
    <style>
        html {
            margin: 0;
            padding: 0;
        }

        h1 {
            position: absolute;
            top: 44%;
            left: 9%;
            transform: translateY(-44%);
            transform: translateX(-9%);
            font-size: 2em;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            /* background: rgba(250, 144, 144, 0.4); */
            width: 450px;
        }

        .bib {
            position: absolute;
            top: 51%;
            transform: translateY(-51%);
            left: 9%;
            transform: translateX(-9%);
            font-size: 1.3 em;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            width: 450px;
        }

        .wording {
            position: absolute;
            top: 58%;
            transform: translateY(-65%);
            left: 9%;
            transform: translateX(-9%);
            font-size: 1.3 em;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            width: 450px;
        }

        .finish {
            position: absolute;
            top: 65%;
            transform: translateY(-65%);
            left: 9%;
            transform: translateX(-9%);
            font-size: 1.3 em;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            width: 450px;
        }

        .rank {
            position: absolute;
            top: 70%;
            transform: translateY(-65%);
            left: 9%;
            transform: translateX(-9%);
            font-size: 1.3 em;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            width: 450px;
        }
        
        img {
            position: absolute;
            border: 1px solid #000;
            z-index: -1;
        }

        .bold {
            font-weight: bold;
        }

        .capital {
            text-transform: uppercase;
        }
    </style>
</head>
<body>
    <h1>{{$data->name}}</h1>
    <p class="bib">BIB no. {{$data->bib_number}}</p>
    @if($data->finish_status == \App\Utilities\Constants::FINISH_STATUS_FINISH)
    <p class="wording bold capital">OFFICIALLY COMPLETED {{\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[$data->registration_category]}} CATEGORY</p>
    <p class="finish">Finish {{$data->total_distance_km_format}} KM</p>
    @else
    <p class="wording">
        Thank you for your participation in the event <br>
        Run & Ride for Palestine 2022
    </p>
    <p class="finish bold">You are the Hero for Palestine.</p>
    @endif
    @if($data->currentRank <= 5)
    <p class="rank bold">Rank {{$data->currentRank}}</p>
    @endif
    @if($data->registration_category == \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE)
    <img src="{{asset('admin/images/sertif-ride.png')}}?v=1" alt="" width="100%">
    @else
    <img src="{{asset('admin/images/sertif-run.png')}}?v=1" alt="" width="100%">
    @endif
</body>
</html>