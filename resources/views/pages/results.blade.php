@extends('layouts.base')
@section('title','Data Submit Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Manajemen Aproval Submit</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Data Submit Peserta</h3>
                </div>
                <div class="body">
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#bike" data-toggle="tab">BIKE</a></li>
                        <li role="presentation"><a href="#run" data-toggle="tab">RUN</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="bike">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right m-b-15">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input id="s_search_{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE }}" type="text" class="form-control" onchange="filter()" placeholder="Cari Data Submit (nama, bib)">
                                            </div>
                                            <div class="col-md-8 text-right">
                                                <button type="button" class="btn btn-primary waves-effect btn-sm float-right" onclick="openFilter()">
                                                    <i class="material-icons">filter_list</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Tanggal</th>
                                                    <th scope="col">BIB</th>
                                                    <th scope="col">Nama</th>
                                                    <th scope="col">Jarak (km)</th>
                                                    <th scope="col">Waktu</th>
                                                    <th scope="col">Link</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Keterangan</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="run">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right m-b-15">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input id="s_search_{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_RUN }}" type="text" class="form-control" onchange="filter()" placeholder="Cari Data Submit (nama, bib)">
                                            </div>
                                            <div class="col-md-8 text-right">
                                                <button type="button" class="btn btn-primary waves-effect btn-sm float-right" onclick="openFilter()">
                                                    <i class="material-icons">filter_list</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id='myTable2' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Tanggal</th>
                                                    <th scope="col">BIB</th>
                                                    <th scope="col">Nama</th>
                                                    <th scope="col">Jarak (km)</th>
                                                    <th scope="col">Waktu</th>
                                                    <th scope="col">Link</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Keterangan</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.results.modal-filter-result')
@include('pages.part.results.modal-reject')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('result.dataTableActivity') }}",
            'data': function(d) {
                d.registration_category = "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE }}";
                d.search = $('#s_search_{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE }}').val();
                d.start_date = $('#s_start_date').val();
                d.end_date = $('#s_end_date').val();
            }
        },
        'dataType': 'json',
        'searching': false,
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'activity_date', name: 'activity_date'},
            {data: 'bib_number', name: 'bib_number'},
            {data: 'name', name: 'name'},
            {data: 'distance', name: 'distance'},
            {data: 'duration', name: 'duration'},
            {data: 'link', name: 'link',orderable:false,searchable:false},
            {data: 'status', name: 'status',orderable:false,searchable:false},
            {data: 'notes', name: 'notes',orderable:false},
            {data: 'action', name: 'action'},
            // {data: 'account', name: 'account'},
        ],
        'info': true,
        'autoWidth': false
    });

    var table2 = $('#myTable2').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('result.dataTableActivity') }}",
            'data': function(d) {
                d.registration_category = "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_RUN }}";
                d.search = $('#s_search_{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_RUN }}').val();
                d.start_date = $('#s_start_date').val();
                d.end_date = $('#s_end_date').val();
            }
        },
        'dataType': 'json',
        'searching': false,
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'activity_date', name: 'activity_date'},
            {data: 'bib_number', name: 'bib_number'},
            {data: 'name', name: 'name'},
            {data: 'distance', name: 'distance'},
            {data: 'duration', name: 'duration'},
            {data: 'link', name: 'link',orderable:false,searchable:false},
            {data: 'status', name: 'status',orderable:false,searchable:false},
            {data: 'notes', name: 'notes',orderable:false},
            {data: 'action', name: 'action'},
            // {data: 'account', name: 'account'},
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.ajax.reload();
        table2.ajax.reload();
    }

    function reject(id,status) {
        $('#modalReject [name="id"]').val(id);
        $('#modalReject [name="status"]').val(status);
        $('#modalReject').modal('show');
    }

    function confirmReject() {
        changeStatus($('#modalReject [name="id"]').val(),$('#modalReject [name="status"]').val(),true);
    }

    function changeStatus(resultId,paramStatus,reject = false) {
        var url = "{{ route('result.changeStatus',['id'=>':id']) }}";
        url = url.replace(':id',resultId);
        let dataForm = {
            status : paramStatus,
            notes : ''
        };
        if(reject) {
            dataForm.notes = $('#modalReject [name="notes"]').val();
        }
        swal({
            title: "Status akan dirubah?",
            text: "Dengan ini status akan diubah",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done)=>{
            if(done){
                $.ajax({
                    url: url,
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: dataForm,
                    success: function(data){
                        if(data.success){
                            swal('Berhasil',data.message,'success');
                            $('#modalReject').modal('hide');
                            table.ajax.reload();
                            table2.ajax.reload();
                        } else {
                            swal('Gagal',data.message,'error');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }

    function openFilter() {
        $('.modal-title').text('Filter Data');
        $('#filterModal').modal('show');
    }
</script>
@endpush
@endsection