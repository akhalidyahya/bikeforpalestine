@extends('layouts.base')
@section('title','Profil Peserta')
@push('customCSS')
<style>
    #copy {
        cursor: pointer;
    }

    #bank1 {
        font-size: 1.3em;
        border: none !important;
        outline: none !important;
        background-color: transparent !important;
        font-weight: bolder;
    }

    input[type=file] {
        background-color: #dedede !important;
        padding: 5px !important;
    }
</style>
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>Website Configuration</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @if (session('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session('success')}}</strong>
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-warning alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session('message')}}</strong>
                        </div>
                    @endif
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                        <li role="presentation" class=""><a href="#tab_2" data-toggle="tab">FAQ</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tab_1">
                            <div class="row">
                                <div class="col-md-6 col-md-push-3">
                                    <h4 for="" class="m-b-15 col-blue text-center">General</h4>
                                    <form action="">

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="tab_2">
                            <div class="row">
                                <div class="col-md-6 col-md-push-3">
                                    <h4 for="" class="m-b-15 col-blue">FAQ</h4>
                                    <form id="profile" action="#">
                                        @foreach($faq as $data)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="">Pertanyaan</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">question_answer</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="question" placeholder="Contoh : Apakah bumi itu bulat?" required autofocus autocomplete="off" value="{{@$data->question}}">
                                                    </div>
                                                </div>
                                                <label for="">Jawaban</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">assignment</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <textarea class="form-control" name="answer" placeholder="Contoh: Ya tentu saja!" rows="5" required autocomplete="off">{!! @$data->answer !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        @endforeach
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script>
    
</script>
@endpush
@endsection