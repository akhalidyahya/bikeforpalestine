<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="myForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="method" value="">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="name" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Nama Peserta
                                    </label>
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="age" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Umur
                                    </label>
                                </div>
                                <div id="age_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="address" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Alamat
                                    </label>
                                </div>
                                <div id="age_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="phone_number" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        No Whatsapp
                                    </label>
                                </div>
                                <div id="age_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Jenis kelamin</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="gender" required>
                                        <option value="">- Pilih Gender -</option>
                                        @foreach(\App\Helper\SelectHelper::getGender() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="registration_category_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Ukuran Jersey</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="jersey" required>
                                        <option value="">- Pilih Ukuran Jersey -</option>
                                        @foreach(\App\Helper\SelectHelper::getJerseySize() as $key => $value)
                                        <?php
                                        $stock = \App\Helper\SelectHelper::getJerseyRemainingStockByKey($key);
                                        ?>
                                        <option @if(@$data->jersey == $key) selected @endif value="{{$key}}" @if($stock < 1) disabled @endif>{{$value}} (Stok: {{ $stock }})</option>
                                        @endforeach
                                    </select>
                                    <div id="registration_category_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Email Peserta
                                    </label>
                                </div>
                                <div id="email_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" name="password" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Password Akun
                                    </label>
                                </div>
                                <div id="password_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Kategori Event</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="registration_category" required>
                                        <option value="">- Pilih Kategori Event -</option>
                                        @foreach(\App\Helper\SelectHelper::getRegistrationCategory() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="registration_category_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Tiket Pendaftaran</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="registration_type" required>
                                        <option value="">- Pilih Tiket Pendaftaran -</option>
                                        @foreach(\App\Helper\SelectHelper::getRegistrationType() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="registration_type_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="community_name" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Nama Komunitas
                                    </label>
                                </div>
                                <div id="community_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Include Racepack</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="include_jersey" required>
                                        @foreach(\App\Helper\SelectHelper::getCommonStatusBooleanList() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="include_jersey_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Status Bayar</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="status_payment" required>
                                        @foreach(\App\Helper\SelectHelper::getRegistrationStatus() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="status_payment_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveData()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>