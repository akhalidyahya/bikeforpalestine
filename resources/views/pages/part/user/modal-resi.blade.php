<div id="modalResi" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formResi" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <label for="">Pakai Ekspedisi?</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="use_expedition" required onchange="isManual()">
                                        @foreach(\App\Helper\SelectHelper::getCommonStatusBooleanList() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="use_expedition_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Ekspedisi</label>
                            <div class="input-group">
                                <div class="form-group form-float">
                                    <select class="form-control show-tick" name="expedition_id" >
                                        @foreach(\App\Helper\SelectHelper::getExpeditionList() as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <div id="expedition_id_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="resi" class="form-control" autocomplete="off" >
                                    <label class="form-label">
                                        Nomor Resi
                                    </label>
                                </div>
                                <div id="resi_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="delivery_message" class="form-control" autocomplete="off">
                                    <label class="form-label">
                                        Pesan Tambahan
                                    </label>
                                </div>
                                <div id="delivery_message_error"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveDataResi()">Kirim</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>