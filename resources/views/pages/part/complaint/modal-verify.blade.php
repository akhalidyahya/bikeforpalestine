<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="myForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="status" value="{{\App\Utilities\Constants::RESULT_STATUS_APPROVED}}">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="activity_id" class="form-control" autocomplete="off">
                                    <label class="form-label">
                                        Activity ID
                                    </label>
                                </div>
                                <div id="activity_id_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="distance" class="form-control" autocomplete="off" required placeholder="contoh: 1200">
                                    <label class="form-label">
                                        Jarak (DALAM METER)*
                                    </label>
                                </div>
                                <div id="distance_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="moving_time" class="form-control" autocomplete="off" required placeholder="contoh: 3200">
                                    <label class="form-label">
                                        Waktu (DALAM DETIK)*
                                    </label>
                                </div>
                                <div id="moving_time_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line" id="custom_bs_datepicker_container">
                                    <input type="text" name="start_date" class="form-control" autocomplete="off" required>
                                    <label class="form-label">
                                        Tanggal (Sesuai dengan yang ada di activity)*
                                    </label>
                                </div>
                                <div id="start_date_error"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="payment_remark">
                                <label for="payment_remark">Catatan</label>
                                <textarea name="remark" class="form-control grey-mode"></textarea>
                                <div id="payment_remark_error" class="help-block help-block-error"> </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveData()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>