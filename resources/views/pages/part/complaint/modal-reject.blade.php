<!-- Modal -->
<div class="ptsi-modal modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Alasan Tolak Data</h4>
            </div>
            <div class="modal-body">
                <form id="myFormReject" class="form-horizontal" action="#" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="status" value="{{\App\Utilities\Constants::RESULT_STATUS_REJECT}}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="remark">
                                <label for="remark">Alasan</label>
                                <textarea name="remark" class="form-control grey-mode"></textarea>
                                <div id="remark_error" class="help-block help-block-error"> </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="submitReject" onclick="saveData(true)" type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<!-- End of Modal -->
