<div id="filterModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="myForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="method" value="">
                    <div class="row clearfix">
                        {{--<div class="col-md-12">
                            <label for="">Kategori Event</label>
                            <div class="form-group form-float">
                                <select class="form-control show-tick grey-mode" id="s_registration_category">
                                    <option value="">--- Semua Event ---</option>
                                    @foreach(\App\Helper\SelectHelper::getRegistrationCategory() as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Paket Registrasi</label>
                            <div class="form-group form-float">
                                <select class="form-control show-tick grey-mode" id="s_registration_type">
                                    <option value="">--- Semua Tiket ---</option>
                                    @foreach(\App\Helper\SelectHelper::getRegistrationType() as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>--}}
                        <div class="col-md-12">
                            <label for="">Status Pembayaran</label>
                            <div class="form-group form-float">
                                <select class="form-control show-tick grey-mode" id="s_registration_status">
                                    <option value="">--- Semua Status ---</option>
                                    @foreach(\App\Helper\SelectHelper::getRegistrationStatus() as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="filter()">Filter</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>