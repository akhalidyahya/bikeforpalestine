<div id="filterModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="myForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="method" value="">
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <label class="form-label">Tanggal Mulai</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="s_start_date" id="s_start_date" class="form-control custom-datepicker" autocomplete="off">
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Tanggal Sampai</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="s_end_date" id="s_end_date" class="form-control custom-datepicker" autocomplete="off">
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        {{--<div class="col-md-12">
                            <label for="">Peserta</label>
                            <div class="form-group form-float">
                                <select class="form-control show-tick" name="s_profile" id="s_profile">
                                    <option value="">--- Semua Peserta ---</option>
                                    @foreach(\App\Helper\SelectHelper::getRegistrationType() as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>--}}
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="filter()">Filter</button>
                <button type="button" class="btn btn-secondary" onclick="$('#myForm')[0].reset()">Reset</button>
            </div>
        </div>
    </div>
</div>