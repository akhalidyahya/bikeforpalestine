@extends('layouts.base')
@section('title','Data Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Manajemen Peserta</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Data</h3>
                </div>
                <div class="body">
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#akun" data-toggle="tab">AKUN</a></li>
                        {{--<li role="presentation"><a href="#peserta" data-toggle="tab">PESERTA</a></li>--}}
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="akun">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right m-b-15">
                                        <button type="button" class="btn btn-primary waves-effect m-r-5" onclick="openFilter()">
                                            {{-- <i class="material-icons">filter_list</i> --}}
                                            <i class="fa fa-filter"></i>
                                        </button>
                                        <button type="button" class="btn btn-primary waves-effect" onclick="addData()"> <i class="fa fa-plus"></i> Tambah Akun</button> 
                                        <button type="button" class="btn btn-primary waves-effect" href="#" onclick="delivery('ALL')"> <i class="fa fa-download"></i> Label Pengiriman</button>
                                        <button type="button" class="btn btn-primary waves-effect" href="#" onclick="exportDelivery()"> <i class="fa fa-file-excel-o"></i> Export Pengiriman</button>
                                        <button type="button" class="btn btn-primary waves-effect" href="#" onclick="exportProfile()"> <i class="fa fa-file-excel-o"></i> Export Peserta</button>
                                        {{-- <button type="button" class="btn btn-primary waves-effect" href="#" onclick="downloadDataPeserta()"> <i class="fa fa-download"></i> Data Peserta</button> --}}
                                    </div>
                                    <div class="table-responsive">
                                        <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Nama Akun</th>
                                                    <th scope="col">Email Akun</th>
                                                    <th scope="col">Kategori</th>
                                                    <th scope="col">Tiket Pendaftaran</th>
                                                    <th scope="col">Include Jersey</th>
                                                    <th scope="col">Status Pembayaran</th>
                                                    <th scope="col">Resi</th>
                                                    <th scope="col">Finish</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div role="tabpanel" class="tab-pane fade" id="peserta">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right m-b-15">
                                        <button type="button" class="btn btn-primary waves-effect btn-xs m-r-5" onclick="openFilter()">
                                            <i class="material-icons">filter_list</i>
                                        </button>
                                        <button type="button" class="btn btn-primary waves-effect btn-xs m-r-5" onclick="openFilter()">
                                            <i class="material-icons">file_download</i>
                                        </button>
                                        <!-- <a type="button" class="btn btn-primary" href="#">Unduh Data User</a> -->
                                    </div>
                                    <div class="table-responsive">
                                        <table id='myTable2' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Nama Peserta</th>
                                                    <th scope="col">BIB</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Jenis Kelamin</th>
                                                    <th scope="col">No Ponsel</th>
                                                    <th scope="col">Kategori</th>
                                                    <th scope="col">Tiket Pendaftaran</th>
                                                    <th scope="col">Include Jersey</th>
                                                    <th scope="col">Jersey</th>
                                                    <!-- <th scope="col">Nama Akun</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.user.modal-participant')
@include('pages.part.user.modal-resi')
@include('pages.part.user.modal-filter-user')
@include('pages.part.user.modal-detail-profile')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('participants.accountDataTable') }}",
            'data': function(d) {
                d.registration_category = $('#s_registration_category').val();
                d.registration_type     = $('#s_registration_type').val();
                d.include_jersey        = $('#s_include_jersey').val();
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'registration_category', name: 'registration_category',orderable:false,searchable:false},
            {data: 'registration_type', name: 'registration_type',orderable:false,searchable:false},
            {data: 'include_jersey',name: 'include_jersey',orderable: false,searchable: false},
            {data: 'status_payment', name: 'status_payment',orderable:false,searchable:false},
            {data: 'resi', name: 'resi'},
            {data: 'finish', name: 'finish'},
            {data: 'action_data', name: 'action_data', orderable: false, searchable: false},

        ],
        'info': true,
        'autoWidth': false
    });

    {{-- var table2 = $('#myTable2').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('participants.profileDataTable') }}",
            'data': function(d) {
                d.registration_category = $('#s_registration_category').val();
                d.registration_type     = $('#s_registration_type').val();
                d.include_jersey        = $('#s_include_jersey').val();
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'name', name: 'name'},
            {data: 'bib_number', name: 'bib_number'},
            {data: 'email', name: 'email'},
            {data: 'gender', name: 'gender'},
            {data: 'phone_number', name: 'phone_number'},
            {data: 'registration_category', name: 'registration_category',orderable:false,searchable:false},
            {data: 'registration_type', name: 'registration_type',orderable:false,searchable:false},
            {data: 'include_jersey',name: 'include_jersey',orderable: false,searchable: false},
            {data: 'jersey', name: 'jersey',orderable:false,searchable:false},
            // {data: 'account', name: 'account'},
        ],
        'info': true,
        'autoWidth': false
    }); --}}

    function filter() {
        console.log($('#s_registration_category').val());
        table.ajax.reload();
        table2.ajax.reload();
    }

    function addData() {
        $('.modal-title').text('Tambah Akun');
        $('[name="method"]').val('add');
        $('[name="id"]').val(0);
        $('#myModal form')[0].reset();
        $('.form-line').removeClass('focused');
        $('#myModal').modal('show');
    }

    function inputResi(id) {
        $('.modal-title').text('Input Resi');
        $('[name="id"]').val(id);
        $('#modalResi form')[0].reset();
        $('.form-line').removeClass('focused');
        $('#modalResi').modal('show');
    }

    function saveDataResi() {
        swal({
            title: "Anda yakin?",
            text: "Pastikan nomor resi sudah benar (bila menggunakan ekspedisi)",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done) => {
            if (done) {
                var url = "{{ route('participants.sendResi') }}";

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: $('#formResi').serialize(),
                    beforeSend: function() {
                        swal({text:'mengirim...',button:false});
                    },
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: 'Berhasil Simpan Data',
                                text: data.message,
                                icon: 'success',
                                timer: '3000'
                            }).then(() => {
                                $('#modalResi').modal('hide');
                                table.ajax.reload();
                            });
                        } else {
                            swal({
                                title: 'Gagal Simpan Data',
                                text: data.message,
                                icon: 'error',
                                timer: '3000'
                            }).then(() => {
                                $('#modalResi').modal('hide');
                                table.ajax.reload();
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '4000'
                        }).then(() => {
                            table.ajax.reload();
                        });
                    }
                });
            }
        });
    }

    function saveData() {
        $('#myForm').validate({
            highlight: function(input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function(input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function(error, element) {
                $(element).parents('.input-group').append(error);
            }
        });

        var url = "{{ route('participants.saveAccount') }}";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#myForm').serialize(),
            success: function(data) {
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#myModal').modal('hide');
                        table.ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#myModal').modal('hide');
                        table.ajax.reload();
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    table.ajax.reload();
                });
            }
        });
    }

    function editData(id) {
        $('.modal-title').text('Edit data akun');
        $('.form-line').removeClass('focused');
        $('[name="method"]').val('edit');
        $('[name="id"]').val(id);
        $('#myModal form')[0].reset();
        var url = "{{route('participants.getAccountById')}}";
        $.ajax({
            url: url,
            type: 'GET',
            data: {
                id: id
            },
            success: function(data) {
                $('.form-line').addClass('focused');
                $('[name="name"]').val(data.name);
                $('[name="email"]').val(data.email);
                $('[name="ttl"]').val(data.ttl);
                $('[name="jurusan"]').val(data.jurusan);
                $('[name="jenis_kelamin"]').val(data.jenis_kelamin);
                $('#myModal').modal('show');
            }
        });
    }

    function deleteData(id) {
        var url = "{{ route('participants.delete') }}";
        swal({
            title: "Anda yakin?",
            text: "Dengan ini data akan dihapus",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done) => {
            if (done) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data.success) {
                            swal('Berhasil', data.message, 'success');
                            table.ajax.reload();
                        } else {
                            swal('Gagal', data.message, 'warning');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }

    function openFilter() {
        $('.modal-title').text('Filter Data');
        $('#filterModal').modal('show');
    }

    function profileData(id,name) {
        $('#detailProfileModal .modal-title').text('List Peserta Pada Akun '+name+'');
        $('#list-profile').html('');

        var url = "{{route('profile.getProfileByAccountId',['id'=>''])}}/"+id;
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                let html = '';
                data.forEach((value,index)=>{
                    html+='<tr>';
                    html+='<td>';html+=index+1;html+='</td>';
                    html+='<td>'+(value.bib_number || '')+'</td>';
                    html+='<td>'+(value.name || 'Belum Diisi')+'</td>';
                    html+='<td>'+(value.address || 'Belum Diisi')+'</td>';
                    html+='<td>'+(value.email || 'Belum Diisi')+'</td>';
                    html+='<td>'+(value.gender || 'Belum Diisi')+'</td>';
                    html+='<td>'+(value.age || 'Belum Diisi')+'</td>';
                    html+='<td><a target="_blank" href="'+value.phone_number.replace('0','https://wa.me/62')+'">'+(value.phone_number || 'Belum Diisi')+'</td>';
                    html+='<td>'+(value.jersey || '-')+'</td>';
                    html+='<td>'+(value.user.resi || '-')+'</td>';
                    html+='<td>'+(value.user.expedition.expedition_name || '-')+'</td>';
                    html+='<td>'+(value.user.delivery_message || '-')+'</td>';
                    html+='</tr>';
                });
                $('#list-profile').html(html);
                $('#detailProfileModal').modal('show');
            }
        });
    }

    function delivery(method,id = 0) {
        if(id != 0) {
            location.href="{{route('participants.deliveryInformation')}}?method="+method+'&id='+id;
        } else {
            location.href="{{route('participants.deliveryInformation')}}?method="+method;
        }
    }

    function exportDelivery() {
        location.href="{{route('export.delivery')}}"
    }

    function exportProfile() {
        location.href="{{route('export.profile')}}"
    }

    function isManual() {
        if($('[name="use_expedition"]').val() == 'NO') {
            $('[name="expedition_id"]').prop('disabled',true).addClass('grey-mode');
            $('[name="resi"]').prop('disabled',true).addClass('grey-mode');
        } else {
            $('[name="expedition_id"]').prop('disabled',false).removeClass('grey-mode');
            $('[name="resi"]').prop('disabled',false).removeClass('grey-mode');
        }
    }
</script>
@endpush
@endsection