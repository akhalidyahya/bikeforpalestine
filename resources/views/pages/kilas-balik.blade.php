﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Kilas Balik | Run & Ride For Palestine</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    {{-- <link href="{{asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">

    <!-- Font Awesome -->
    <link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

    
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('admin/css/themes/all-themes.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/owl-carousel/assets/owl.carousel.min.css') }}?v=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/owl-carousel/assets/owl.theme.default.css') }}?v=1">

    <link href="{{asset('admin/css/kilas-balik.css')}}" rel="stylesheet">
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <div class="main-wrapper owl-carousel owl-theme">
        <div class="item kilas-balik-item bg-1">
            <h1 class="text-center m-t-65">Kilas Balik RFP 2022</h1>
            <p class="text-center main-text m-t-50">
                Yuk lihat pencapaianmu di Run and Ride for Palestine 2022!
            </p>
            @if($user->registration_category == \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE)
            <img src="{{asset('assets/images/mascot_1.png')}}" class="img-fluid mirror" alt="First Vector Graphic">
            @else
            <img src="{{asset('assets/images/mascot_2.png')}}" class="img-fluid mirror mascot-run" alt="First Vector Graphic">
            @endif
            <p class="text-center m-t-50 main-text">
                Geser untuk baca <br> selengkapnya
            </p>
            <p class="text-center animate__animated animate__shakeX animate__infinite animate__slower">
                <i class="fa fa-arrow-right fa-lg"></i>
            </p>
            <img src="{{asset('assets/images/logo-white.png')}}" class="img-fluid rfp-logo" alt="First Vector Graphic">
        </div>
        <div class="item kilas-balik-item bg-1 text-center">
            <p class="m-t-65">
                Hai <span class="bold">{{$user->name}}</span>!
            </p>
            <p class="main-text">
                Terimakasih telah mengikuti serangkaian kegiatan Run and Ride for Palestine 2022!
            </p>
            <p class="main-text">
                Selama periode challenge, kamu berhasil menempuh <span class="bold highlight">{{$user->profiles[0]->total_distance_km_format}} KM!</span>
            </p>
            <p class="main-text">
                <img src="{{asset('assets/images/aqsha.png')}}" class="img-fluid mirror mascot-run" alt="First Vector Graphic">
                Wow itu sama saja dengan mengelilingi Masjid Al-Aqsha sebanyak <span class="bold highlight">{{$user->profiles[0]->round_aqsha}} Kali!</span>
            </p>
            {{-- <p class="text-center animate__animated animate__shakeX animate__infinite animate__slower">
                <i class="fa fa-arrow-right fa-lg"></i>
            </p> --}}
            <img src="{{asset('assets/images/logo-white.png')}}" class="img-fluid rfp-logo" alt="First Vector Graphic">
        </div>
        <div class="item kilas-balik-item bg-1 text-center">
            <p class="main-text m-t-65">
                {{$user->name}}, kamu berhasil mencapai itu semua dalam waktu <span class="bold highlight">{{$user->profiles[0]->total_indonesia_format}}!</span>
            </p>
            <p class="main-text m-t-50">
                <img src="{{asset('assets/images/medal.png')}}" class="img-fluid mirror mascot-run" alt="First Vector Graphic">
                Dan saat ini kamu berada di peringkat <span class="bold highlight">{{\App\Services\CommonService::getUserRank($user->registration_category,$user->id)}}</span> dari {{\App\Services\CommonService::getTotalParticipant($user->registration_category)}} peserta kategori {{\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[$user->registration_category]}}.
            </p>
            {{-- <p class="text-center animate__animated animate__shakeX animate__infinite animate__slower">
                <i class="fa fa-arrow-right fa-lg"></i>
            </p> --}}
            <img src="{{asset('assets/images/logo-white.png')}}" class="img-fluid rfp-logo" alt="First Vector Graphic">
        </div>
        <div class="item kilas-balik-item bg-1 text-center">
            <p class="main-text m-t-65">
                <h1>Terimakasih <br> {{$user->name}}</h1>
            </p>
            <p class="main-text m-t-15">
                Perjuanganmu dalam menyelesaikan challenge merupakan bagian dari langkah untuk menggaungkan "kesadaran" tentang Palestina di Indonesia, juga dunia.
            </p>
            @if($user->registration_category == \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE)
            <img src="{{asset('assets/images/mascot_1.png')}}" class="img-fluid mirror" alt="First Vector Graphic">
            @else
            <img src="{{asset('assets/images/mascot_2.png')}}" class="img-fluid mirror mascot-run" alt="First Vector Graphic">
            @endif
            <p class="main-text m-t-10">
                Sampai jumpa <br> di event selanjutnya!
            </p>
            <img src="{{asset('assets/images/logo-white.png')}}" class="img-fluid rfp-logo" alt="First Vector Graphic">
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Tooltip and Popover -->
    <script src="{{asset('admin/js/pages/ui/tooltips-popovers.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>

    <!-- Sweetalert -->
    <script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{asset('admin/plugins/autosize/autosize.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{asset('admin/plugins/momentjs/moment.js')}}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <!-- Datetime picker custom js -->
    <script src="{{asset('admin/js/pages/forms/basic-form-elements.js')}}"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{asset('admin/plugins/bootstrap-notify/bootstrap-notify.js')}}"></script>
    
    <script src="{{asset('admin/js/pages/ui/notifications.js')}}"></script>
    
    <!-- Demo Js -->
    <script src="{{asset('admin/js/demo.js')}}"></script>
    <script src="{{ asset('assets/js/owl-carousel-custom.js') }}?v=1"></script>
    <script>
        $(".owl-carousel").owlCarousel({
            loop: false,
            margin: 30,
            // nav: true,
            pagination: true,
            dots: true,
            responsiveClass:true,
            autoplay: false,
            autoplayTimeout: 10000,
            autoplaySpeed: 2000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            },
            profile: true,
            closeUrl: '{{route("dashboard.index")}}',
        });
    </script>
</body>

</html>
