@extends('layouts.base')
@section('title','Leaderboard')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Leaderboard</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Leaderboard</h3>
                </div>
                <div class="body">
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#bike" data-toggle="tab">BIKE</a></li>
                        <li role="presentation"><a href="#run" data-toggle="tab">RUN</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="bike">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right m-b-15">
                                        <!-- <button type="button" class="btn btn-primary waves-effect btn-xs m-r-5" onclick="openFilter()">
                                            <i class="material-icons">filter_list</i>
                                        </button> -->
                                    </div>
                                    <div class="table-responsive">
                                        <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jenis Kelamin</th>
                                                    <th>Nama</th>
                                                    <th>Jarak (meter)</th>
                                                    <th>Waktu (detik)</th>
                                                    <th>Pace (menit/km)</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="run">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-right m-b-15">
                                        <!-- <button type="button" class="btn btn-primary waves-effect btn-xs m-r-5" onclick="openFilter()">
                                            <i class="material-icons">filter_list</i>
                                        </button> -->
                                    </div>
                                    <div class="table-responsive">
                                        <table id='myTable2' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Nama</th>
                                                <th>Jarak (meter)</th>
                                                <th>Waktu (detik)</th>
                                                <th>Pace (menit/km)</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.user.modal-participant')
@include('pages.part.user.modal-filter-user')
@include('pages.part.user.modal-detail-profile')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('leaderboard.dataTableActivity') }}",
            'data': function(d) {
                d.registration_category = "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex',name: 'DT_RowIndex',searchable:false,orderable:false},
            {data: 'gender',name: 'gender'},
            {data: 'name',name: 'name'},
            {data: 'total_distance',name: 'total_distance'},
            {data: 'total_time',name: 'total_time'},
            {data: 'pace',name: 'pace',searchable:false,orderable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    var table2 = $('#myTable2').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('leaderboard.dataTableActivity') }}",
            'data': function(d) {
                d.registration_category = "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_RUN }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex',name: 'DT_RowIndex',searchable:false,orderable:false},
            {data: 'gender',name: 'gender'},
            {data: 'name',name: 'name'},
            {data: 'total_distance',name: 'total_distance'},
            {data: 'total_time',name: 'total_time'},
            {data: 'pace',name: 'pace',searchable:false,orderable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.ajax.reload();
        table2.ajax.reload();
    }

</script>
@endpush
@endsection