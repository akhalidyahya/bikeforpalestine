@extends('layouts.base')
@section('title','Dasbor')
@push('customCSS')
<style>
    #copy {
        cursor: pointer;
    }

    #bank1 {
        font-size: 1.3em;
        border: none !important;
        outline: none !important;
        background-color: transparent !important;
        font-weight: bolder;
    }

    input[type=file] {
        background-color: #dedede !important;
        padding: 5px !important;
    }
</style>
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>Dasbor</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-indigo">
                    <i class="material-icons">phone_android</i>
                </div>
                <div class="content">
                    <div class="text">PENDAFTAR</div>
                    <div class="number count-to" data-from="0" data-to="{{$account_count}}" data-speed="1000" data-fresh-interval="20">{{$account_count}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-blue">
                    <i class="material-icons">tag_faces</i>
                </div>
                <div class="content">
                    <div class="text">DIKONFIRMASI</div>
                    <div class="number count-to" data-from="0" data-to="{{$paid_account_count}}" data-speed="1000" data-fresh-interval="20">{{$paid_account_count}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-deep-purple">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text">PENDAFTARAN</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_revenue}}" data-speed="1000" data-fresh-interval="20">{{\App\Helper\GeneralHelper::moneyFormat($total_revenue)}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-cyan">
                    <i class="material-icons">directions_bike</i>
                </div>
                <div class="content">
                    <div class="text">DATA SUBMIT</div>
                    <div class="number count-to" data-from="0" data-to="{{$data_submit}}" data-speed="1000" data-fresh-interval="20">{{$data_submit}}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Selamat Datang {{\Auth::user()->name}}!</h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{asset('assets/images/main-poster.jpg')}}?v=2" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script src="{{asset('admin/plugins/jquery-countto/jquery.countTo.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
<script src="{{asset('admin/js/pages/widgets/infobox/infobox-1.js')}}"></script>
<script>
    
</script>
@endpush
@endsection