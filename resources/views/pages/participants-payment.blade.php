@extends('layouts.base')
@section('title','Pembayaran Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Manajemen Pembayaran</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-right m-b-15">
                                <button type="button" class="btn btn-primary waves-effect btn-xs m-r-5" onclick="openFilter()">
                                    <i class="material-icons">filter_list</i>
                                </button>
                            </div>
                            <div class="table-responsive">
                                <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Akun</th>
                                            {{-- <th scope="col">Email</th> --}}
                                            <th scope="col">Kategori Event</th>
                                            <th scope="col">Tiket Registrasi</th>
                                            <th scope="col">Include Jersey</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Bukti</th>
                                            {{-- <th scope="col">Action</th> --}}
                                            <th scope="col">Tgl Daftar</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.payment.modal-reject')
@include('pages.part.payment.modal-bukti')
@include('pages.part.payment.modal-filter-status')

@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('participants.accountDataTable') }}",
            'data': function(d) {
                d.registration_category = $('#s_registration_category').val();
                d.registration_type     = $('#s_registration_type').val();
                d.include_jersey        = $('#s_include_jersey').val();
                d.status_payment        = $('#s_registration_status').val();
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex',name: 'DT_RowIndex',orderable: false,searchable: false},
            {data: 'name',name: 'name'},
            // {data: 'email',name: 'email'},
            {data: 'registration_category',name: 'registration_category',orderable: false,searchable: false},
            {data: 'registration_type',name: 'registration_type',orderable: false,searchable: false},
            {data: 'include_jersey',name: 'include_jersey',orderable: false,searchable: false},
            {data: 'registration_amount',name: 'registration_amount'},
            {data: 'status_payment',name: 'status_payment',orderable: false,searchable: false},
            {data: 'proof_of_payment',name: 'proof_of_payment',orderable: false,searchable: false},
            // {
            //     data: 'action_payment',
            //     name: 'action_payment',
            //     orderable: false,
            //     searchable: false
            // },
            {data: 'registration_date',name:'registration_date'}
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.draw();
    }
    
    function detailBukti(url,amount) {
        $('#modalBuktiImage').attr('src','');
        $('#modalBuktiImage').attr('src',url);
        $('.modal-title').text('Detail Bukti ('+amount+')');
        $('#modalBukti').modal('show');
    }

    function reject(id,status) {
        $('#modalReject [name="id"]').val(id);
        $('#modalReject [name="status"]').val(status);
        $('#modalReject').modal('show');
    }

    function confirmReject() {
        changeStatus($('#modalReject [name="id"]').val(),$('#modalReject [name="status"]').val(),true);
    }

    function changeStatus(userId,status,reject = false) {
        var url = "{{ route('participants.changeStatus',['user_id'=>':id']) }}";
        let dataForm = {
            status_pembayaran : status,
            payment_remark : ''
        };
        if(reject) {
            dataForm.payment_remark = $('#modalReject [name="payment_remark"]').val();
        }
        url = url.replace(':id',userId);
        swal({
            title: "Status akan dirubah?",
            text: "Dengan ini status akan diubah",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done)=>{
            if(done){
                $.ajax({
                    url: url,
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: dataForm,
                    beforeSend:function(data){
                        swal({text:'Mengubah...',button:false})
                    },
                    success: function(data){
                        if(data.success){
                            swal('Berhasil',data.message,'success');
                            $('#modalReject').modal('hide');
                            table.ajax.reload();
                        } else {
                            swal('Gagal',data.message,'warning');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }

    function sendReminder(userId) {
        var url = "{{ route('reminder.send',['user_id'=>':id']) }}";
        url = url.replace(':id',userId);
        swal({
            title: "Yakin ingin kirim reminder?",
            text: "Reminder berupa email ke peserta",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done)=>{
            if(done){
                $.ajax({
                    url: url,
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend:function(data){
                        swal({text:'Mengirim...',button:false})
                    },
                    success: function(data){
                        if(data.status){
                            swal('Berhasil',data.message,'success');
                            table.ajax.reload();
                        } else {
                            swal('Gagal',data.message,'error');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }

    function openFilter() {
        $('#filterModal .modal-title').text('Filter Data');
        $('#filterModal').modal('show');
    }

</script>
@endpush
@endsection