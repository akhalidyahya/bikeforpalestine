@extends('layouts.base')
@section('title','Data Submit Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Laporan Record Bermasalah</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Laporan Record Tidak Tersimpan</h3>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-left m-b-10">
                                <a target="_blank" class="btn btn-primary" href="https://www.calculatorsoup.com/calculators/time/time-to-decimal-calculator.php">Time Calculator</a>
                            </div>
                            <div class="table-responsive">
                                <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th>Name</th>
                                            <th scope="col">Tanggal Pengajuan</th>
                                            <th scope="col">Activity yang Hilang</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Catatan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.complaint.modal-verify')
@include('pages.part.complaint.modal-reject')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('admin.complaint.dataTable') }}",
            'data': function(d) {
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'searching': true,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'name', name: 'name',orderable:false,searchable:false},
            {data: 'created_at', name: 'created_at',orderable:false,searchable:false},
            {data: 'link', name: 'link',orderable:false,searchable:false},
            {data: 'status', name: 'status',orderable:false,searchable:false},
            {data: 'remark', name: 'remark',orderable:false,searchable:false},
            {data: 'action', name: 'action',orderable:false,searchable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.ajax.reload();        
    }

    function addData() {
        $('.modal-title').text('Submit Data');
        $('#myModal form')[0].reset();
        $('.form-line').removeClass('focused');
        $('#myModal').modal('show');
    }

    function saveData(reject=false) {
        $('#myForm').validate({
            highlight: function(input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function(input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function(error, element) {
                $(element).parents('.input-group').append(error);
            }
        });

        var url = "{{ route('admin.complaint.changeStatus') }}";

        var data = $('#myForm').serialize();
        
        if(reject) {
            data = $('#myFormReject').serialize();
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(data) {
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#myModal').modal('hide');
                        $('#modalReject').modal('hide');
                        table.ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                });
            }
        });
    }

    $(function(){
        $('.custom-time').inputmask('**:**:**', { placeholder: 'HH:MM:SS' });
        //Bootstrap datepicker plugin
        $('#custom_bs_datepicker_container input').datepicker({
            autoclose: true,
            container: '#custom_bs_datepicker_container',
            dateFormat: 'yyyy-mm-dd',
            format: 'yyyy-mm-dd',
        });
    })

    function openFilter() {
        $('.modal-title').text('Filter Data');
        $('#filterModal').modal('show');
    }

    function verify(id,status) {
        $('.modal-title').text('Input Activity Data');
        $('#myModal form')[0].reset();
        $('[name="id"]').val(id);
        $('#myModal').modal('show');
    }

    function reject(id,status) {
        $('.modal-title').text('Reject Activity Data');
        $('#modalReject form')[0].reset();
        $('[name="id"]').val(id);
        $('#modalReject').modal('show');
    }
    
</script>
@endpush
@endsection