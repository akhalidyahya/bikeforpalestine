@extends('layouts.base')
@section('title','Summary')
@push('customCSS')
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>Summary</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-indigo">
                    <i class="material-icons">phone_android</i>
                </div>
                <div class="content">
                    <div class="text">TOTAL AKUN</div>
                    <div class="number count-to" data-from="0" data-to="{{$account_count}}" data-speed="1000" data-fresh-interval="20">{{$account_count}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-blue">
                    <i class="material-icons">verified_user</i>
                </div>
                <div class="content">
                    <div class="text">AKUN AKTIF</div>
                    <div class="number count-to" data-from="0" data-to="{{$active_account_count}}" data-speed="1000" data-fresh-interval="20">{{$active_account_count}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-amber">
                    <i class="material-icons">tag_faces</i>
                </div>
                <div class="content">
                    <div class="text">TOTAL PESERTA</div>
                    <div class="number count-to" data-from="0" data-to="{{$profile_count}}" data-speed="1000" data-fresh-interval="20">{{$profile_count}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-deep-orange">
                    <i class="material-icons">attach_money</i>
                </div>
                <div class="content">
                    <div class="text">PENDAFTARAN (RP)</div>
                    <div class="number count-to" data-from="0" data-to="{{$total_revenue}}" data-speed="1000" data-fresh-interval="20">{{\App\Helper\GeneralHelper::moneyFormat($total_revenue)}}</div>
                </div>
            </div>
        </div>
        {{-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-red">
                    <i class="material-icons">person</i>
                </div>
                <div class="content">
                    <div class="text">PESERTA MALE</div>
                    <div class="number count-to" data-from="0" data-to="{{$peserta_male}}" data-speed="1000" data-fresh-interval="20">{{$peserta_male}}
    </div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box hover-zoom-effect">
        <div class="icon bg-pink">
            <i class="material-icons">person</i>
        </div>
        <div class="content">
            <div class="text">PESERTA FEMALE</div>
            <div class="number count-to" data-from="0" data-to="{{$peserta_female}}" data-speed="1000" data-fresh-interval="20">{{$peserta_female}}</div>
        </div>
    </div>
</div> --}}

<div class="clearfix"></div>

{{-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-amber">
                    <i class="material-icons">directions_bike</i>
                </div>
                <div class="content">
                    <div class="text">PESERTA BIKE</div>
                    <div class="number count-to" data-from="0" data-to="{{$peserta_bike}}" data-speed="1000" data-fresh-interval="20">{{$peserta_bike}}</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box hover-zoom-effect">
        <div class="icon bg-orange">
            <i class="material-icons">directions_run</i>
        </div>
        <div class="content">
            <div class="text">PESERTA RUN</div>
            <div class="number count-to" data-from="0" data-to="{{$peserta_run}}" data-speed="1000" data-fresh-interval="20">{{$peserta_run}}</div>
        </div>
    </div>
</div> --}}

<div class="clearfix"></div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box hover-zoom-effect">
        <div class="icon bg-teal">
            <i class="material-icons">person_outline</i>
        </div>
        <div class="content">
            <div class="text">JERSEY S</div>
            <div class="number count-to" data-from="0" data-to="{{$jersey_s_count}}" data-speed="1000" data-fresh-interval="20">{{$jersey_s_count}}</div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box hover-zoom-effect">
        <div class="icon bg-teal">
            <i class="material-icons">person_outline</i>
        </div>
        <div class="content">
            <div class="text">JERSEY M</div>
            <div class="number count-to" data-from="0" data-to="{{$jersey_m_count}}" data-speed="1000" data-fresh-interval="20">{{$jersey_m_count}}</div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box hover-zoom-effect">
        <div class="icon bg-teal">
            <i class="material-icons">person_outline</i>
        </div>
        <div class="content">
            <div class="text">JERSEY L</div>
            <div class="number count-to" data-from="0" data-to="{{$jersey_l_count}}" data-speed="1000" data-fresh-interval="20">{{$jersey_l_count}}</div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box hover-zoom-effect">
        <div class="icon bg-teal">
            <i class="material-icons">person_outline</i>
        </div>
        <div class="content">
            <div class="text">JERSEY XL</div>
            <div class="number count-to" data-from="0" data-to="{{$jersey_xl_count}}" data-speed="1000" data-fresh-interval="20">{{$jersey_xl_count}}</div>
        </div>
    </div>
</div>
</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <div class="header">
                <h2 class="col-blue">Peserta Berdasarkan Kategori</h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="categoryChart" height="150"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <div class="header">
                <h2 class="col-blue">Pendaftar (akun aktif) Berdasarkan Tiket</h2>
            </div>
            <div class="body">
                <div class="row">
                    <canvas id="ticketChart" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2 class="col-blue">Pemasukan berdasarkan tiket</h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tiket</th>
                                        <th>Satuan</th>
                                        <th>Terjual</th>
                                        <th class="text-right">Pemasukan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($revenue_by_registration_types as $ticketType => $revenue)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{\App\Utilities\Constants::REGISTRATION_TYPE_LIST[$ticketType]}}</td>
                                        <td>
                                            {{ number_format(\App\Utilities\Constants::REGISTRATION_TYPE_PRICE_LIST[$ticketType][\App\Utilities\Constants::COMMON_STATUS_YES],0,',','.') }}
                                        </td>
                                        <td>
                                            @foreach($chart_data as $regisType => $count)
                                            @if($regisType == $ticketType)
                                            {{$count}}
                                            @endif
                                            @endforeach
                                        </td>
                                        <td class="text-right">{{number_format($revenue,0,',','.')}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <th colspan="3">Tidak ada data</th>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="clearfix"></div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <div class="header">
                <h2 class="col-blue">Pemenang Kategori Sepeda</h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="" class="col-deep-orange">Jarak Terjauh Laki-Laki</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_bike_male as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->total_distance}}</td>
                                        <td>{{$data->total_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="" class="col-deep-orange">Jarak Terjauh Perempuan</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_bike_female as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->total_distance}}</td>
                                        <td>{{$data->total_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="" class="col-teal">Finish Tercepat Laki-Laki</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_time_bike_male as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->finished_at}}</td>
                                        <td>{{$data->finished_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="" class="col-teal">Finish Tercepat Perempuan</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_time_bike_female as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->finished_at}}</td>
                                        <td>{{$data->finished_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <div class="header">
                <h2 class="col-blue">Pemenang Kategori Lari</h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="" class="col-deep-orange">Jarak Terjauh Laki-Laki</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_run_male as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->total_distance}}</td>
                                        <td>{{$data->total_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="" class="col-deep-orange">Jarak Terjauh Perempuan</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_run_female as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->total_distance}}</td>
                                        <td>{{$data->total_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="" class="col-teal">Finish tercepat Laki-Laki</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_time_run_male as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->finished_at}}</td>
                                        <td>{{$data->finished_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="" class="col-teal">Finish Tercepat Perempuan</label>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($winner_time_run_female as $data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$data->bib_number}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->finished_at}}</td>
                                        <td>{{$data->finished_time}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5" class="text-center">Belum ada pemenang</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@push('customJS')
<script src="{{asset('admin/plugins/jquery-countto/jquery.countTo.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
<script src="{{asset('admin/js/pages/widgets/infobox/infobox-1.js')}}"></script>
<script src="{{asset('admin/plugins/chartjs/Chart.bundle.js')}}"></script>
<script>
    const ctx = document.getElementById('categoryChart');
    const myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [Number("{{$peserta_bike}}"), Number("{{$peserta_run}}")],
                backgroundColor: [
                    "rgb(233, 30, 99)",
                    "rgb(255, 193, 7)",
                ],
            }],
            labels: [
                "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[\App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE] }}",
                "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[\App\Utilities\Constants::REGISTRATION_CATEGORY_RUN] }}",
            ],

        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    display: true,
                    labels: {
                        color: 'rgb(255, 99, 132)'
                    }
                }
            }
        }
    });

    let ticketChartData = JSON.parse('{!! json_encode($chart_data) !!}');
    let ticketChartValues = [];
    let ticketChartKeys = [];
    for(let i in ticketChartData) {
        ticketChartValues.push(ticketChartData[i]);
        ticketChartKeys.push(i);
    }
    const ticketCtx = document.getElementById('ticketChart');
    const ticketChart = new Chart(ticketCtx, {
        type: 'pie',
        data: {
            datasets: [{
                data: ticketChartValues,
                backgroundColor: JSON.parse('{!!json_encode(\App\Utilities\Constants::REGISTRATION_TYPE_COLOR_LIST)!!}'),
            }],
            labels: ticketChartKeys,

        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    display: true,
                    labels: {
                        color: 'rgb(255, 99, 132)'
                    }
                }
            }
        }
    });
</script>
@endpush
@endsection