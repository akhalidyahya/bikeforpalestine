﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Run & Ride For Palestine</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo" style="margin-top: 40vh;">
            <!-- <a href="{{url('/')}}">Run & Ride For <b>Palestine</b></a> -->
            <a class="animated fadeInLeft" href="{{url('/')}}"><img src="assets/images/logo-white.png" width="250px" alt="Logo" style="padding-top: 20px;"></a>
            <!-- <small>Aplikasi Pendaftaran Online</small> -->
            <h1 class="col-white text-center animated fadeIn">
                COMING SOON
            </h1>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>
    <script src="{{asset('admin/js/pages/examples/sign-in.js')}}"></script>
    <script>
        $("#reload").click(function(){
            $.ajax({
                type:'GET',
                url:'{{route("captcha.reload")}}',
                success:function(data){
                    $(".captcha span").html(data.captcha);
                }
            });
        });
    </script>
</body>

</html>