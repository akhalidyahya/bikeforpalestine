<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Run & Ride for Palestine adalah kegiatan sosial dengan event utama Virtual Cycling and Running to Charity, yang akan berlangsung pada Januari 2022 nanti.⁣⁣⁣⁣⁣⁣ Melalui event ini, peserta tidak hanya diajak untuk berolahraga dengan bersepeda , tetapi juga diajak untuk membentuk rasa kepedulian terhadap Palestina.⁣⁣">
    <meta name="author" content="Run & Ride For Palestine">
    <meta name="robots" content="index, follow" />
    <link rel="canonical" href="{{$CONF->webUrl}}" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}">

    <title>{{$CONF->webName}} | @yield('title')</title>

    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}?v=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.css') }}?v=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/templatemo-art-factory.css') }}?v=1.2">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl-carousel.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/owl-carousel/assets/owl.carousel.min.css') }}?v=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/owl-carousel/assets/owl.theme.default.css') }}?v=1">
    @stack('customCSS')

    </head>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="{{route('home')}}"><img src="assets/images/logo.png" width="150px" alt="Logo" style="padding-top: 20px;"></a>
                        <!-- <a href="#" class="logo">Mulai Belajar Yok</a> -->
                        <!-- ***** Logo End ***** -->

                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            @if(Route::currentRouteName() == 'home')
                            <li class="scroll-to-section"><a data-target="#welcome" href="#welcome" class="active">Home</a></li>
                            <li class="scroll-to-section"><a data-target="#event" href="#event">Event</a></li>
                            <li class="scroll-to-section"><a href="{{route('donation')}}">Donasi</a></li>
                            @else
                            <li class="scroll-to-section"><a href="{{route('home')}}" class="active">Home</a></li>
                            <li class="scroll-to-section"><a href="{{route('home')}}#event">Event</a></li>
                            <li class="scroll-to-section"><a href="{{route('donation')}}">Donasi</a></li>
                            @endif
                            <li class="scroll-to-section"><a href="{{route('merchandise')}}">Merchandise</a></li>
                            <li class="scroll-to-section"><a data-target="" href="{{ route('leaderboard') }}">leaderboard</a></li>
                            <li class="scroll-to-section"><a data-target="" href="{{ route('login.index') }}">Login</a></li>
                            <!-- <li class="scroll-to-section"><a href="#footer">Contact Us</a></li> -->
                        </ul>
                        <a class="menu-trigger">
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->


    @yield('content')

    

    <!-- ***** Footer Start ***** -->
    <div class="ornamen-maskot-footer">
        <img src="{{asset('assets/images/mascot_2.png')}}?v=1" alt="maskot">
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <p class="copyright">Copyright &copy; <?php echo Date('Y'); ?> Run & Ride For Palestine</p>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12" id="footer">
                    <ul class="social">
                        <p>Contact Us : </p> <br>
                        <li><a href="https://instagram.com/rfpofficial_"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://wa.me/628999207972"><i class="fa fa-whatsapp"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- jQuery -->
    <script src="{{ asset('assets/js/jquery-2.1.0.min.js') }}?v=1"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('assets/js/popper.js') }}?v=1"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}?v=1"></script>

    <!-- Plugins -->
    <script src="{{ asset('assets/js/owl-carousel.js') }}?v=1"></script>
    <script src="{{ asset('assets/js/scrollreveal.min.js') }}?v=1"></script>
    <script src="{{ asset('assets/js/waypoints.min.js') }}?v=1"></script>
    <script src="{{ asset('assets/js/jquery.counterup.min.js') }}?v=1"></script>
    <script src="{{ asset('assets/js/imgfix.min.js') }}?v=1"></script> 
    <script src="{{ asset('assets/js/autonumeric.min.js') }}?v=1"></script> 
    
    <!-- Global Init -->
    <script src="{{ asset('assets/js/custom.js') }}?v=1"></script>
    @stack('customJS')
    
  </body>
</html>