<p><h3 class="text-center">Frequently Asked Question (FAQ)</h3></p>
<ol>
    @foreach($faq as $data)
    <li>
        <b>{{$data->question}}</b>
        <br />
        {{$data->answer}}
    </li>
    @endforeach
</ol>