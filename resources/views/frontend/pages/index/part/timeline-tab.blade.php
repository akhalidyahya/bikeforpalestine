{{--
<div class="row">
    <div class="col-md-12">
        <!-- <h3 class="text-center">Timeline</h3> -->
        <img class="img-responsive" src="{{asset('assets/images/timeline.jpg')}}" alt="timeline of RFP 2.0" width="100%">
        <a href='https://www.freepik.com/vectors/infographic'>Infographic vector created by freepik - www.freepik.com</a>
    </div>
</div>
--}}

<p><h3 class="text-center">Timeline</h3></p>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Agenda</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Periode Pendaftaran</td>
            <td>29 November 2021 - 30 Januari 2022</td>
        </tr>
        <tr>
            <td>Kajian Kepalestinaan</td>
            <td>11 Desember 2021</td>
        </tr>
        <tr>
            <td>Opening Ceremony dan Technical Meeting</td>
            <td>22 Januari 2022</td>
        </tr>
        <tr>
            <td>Periode Challenge</td>
            <td>23 Januari - 5 Februari 2022</td>
        </tr>
        <tr>
            <td>Acara Puncak</td>
            <td>12 Februari 2022</td>
        </tr>
        <tr>
            <td>Pengiriman E-Sertifikat dan Medali Finisher</td>
            <td>23 Januari - 13 Februari 2022</td>
        </tr>
    </tbody>
</table>