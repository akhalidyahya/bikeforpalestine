<p><h3 class="text-center">Ketentuan Umum</h3></p>
<ol>
    <li>Pendaftaran tidak dapat dibatalkan ataupun diubah karena alasan apapun.</li>
    <li>1 Tiket hanya untuk 1 cabang Challenge (pilih Run atau Ride).</li>
    <li>Sistem Multiple Run and Ride : berlari atau bersepeda boleh lebih dari satu kali submit selama periode Challenge.</li>
    <li>Aplikasi perekam yang disarankan yaitu <b>Strava</b>.</li>
    <li>Selalu <b>hidupkan GPS</b> saat berlari/bersepeda di kawasan <i>outdoor</i> dengan aplikasi perekam aktivitas lari/sepeda.</li>
    <li>Untuk hasil rekaman aktivitas yang mengalami <b>GPS Drift </b>dimana rute menjadi tidak sesuai dengan yang seharusnya, panitia berhak untuk mendiskualifikasikan atau menyesuaikan jarak tempuh dari aktivitas tersebut.</li>
    <li>Batas waktu pendaftaran adalah hingga hari Sabtu tanggal 30 Januari 2022 pukul 23:59:59 WIB.</li>
    <li>Submission atau upload hasil berlari/bersepeda hanya diterima selama tanggal pelaksanaan acara, yaitu dari&nbsp;<b>23 Januari 2022 pukul 00:00 WIB</b>&nbsp;hingga&nbsp;<b>5 Februari 2022 pukul 23:59 WIB</b>.</li>
    <li>
    <p>Proses validasi hasil aktivitas akan dilakukan secara bertahap untuk menjaga sportivitas dan keabsahan dari leaderboard acara. Leaderboard baru bersifat final apabila pemenang sudah diumumkan.</p>
    </li>
    <li>Peserta yang dianggap melanggar peraturan akan didiskualifikasikan dari daftar calon pemenang.</li>
    <li>Patuhi protokol kesehatan COVID-19 selama kamu mengikuti RFP 2022.</li>
    <li>Peraturan dan ketentuan dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu.</li>
    <li>Apabila terdapat <b>pertanyaan / laporan terkait event</b>, mohon untuk menghubungi kami melalui form Contact Us.</li>
</ol>
<br><br>
<p><h3 class="text-center">Ketentuan Instagram Contest</h3></p>
<ol>
    <li>Follow akun Instagram @rfpofficial_.</li>
    <li>Unggah foto terbaikmu saat atau setelah selesai melakukan aktivitas lari atau bersepeda dengan menggunakan twibbon yang disediakan.</li>
    <li>Tulis caption yang kreatif dan inspiratif tentang Palestina dan gaya hidup sehat kamu dan tag sebanyak mungkin temanmu untuk ikutan!</li>
    <li>Tag rfpofficial_ di fotonya.</li>
    <li>Jangan lupa untuk sertakan hashtag #sportcharityforpalestine #RFPVirtualsport2022 pada caption.</li>
    <li>Periode kontes adalah tanggal 23 Januari 2022 hingga 10 Februari 2022.</li>
    <li>Pastikan akun Instagram kamu tidak dalam keadaan terkunci.</li>
    <li>Tidak ada batas maksimum jumlah Instagram post yang boleh diunggah.
    </li>
    <li>
    Pemenang akan ditentukan berdasarkan foto serta caption paling bagus dan inspiratif dan akan diumumkan melalui akun Instagram @rfpofficial_ pada tanggal 12 Februari 2022.
    </li>
    <li>Setiap materi yang diikutsertakan dan dikirimkan dalam acara ini menjadi hak milik RFP yang sepenuhnya dapat disesuaikan dan diubah untuk disebarluaskan, diperbanyak serta dipublikasikan dalam bentuk apapun, dalam jumlah dan jangka waktu tak terbatas.</li>
</ol>