<p><h3 class="text-center">Informasi</h3></p>
<div class="owl-carousel owl-theme">
    <div class="item">
        <img class="img-responsive" src="{{asset('assets/images/slider/image5.jpeg')}}" alt="Main poster of RFP 2.0" width="100%">
    </div>
    <div class="item">
        <img class="img-responsive" src="{{asset('assets/images/slider/image-2.1.png')}}" alt="Main poster of RFP 2.0" width="100%">
    </div>
    <div class="item">
        <img class="img-responsive" src="{{asset('assets/images/slider/image-3.1.jpg')}}" alt="Main poster of RFP 2.0" width="100%">
    </div>
    <div class="item">
        <img class="img-responsive" src="{{asset('assets/images/slider/image-4.1.jpg')}}" alt="Main poster of RFP 2.0" width="100%">
    </div>
</div>