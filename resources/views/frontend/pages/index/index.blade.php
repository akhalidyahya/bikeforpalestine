@extends('frontend.layouts.template')
@section('title','Beranda')
@push('customCSS')
@endpush
@section('content')
<!-- ***** Welcome Area Start ***** -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="welcome-area" id="welcome">

    <!-- ***** Header Text Start ***** -->
    <div class="header-text pt-5">
        <div class="container">
            <div class="row">
                <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 1s after 0.6s">
                    <h1>Run & Ride for Palestine</h1>
                    <p>Run & Ride for Palestine adalah kegiatan sosial dengan event utama Virtual Run and Ride to Charity, yang akan berlangsung dimulai pada Januari 2022 nanti.⁣⁣⁣⁣⁣⁣ Melalui event ini, peserta tidak hanya diajak untuk berolahraga dengan berlari dan bersepeda
                        , tetapi juga diajak untuk membentuk rasa kepedulian terhadap Palestina.⁣⁣</p>
                    <a href="{{ route('register.index') }}" class="main-button-slider">Daftar Sekarang</a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter right move 30px over 0.8s after 0.6s">
                    <img src="{{asset('assets/images/mascot_1.png')}}" class="rounded img-fluid d-block mx-auto" alt="First Vector Graphic">
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->

<!-- ***** event Section Start ***** -->
<div class="event-section" id="event">
    <div class="container">
        <div class="row" id="pc-ver">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Informasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="beramal-tab" data-toggle="tab" href="#beramal-pc" role="tab" aria-controls="info" aria-selected="true">Beramal Lewat karya</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="registrasi-tab" data-toggle="tab" href="#registrasi" role="tab" aria-controls="registrasi" aria-selected="false">Timeline</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="peraturan-tab" data-toggle="tab" href="#peraturan" role="tab" aria-controls="peraturan" aria-selected="false">Peraturan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="faq-tab" data-toggle="tab" href="#faq" role="tab" aria-controls="faq" aria-selected="false">FAQ</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
                        @include('frontend.pages.index.part.info-tab')
                    </div>
                    <div class="tab-pane fade" id="beramal-pc" role="tabpanel" aria-labelledby="info-tab">
                        @include('frontend.pages.index.part.beramal-tab')
                    </div>
                    <div class="tab-pane fade" id="registrasi" role="tabpanel" aria-labelledby="registrasi-tab">
                        @include('frontend.pages.index.part.timeline-tab')
                    </div>
                    <div class="tab-pane fade" id="peraturan" role="tabpanel" aria-labelledby="peraturan-tab">
                        @include('frontend.pages.index.part.peraturan-tab')
                    </div>
                    <div class="tab-pane fade" id="faq" role="tabpanel" aria-labelledby="faq-tab">
                        @include('frontend.pages.index.part.faq-tab')
                    </div>
                </div>
                <div class="text-center mt-5">
                    <a href="{{ route('register.index') }}" class="main-button-slider" style="color: #fff">Daftar Sekarang</a>
                </div>
            </div>
        </div>
        <div class="row" id="mobile-ver">
            <div class="col-md-12">
            <div class="" >
                    <div class="" id="slideInformasi">
                        <p><h3 class="text">Informasi <i class="fa fa-caret-down"></i></h3></p>
                    </div>
                    <div class="mt-5" id="informasi">
                        @include('frontend.pages.index.part.info-tab-mobile')
                    </div>

                    <div class="mt-2" id="slideBeramal">
                        <p><h3 class="text">Beramal Lewat Karya <i class="fa fa-caret-down"></i></h3></p>
                    </div>
                    <div class="mt-5" id="beramal">
                        @include('frontend.pages.index.part.beramal-tab-mobile')
                    </div>

                    <div class="mt-2" id="slideTimeline">
                        <p><h3 class="text">Timeline <i class="fa fa-caret-down"></i></h3></p>
                    </div>
                    <div class="mt-5" id="timeline">
                        @include('frontend.pages.index.part.timeline-tab')
                    </div>

                    <div class="mt-2" id="slideKetentuan">
                        <p><h3 class="text">Peraturan <i class="fa fa-caret-down"></i></h3></p>
                    </div>
                    <div class="tab-pane mt-5" id="ketentuan">
                        @include('frontend.pages.index.part.peraturan-tab')
                    </div>

                    <div class="mt-2" id="slideFAQ">
                        <p><h3 class="text">FAQ <i class="fa fa-caret-down"></i></h3></p>
                    </div>
                    <div class="tab-pane mt-5" id="FAQ">
                        @include('frontend.pages.index.part.faq-tab')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** event Section End ***** -->

<!-- ***** sponsor Section Start ***** -->
<div class="sponsor-section" id="sponsor">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Sponsor Kami</h3>
                <div class="text-center sponsor-img-container">
                    <a href="https://instagram.com/backbone.gaming?utm_medium=copy_link">
                        <img src="{{asset('assets/images/sponsors/logo bbg transparan.png')}}" alt="" height="50px">
                    </a>
                    <a href="https://instagram.com/iwake_up.id?utm_medium=copy_link">
                        <img src="{{asset('assets/images/sponsors/logo iwake transparan.png')}}" alt="" height="50px">
                    </a>
                    <a href="https://instagram.com/zisindosat?utm_medium=copy_link">
                        <img src="{{asset('assets/images/sponsors/Logo ZIS _ Solid.png')}}" alt="" height="50px">
                    </a>
                    <a href="https://instagram.com/qomishu.official?utm_medium=copy_link">
                        <img src="{{asset('assets/images/sponsors/QOMISHU HITAM.png')}}" alt="" height="50px">
                    </a>
                    <a href="https://instagram.com/machaofficial.id?utm_medium=copy_link">
                        <img src="{{asset('assets/images/sponsors/1. Logo Macha Fix Transparant.png')}}" alt="" height="50px">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** sponsor Section End ***** -->

<!-- ***** Donasi Section Start ***** -->
<div class="donasi-section" id="donasi">
    <div class="container small">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Donasi Langsung</h3>
                <p style="letter-spacing: .05em;">
                    Selain mengikuti event seru dari Run and Ride for Palestine, kamu juga dapat berdonasi secara langsung untuk Palestina melalui KNRP.
                    <br>
                    Donasi dapat dilakukan dengan transfer bank ke QR Code QRIS Run Ride for Palestine pada <a href="{{route('donation')}}" target="_blank">halaman donasi</a>. <br>
                    <b>Tambahkan kode unik 050 di 3 digit terakhir nominal donasi Anda.</b>
                    <br>
                    Contoh: Rp 50.050. Lalu lakukan konfirmasi pada <a href="{{route('donation')}}" target="_blank">halaman donasi</a> juga.
                    <br>
                </p>
                <div class="text-center mt-5">
                    <a href="{{ route('donation') }}" class="main-button-slider" style="color: #fff">Donasi Sekarang</a>
                </div>
                {{-- <div class="text-center mt-3">
                    <p>Donasi dapat dilakukan dengan transfer bank ke: <br> </p>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="{{asset('assets/images/muamalat-logo.png')}}" alt="logo muamalat" height="40px">
                        </div>
                        <div class="col-sm-6">
                            <p>
                                <b style="letter-spacing: .1em;">8306 11 00000 2000 2</b> <br>
                                a.n. YPNJ PEDULI PALESTINA
                            </p>
                        </div>
                    </div>
                    <hr>
                </div> --}}
            </div>
        </div>
    </div>
</div>
<!-- ***** Donasi Section End ***** -->
@endsection
@push('customJS')
<script>
$(document).ready(function() {
    $("#slideInformasi").click(function() {
        $("#informasi").slideToggle();
    })

    $("#slideTimeline").click(function() {
        $("#timeline").slideToggle();
    })

    $("#slideBeramal").click(function() {
        $("#beramal").slideToggle();
    })

    $("#slideKetentuan").click(function() {
        $("#ketentuan").slideToggle();
    })

    $("#slideFAQ").click(function() {
        $("#FAQ").slideToggle();
    })

    
})
</script>
@endpush