@extends('frontend.layouts.template')
@section('title', 'Donasi')
@section('content')
@push('customCSS')

@endpush

<div class="merchandise-section" style="margin-top: 100px; z-index:10;">
    <div class="container">
        <h3 class="text-center">Merchandise</h3>
        <div class="row justify-content-center">
            <div class="col-md-12 merchandise">
                        <div class="card" style="width: 18rem; background-color:white; margin-top:50px; margin-bottom:280px;">
                            <img src="{{asset('assets/images/merchandise/merchandise1.jpeg')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                            <h5 class="card-title">Merchandise Exclusive Run & Ride For Palestine 2022</h5>
                            <p class="card-text">Yuk langsung check-out karena sebagian hasil penjualan akan di donasikan untuk Rakyat Palestina🇵🇸</p>
                            <br>
                            <a href="https://www.tokopedia.com/rfpofficial/jersey-run-and-ride-for-palestine-s" class="btn btn-primary">Beli Sekarang</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>