@extends('frontend.layouts.template')
@section('title','Leaderboard')
@section('content')
@push('customCSS')
<link rel="styleshet" type="text/css" href="{{asset('assets/plugins/datatable/datatables.min.css')}}">
@endpush
<!-- ***** event Section Start ***** -->
<div class="sponsor-section" style="margin-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 leaderboard">
                <h3 class="text-center">Leaderboard</h3>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="1-tab" data-toggle="tab" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true">Sepeda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="2-tab" data-toggle="tab" href="#tab_2" role="tab" aria-controls="tab_2" aria-selected="false">Lari</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="tab_1" role="tabpanel" aria-labelledby="1-tab">
                        <div class="table table-responsive pb-3 mt-3" style="background-color: #fff;">
                            <table class="table" id="myTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>E-BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak (km)</th>
                                        <th>Waktu (h:j:m:d)</th>
                                        <th>Pace (menit/km)</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="2-tab">
                        <div class="table table-responsive pb-3 mt-3" style="background-color: #fff;">
                            <table class="table" id="myTable2">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>E-BIB</th>
                                        <th>Nama</th>
                                        <th>Jarak (km)</th>
                                        <th>Waktu (h:j:m:d)</th>
                                        <th>Pace (menit/km)</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** event Section End ***** -->
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('assets/plugins/datatable/datatables.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('fe.leaderboard.data') }}",
            'data': function(d) {
                d.registration_category = "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex',name: 'DT_RowIndex',searchable:false,orderable:false},
            {data: 'bib_number',name: 'bib_number'},
            {data: 'name',name: 'name'},
            {data: 'total_distance_km_format',name: 'total_distance_km_format',searchable:false,orderable:false},
            {data: 'total_time_time_format',name: 'total_time_time_format',searchable:false,orderable:false},
            {data: 'pace',name: 'pace',searchable:false,orderable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    var table2 = $('#myTable2').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('fe.leaderboard.data') }}",
            'data': function(d) {
                d.registration_category = "{{ \App\Utilities\Constants::REGISTRATION_CATEGORY_RUN }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex',name: 'DT_RowIndex',searchable:false,orderable:false},
            {data: 'bib_number',name: 'bib_number'},
            {data: 'name',name: 'name'},
            {data: 'total_distance_km_format',name: 'total_distance_km_format',searchable:false,orderable:false},
            {data: 'total_time_time_format',name: 'total_time_time_format',searchable:false,orderable:false},
            {data: 'pace',name: 'pace',searchable:false,orderable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    $('.dataTables_filter').addClass('text-right');
</script>
@endpush
@endsection