@extends('frontend.layouts.template')
@section('title', 'Donasi')
@section('content')
@push('customCSS')

@endpush
<!-- ***** event Section Start ***** -->
<div class="sponsor-section" style="margin-top: 50px;">
    <div class="container">
        <h3 class="text-center">Donasi Langsung</h3>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 donation">
                <div class="donation-container">
                    @error('captcha')
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <span> {{ $message }}. </span>
                        </div>
                    @enderror
                    @if (session('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-warning alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            {{ session('message') }}
                        </div>
                    @endif
                    <form id="donationForm" action="{{route('donation.save')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src="{{asset('assets/images/QRISRFP.jpg')}}" alt="QRISRFP" height="200px">
                                    <p>
                                        Silahkan lakukan donasi melalui QR code QRIS Run Ride For Palestine diatas dan masukan kode unik {{\App\Utilities\Constants::UNIQUE_CODE_DONATION}} pada akhir nominal. Contoh: 100.0{{\App\Utilities\Constants::UNIQUE_CODE_DONATION}}.
                                        Lalu lakukan konfirmasi dengan mengisi data dibawah ini yaa.
                                    </p>
                                    <br>
                                </div>
                            </div>
                            <div class="form-group col">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="name" class="form-control" placeholder="Contoh: Khalid Yahya">
                                <div class="form-check" style="margin-top: 5px">
                                    <input name="is_anonymous" class="form-check-input" type="checkbox" id="gridCheck" value="1">
                                    <label class="form-check-label" for="gridCheck">
                                        <small>Donasi sebagai Anonim</small>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">No Handphone</label>
                                <input type="text" name="phone_number" class="form-control" placeholder="Contoh: 081234567891">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Contoh: yahya@gmail.com">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col">
                                <label for="">Nominal Donasi</label>
                                <small>(minimal 10.000 dan beserta kode unik)</small>
                                <input type="text" name="donation_amount" class="form-control autonumeric" placeholder="Rp.">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8 captcha text-center">
                                <span>{!! captcha_img('flat') !!}</span>
                                <button type="button" class="btn btn-primary" class="reload" id="reload">
                                &#x21bb;
                                </button>
                                <input id="captcha" type="text" class="form-control" style="margin-top: 10px;" placeholder="Enter Captcha" name="captcha" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary" style="background-color: #70b1d7;">Donasi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ***** event Section End ***** -->
@push('customJS')
<!-- Validation Plugin Js -->
<script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>
<script>
    $('#donationForm').validate({
        rules: {
            password: {
                required: true,
                minlength: 4
            },

        },
        highlight: function(input) {
            console.log(input);
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function(input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function(error, element) {
            $(element).parents('.input-group').append(error);
        }
    });

    $("#reload").click(function(){
        $.ajax({
            type:'GET',
            url:'{{route("captcha.reload")}}',
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
@endpush
@endsection
