﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Register | Run & Ride For Palestine</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Font Awesome -->
    <link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
</head>

<body class="container signup-page">
    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <div class="signup-box">
                <div class="row">
                    <div class="col-md-6 col-md-push-3">
                        <div class="logo">
                        <a href="{{url('/')}}"><img src="assets/images/logo-white.png" width="250px" alt="Logo" style="padding-top: 20px;"></a>
                        </div>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
                <div class="card" style="margin-left: 12px;">
                    <div class="body">
                        <form id="sign_in" method="POST" action="{{route('register.save')}}">
                            {{csrf_field()}}
                            <h3 class="text-center col-blue m-b-30">Sampai Jumpa di event selanjutnya!</h3>
                            <!-- Basic Example | Vertical Layout -->
                            {{-- <div id="wizard_horizontal">
                                <h2>Event</h2>
                                <section>
                                    <p>
                                        <label for="">Kategori Challenge</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">directions_run</i>
                                            </span>
                                            <select class="form-control show-tick" name="registration_category" required onchange="renderSummary()">
                                                <option value="">- Pilih Kategori Challenge -</option>
                                                @foreach(\App\Helper\SelectHelper::getRegistrationCategory() as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}

                                        {{--
                                        <label for="">Paket Pendaftaran</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">theaters</i>
                                            </span>
                                            <select class="form-control show-tick" name="registration_type" required onchange="renderSummary()">
                                                <option value="">- Pilih Paket Pendaftaran -</option>
                                                @foreach(\App\Helper\SelectHelper::getRegistrationType() as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        --}}
                                        {{-- <label for="">Tiket Pendaftaran</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">theaters</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control grey-mode" name="registration_type_text" placeholder="Contoh : John Doe" disabled autocomplete="off" value="{{ \App\Utilities\Constants::REGISTRATION_TYPE_LIST[\App\Helper\SelectHelper::getRegistrationTypeByDate(Date('Y-m-d'))] }}">
                                            </div>
                                        </div>
                                        <input type="hidden" name="registration_type" value="{{\App\Helper\SelectHelper::getRegistrationTypeByDate(Date('Y-m-d'))}}">

                                        <label for="">Include Racepack 
                                            <i class="text-primary fa fa-question-circle fa-lg" 
                                                data-trigger="focus" 
                                                data-container="body" 
                                                data-toggle="popover" 
                                                data-placement="right" 
                                                title="" 
                                                data-content="Pilihan apakah ingin dikirim racepack (Jersey dan Masker) atau tidak. Harga berubah tergantung dengan yang dipilih." 
                                                data-original-title="Info!"></i> 
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment_ind</i>
                                            </span>
                                            <select class="form-control show-tick" name="include_jersey" required onchange="renderSummary()">
                                                @foreach(\App\Helper\SelectHelper::getCommonStatusBooleanList() as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <small class="col-red">*harga tiket akan menyesuaikan dengan apakah perlu racepack atau tidak</small>
                                        </div>

                                        <label for="">Harga Tiket: <span class="col-blue" id="receipt"></span></label>
                                    </p>
                                </section>

                                <h2>Data Diri</h2>
                                <section>
                                    <p>
                                        <label for="">Nama</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="name" placeholder="Contoh : John Doe" required autofocus autocomplete="off" value="{{ old('name') }}">
                                            </div>
                                        </div>

                                        <label for="">Email</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="email" class="form-control" name="email" placeholder="Contoh : name@email.com" required autocomplete="off" value="{{old('email')}}">
                                            </div>
                                        </div>

                                        <label for="">Umur</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="age" placeholder="Contoh: 29" required autocomplete="off" value="{{ old('age') }}">
                                            </div>
                                        </div>

                                        <label for="">Jenis Kelamin</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">wc</i>
                                            </span>
                                            <select class="form-control show-tick" name="gender" required>
                                                @foreach(\App\Helper\SelectHelper::getGender() as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="">No HP (WA)</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone_android</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="number" class="form-control" name="phone_number" placeholder="Contoh : 088211112222" required autocomplete="off" value="{{ old('phone_number') }}">
                                            </div>
                                        </div>

                                        <label for="">Alamat</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">home</i>
                                            </span>
                                            <div class="form-line">
                                                <textarea class="form-control" name="address" placeholder="Contoh: Jl Kenanga V no 96 Atsiri Permai RT 01 RW 13 Kab Bogor 16431" required autocomplete="off"></textarea>
                                            </div>
                                        </div>
                                        
                                        <label for="">Password Akun</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password akun minimal 4 karakter" required>
                                            </div>
                                        </div>
                                    </p>
                                </section>

                                <h2>Summary</h2>
                                <section>
                                    <p>
                                        <div class="col-md-12">
                                            <label for="">Summary</label>
                                            <table>
                                                <tr class="m-b-10">
                                                    <td>Kategori Challenge</td>
                                                    <td class="p-r-10 p-l-5">:</td>
                                                    <td><b id="kategoriEvent"></b></td>
                                                </tr>
                                                <tr class="m-b-10">
                                                    <td>Tiket Pendaftaran</td>
                                                    <td class="p-r-10 p-l-5">:</td>
                                                    <td><b id="paketPendaftaran"></b></td>
                                                </tr>
                                                <tr class="m-b-10">
                                                    <td>Include Racepack</td>
                                                    <td class="p-r-10 p-l-5">:</td>
                                                    <td><b id="racepack"></b></td>
                                                </tr>
                                                <tr class="m-b-10">
                                                    <td>Kode Unik</td>
                                                    <td class="p-r-10 p-l-5">:</td>
                                                    <td><b id="uniqueCode">125</b></td>
                                                </tr>
                                                <tr class="m-b-10">
                                                    <td>Total harga tiket</td>
                                                    <td class="p-r-10 p-l-5">:</td>
                                                    <td><b id="hargaTiket"></b></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </p>
                                </section> 
                            </div> --}}
                            <!-- #END# Basic Example | Vertical Layout -->
                            {{-- <p class="text-center m-t-20">
                                Sudah memiliki akun? <a href={{ route('login.index') }}>Login</a><br>
                                Lupa password? <a href="{{ route('resetPassword.index') }}">Klik Disini </a>
                            </p> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Sweetalert -->
    <script src="{{asset('admin/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <!-- Popper Js -->
    <script src="{{asset('admin/js/pages/ui/tooltips-popovers.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>
    {{--<script src="{{asset('admin/js/pages/examples/sign-in.js')}}"></script>--}}

    <script src="{{asset('admin/plugins/jquery-steps/jquery.steps.js')}}"></script>

    <script>
        let priceList = JSON.parse('{!! json_encode(\App\Utilities\Constants::REGISTRATION_TYPE_PRICE_LIST) !!}');
        let registrationType = JSON.parse('{!! json_encode(\App\Utilities\Constants::REGISTRATION_TYPE_LIST) !!}');

        $('[data-toggle="tooltip"]').tooltip();

        $('#wizard_horizontal').steps({
            headerTag: 'h2',
            bodyTag: 'section',
            transitionEffect: 'slideLeft',
            stepsOrientation: 'horizontal',
            enableAllSteps: false,
            labels: {
                finish: "Daftar",
                next: "Selanjutnya",
                previous: "Sebelumnya",
            },
            onInit: function (event, currentIndex) {
                setButtonWavesEffect(event);
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                if (currentIndex > newIndex) { return true; }

                if (currentIndex < newIndex) {
                    $('#sign_in').find('.body:eq(' + newIndex + ') label.error').remove();
                    $('#sign_in').find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                // form.validate().settings.ignore = ':disabled,:hidden';
                renderSummary();
                return $('#sign_in').valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex) {
                setButtonWavesEffect(event);
            },
            onFinished: function (event, currentIndex) {
                swal({text:"Sedang Menyimpan Data...",button:false});
                saveData(event);
            }

        });
        
        function setButtonWavesEffect(event) {
            $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
            $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
        }

        function saveData(event) {
            event.preventDefault(); 
            document.getElementById('sign_in').submit();
        }

        function renderSummary() {
            $('#kategoriEvent').text($('[name="registration_category"] option:selected').text());
            $('#paketPendaftaran').text(registrationType[$('[name="registration_type"]').val()]);
            $('#racepack').text($('[name="include_jersey"] option:selected').text());
            console.log($('[name="include_jersey"]').val());
            if($('[name="include_jersey"]').val() == 'YES') {
                console.log(1);
                $('[name="registration_type"]').val('{{\App\Helper\SelectHelper::getRegistrationTypeByDate(Date('Y-m-d'))}}');
                $('[name="registration_type_text"]').val(registrationType['{{\App\Helper\SelectHelper::getRegistrationTypeByDate(Date('Y-m-d'))}}']);
            } else {
                $('[name="registration_type"]').val('{{\App\Utilities\Constants::REGISTRATION_TYPE_NON_RACEPACK}}');
                $('[name="registration_type_text"]').val('Non Racepack');
            }
            $('#hargaTiket').text('Rp '+(priceList[$('[name="registration_type"]').val()][$('[name="include_jersey"]').val()]+125).toLocaleString("id-ID"));
            $('#receipt').text('Rp '+(priceList[$('[name="registration_type"]').val()][$('[name="include_jersey"]').val()]+125).toLocaleString("id-ID"));
        }

        $('#sign_in').validate({
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
            },
            rules: {
                password: {
                    required: true,
                    minlength: 4
                },
            },
        });
    </script>
</body>

</html>