<!-- User Info -->
<div class="user-info">
    <div class="image">
        <img src="{{asset('admin/images/user.png')}}" width="48" height="48" alt="User" />
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{@\Auth::user()->name}} </div>
        <div class="email">{{@\Auth::user()->email}}</div>
        <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
                <!-- <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li> -->
                <li role="separator" class="divider"></li>
                <li><a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="material-icons">input</i>Sign Out</a></li>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </div>
    </div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{@$sidebar == 'dashboard' ? 'active': ''}}">
            <a href="{{route('dashboard.index')}}">
                <i class="material-icons">dashboard</i>
                <span>Dasbor</span>
            </a>
        </li>
        @if(\App\Helper\GeneralHelper::isAdmin())
        <?php 
            $participants_menu = ['participants-payment','participants-data'];
            $notifications = \App\Helper\SelectHelper::getNewPaymentNotification();
            $reports = \App\Helper\SelectHelper::getNewReportNotification();
        ?>
        <li class="@if(in_array(@$sidebar,$participants_menu)) active @endif">
            <a href="javascript:void(0);" class="menu-toggle @if(in_array(@$sidebar,$participants_menu)) toggled @endif">
                <i class="material-icons">perm_identity</i>
                <span>Peserta @if($notifications > 0) <i class="fa fa-circle col-red fa-lg"></i> @endif</span>
            </a>
            <ul class="ml-menu">
                <li class="{{@$sidebar == 'participants-data' ? 'active': ''}}">
                    <a href="{{route('participants.data.index')}}">Data</a>
                </li>
                <li class="{{@$sidebar == 'participants-payment' ? 'active': ''}}">
                    <a href="{{route('participants.payment.index')}}">Pembayaran @if($notifications > 0) <span class="label label-danger col-white" style="transform: translateY(-5px);">{{$notifications}}</span> @endif</a>
                </li>
            </ul>
        </li>
        <li class="{{@$sidebar == 'complaint' ? 'active': ''}}">
            <a href="{{route('admin.complaint.index')}}">
                <i class="material-icons">timeline</i>
                <span>Laporan Record @if($reports > 0) <span class="label label-danger col-white" style="transform: translateY(-5px);">{{$reports}}</span> @endif</span>
            </a>
        </li>
        {{-- <li class="{{@$sidebar == 'hasil' ? 'active': ''}}">
            <a href="{{route('result.index')}}">
                <i class="material-icons">cloud_upload</i>
                <span>
                    Hasil Submit
                    @if($results > 0) <span class="label label-danger col-white">{{$results}}</span> @endif
                </span>
            </a>
        </li> --}}
        <li class="{{@$sidebar == 'leaderboard' ? 'active': ''}}">
            <a href="{{route('leaderboard.index')}}">
                <i class="material-icons">clear_all</i>
                <span>Leaderboard</span>
            </a>
        </li>
        <li class="{{@$sidebar == 'summary' ? 'active': ''}}">
            <a href="{{route('summary.index')}}">
                <i class="material-icons">insert_chart</i>
                <span>Summary</span>
            </a>
        </li>
        {{-- <li class="{{@$sidebar == 'config' ? 'active': ''}}">
            <a href="{{route('config.index')}}">
                <i class="material-icons">computer</i>
                <span>Config</span>
            </a>
        </li> --}}
        @elseif(!\App\Helper\GeneralHelper::isAdmin() && \Auth::user()->status_payment == \App\Utilities\Constants::REGISTRATION_STATUS_PAID)
        <li>
            <a href="{{route('kilasBalik.index')}}">
                <i class="material-icons">av_timer</i>
                <span>Kilas Balik</span>
            </a>
        </li>
        <li class="{{@$sidebar == 'profile' ? 'active': ''}}">
            <a href="{{route('user.profile.index')}}">
                <i class="material-icons">people</i>
                <span>Data Profil</span>
            </a>
        </li>
        @if($CONF->startChallenge)
        {{-- <li class="{{@$sidebar == 'submit' ? 'active': ''}}">
            <a href="{{route('user.submit.index')}}">
                <i class="material-icons">cloud_upload</i>
                <span>Submit</span>
            </a>
        </li> --}}
        <li class="{{@$sidebar == 'leaderboard' ? 'active': ''}}">
            <a href="{{route('user.leaderboard.index')}}">
                <i class="material-icons">clear_all</i>
                <span>Leaderboard</span>
            </a>
        </li>
        <li class="{{@$sidebar == 'history' ? 'active': ''}}">
            <a href="{{route('user.history.index')}}">
                <i class="material-icons">timeline</i>
                <span>Riwayat Record</span>
            </a>
        </li>
        @endif
        <li class="{{@$sidebar == 'sync' ? 'active': ''}}">
            <a href="{{route('user.strava.sync')}}">
                <i class="material-icons">autorenew</i>
                <span>Sync Strava</span>
            </a>
        </li>
        @endif

    </ul>
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
    <div class="copyright">
        &copy; <a href="javascript:void(0);">{{$CONF->webName}}</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1
    </div>
</div>
<!-- #Footer -->