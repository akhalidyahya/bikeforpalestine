@extends('layouts.base')
@section('title','Data Submit Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Manajemen Data Submit</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Data Submit</h3>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="s_search" type="text" class="form-control" onchange="filter()" placeholder="Cari Data Submit (nama, bib)">
                                </div>
                                <div class="col-md-8">
                                    <div class="text-right m-b-15">
                                        <button type="button" class="btn btn-primary waves-effect btn-sm m-r-5" onclick="openFilter()">
                                            <i class="material-icons">filter_list</i>
                                        </button>
                                        <button type="button" class="btn btn-primary waves-effect btn-sm" onclick="addData()">
                                            <i class="material-icons">cloud_upload</i>
                                            <span>Submit</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Tanggal</th>
                                            <th scope="col">BIB</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Jarak (km)</th>
                                            <th scope="col">Waktu</th>
                                            <th scope="col">Link</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('user-pages.part.submit.modal-result')
@include('user-pages.part.submit.modal-filter-result')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('user.submit.dataTableActivity') }}",
            'data': function(d) {
                d.registration_category = "{{ \Auth::user()->registration_category }}";
                d.search = $('#s_search').val();
                d.start_date = $('#s_start_date').val();
                d.end_date = $('#s_end_date').val();
                d.profile_id = $('#s_profile_id').val();
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'searching': false,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'activity_date', name: 'activity_date'},
            {data: 'bib_number', name: 'bib_number',orderable:false,searchable:false},
            {data: 'name', name: 'name',orderable:false,searchable:false},
            {data: 'distance', name: 'distance'},
            {data: 'duration', name: 'duration'},
            {data: 'link', name: 'link',orderable:false,searchable:false},
            {data: 'status', name: 'status',orderable:false,searchable:false},
            {data: 'notes', name: 'notes',orderable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.ajax.reload();        
    }

    function addData() {
        $('.modal-title').text('Submit Data');
        $('[name="method"]').val('add');
        $('#myModal form')[0].reset();
        $('.form-line').removeClass('focused');
        $('#myModal').modal('show');
    }

    function saveData() {
        $('#myForm').validate({
            highlight: function(input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function(input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function(error, element) {
                $(element).parents('.input-group').append(error);
            }
        });

        var url = "{{ route('user.submit.save') }}";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#myForm').serialize(),
            success: function(data) {
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#myModal').modal('hide');
                        table.ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                });
            }
        });
    }

    $(function(){
        $('.custom-time').inputmask('**:**:**', { placeholder: 'HH:MM:SS' });
        //Bootstrap datepicker plugin
        $('#custom_bs_datepicker_container input').datepicker({
            autoclose: true,
            container: '#custom_bs_datepicker_container',
            dateFormat: 'yyyy-mm-dd',
            format: 'yyyy-mm-dd',
        });
    })

    function openFilter() {
        $('.modal-title').text('Filter Data');
        $('#filterModal').modal('show');
    }


    
</script>
@endpush
@endsection