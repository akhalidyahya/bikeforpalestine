@extends('layouts.base')
@section('title','Profil Peserta')
@push('customCSS')
<style>
    #copy {
        cursor: pointer;
    }

    #bank1 {
        font-size: 1.3em;
        border: none !important;
        outline: none !important;
        background-color: transparent !important;
        font-weight: bolder;
    }

    input[type=file] {
        background-color: #dedede !important;
        padding: 5px !important;
    }
</style>
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>Profil Peserta</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2 class="col-blue">Tiket Pendaftaran: <strong>{{ \App\Utilities\Constants::REGISTRATION_TYPE_LIST[\Auth::user()->registration_type] }}</strong>
                        {{-- <div class="right"> --}}
                            @if(\Auth::user()->registration_category == \App\Utilities\Constants::REGISTRATION_CATEGORY_BIKE)
                            <i class="material-icons font-32" style="transform: translateY(10px)">directions_bike</i>
                            @else
                            <i class="material-icons font-32" style="transform: translateY(10px)">directions_run</i>
                            @endif
                        {{-- </div> --}}
                    </h2>
                </div>
                <div class="body">
                    @if (session('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session('success')}}</strong>
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-warning alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session('message')}}</strong>
                        </div>
                    @endif
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        @foreach($profiles as $data)
                        <li role="presentation" class="@if($loop->index == 0) active @endif"><a href="#peserta_{{$loop->index}}" data-toggle="tab">PESERTA {{$loop->index+1}}</a></li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($profiles as $data)
                        <div role="tabpanel" class="tab-pane fade @if($loop->index == 0) in active @endif" id="peserta_{{$loop->index}}">
                            <div class="row">
                                <div class="col-md-6 col-md-push-3">
                                    <h4 for="" class="m-b-15 col-blue">Data Profil Peserta {{$loop->index+1}}: {{$data->name}}</h4>
                                    <form id="profile" action="#">
                                        <input type="hidden" name="id" value="{{@$data->id}}">
                                        
                                        <label for="">
                                            Nomor BIB Peserta
                                            {{-- @if($CONF->startChallenge) --}}
                                            <a class="col-blue" target="_blank" href="{{route('user.bib.generate',['token'=>$data->id.'='.\Auth::user()->token]).'='.md5(uniqid(rand(), true))}}">
                                                <i class="fa fa-download"></i>
                                            </a>
                                            {{-- @endif --}}
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">confirmation_number</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control grey-mode" name="bib_number" required autocomplete="off" value="{{ @$data->bib_number }}" disabled>
                                            </div>
                                        </div>

                                        <label for="">Nama Peserta</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="name" placeholder="Contoh : John Doe" required autofocus autocomplete="off" value="{{ @$data->name }}">
                                            </div>
                                        </div>

                                        <label for="">Email</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="email" class="form-control" name="email" placeholder="Contoh : name@email.com" required autocomplete="off" value="{{ @$data->email }}">
                                            </div>
                                        </div>

                                        <label for="">Umur</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="age" placeholder="Contoh: 29" required autocomplete="off" value="{{ @$data->age }}">
                                            </div>
                                        </div>

                                        <label for="">Jenis Kelamin</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">wc</i>
                                            </span>
                                            <select class="form-control show-tick" name="gender" required>
                                                @foreach(\App\Helper\SelectHelper::getGender() as $key => $value)
                                                <option @if(@$data->gender == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="">No HP (WA)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone_android</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="number" class="form-control" name="phone_number" placeholder="Contoh : 088211112222" required autocomplete="off" value="{{ @$data->phone_number }}">
                                            </div>
                                        </div>

                                        <label for="">Alamat Lengkap</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">home</i>
                                            </span>
                                            <div class="form-line">
                                                <textarea class="form-control" name="address" placeholder="Contoh: Jl Kenanga V no 96 Atsiri Permai RT 01 RW 13 Kab Bogor 16431" required autocomplete="off">{{ @$data->address }}</textarea>
                                            </div>
                                            @if(\Auth::user()->include_jersey != \App\Utilities\Constants::COMMON_STATUS_NO && $loop->index == 0)
                                            <small class="col-red">*Alamat ini akan digunakan sebagai alamat pengiriman racepack</small>
                                            @endif
                                        </div>

                                        @if(\Auth::user()->include_jersey != \App\Utilities\Constants::COMMON_STATUS_NO)
                                        <label for="">Ukuran Sersey</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">wc</i>
                                            </span>
                                            <select class="form-control show-tick" name="jersey" required>
                                                <option value="">- Pilih Ukuran Jersey -</option>
                                                @foreach(\App\Helper\SelectHelper::getJerseySize() as $key => $value)
                                                <option @if(@$data->jersey == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <img class="img-responsive m-b-30" src="{{asset('admin/images/jersey_size_chart.jpg')}}">
                                        @endif

                                        {{--
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="text-center">
                                                    <button class="btn btn-block bg-pink waves-effect" type="submit">Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                        --}}
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script>
    $('input,select,textarea').prop('disabled',true).addClass('grey-mode').css('padding-left','5px');
</script>
@endpush
@endsection