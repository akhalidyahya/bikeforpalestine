@extends('layouts.base')
@section('title','Data Submit Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Riwayat Record dan Laporan Record Bermasalah</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Riyawat Record yang Tersimpan</h3>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <i class="fa fa-info-circle fa-lg"></i>
                                <b>Pastikan Record Activity di Strava menggunakan akun strava yang tersinkronisasi dengan web RFP</b>
                            </div>
                            <div class="alert alert-info alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <i class="fa fa-info-circle fa-lg"></i>
                                Apabila ada activity yang tidak tersimpan, silahkan coba re-login (logout, lalu login lagi). Apabila masih belum ada, silahkan lapor activity tersebut dengan tombol 'Sync Manual'.
                            </div>
                            <div class="table-responsive">
                                <table id='myTable' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Tanggal</th>
                                            <th scope="col">TimeZone</th>
                                            <th scope="col">Jarak (KM)</th>
                                            <th scope="col">Waktu</th>
                                            <th scope="col">Activity Strava</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Laporan Record Tidak Tersimpan</h3>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <i class="fa fa-info-circle fa-lg"></i> 
                                Proses verifikasi laporan dilakukan secara manual oleh admin sehingga butuh waktu beberapa saat. Silahkan cek secara berkala.
                                Pastikan menginput link activity yang benar.
                            </div>
                            <div class="text-right">
                                <button type="button" class="btn btn-primary waves-effect btn-sm" onclick="addData()">
                                    <i class="material-icons">cloud_upload</i>
                                    <span>Sync Manual</span>
                                </button>
                            </div>
                            <div class="table-responsive">
                                <table id='myTable2' class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Tanggal Pengajuan</th>
                                            <th scope="col">Activity yang Hilang</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Catatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('user-pages.part.history.modal-add')
@include('user-pages.part.submit.modal-filter-result')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('user.history.historyTableActivity') }}",
            'data': function(d) {
                d.user_id = "{{ \Auth::user()->id }}";
                d.registration_category = "{{ \Auth::user()->registration_category }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'searching': false,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'start_date_local', name: 'start_date_local',orderable:false,searchable:false},
            {data: 'timezone', name: 'timezone',orderable:false,searchable:false},
            {data: 'distance_km_format', name: 'distance_km_format',orderable:false,searchable:false},
            {data: 'moving_time_format', name: 'moving_time_format',orderable:false,searchable:false},
            {data: 'strava_activity_link', name: 'strava_activity_link',orderable:false,searchable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    var table2 = $('#myTable2').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('user.history.complaintDataTable') }}",
            'data': function(d) {
                d.user_id = "{{ \Auth::user()->id }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'searching': false,
        'columns': [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable:false,searchable:false},
            {data: 'created_at', name: 'created_at',orderable:false,searchable:false},
            {data: 'link', name: 'link',orderable:false,searchable:false},
            {data: 'status', name: 'status',orderable:false,searchable:false},
            {data: 'remark', name: 'remark',orderable:false,searchable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.ajax.reload();        
    }

    function addData() {
        $('.modal-title').text('Submit Data');
        $('#myModal form')[0].reset();
        $('.form-line').removeClass('focused');
        $('#myModal').modal('show');
    }

    function saveData() {
        $('#myForm').validate({
            highlight: function(input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function(input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function(error, element) {
                $(element).parents('.input-group').append(error);
            }
        });

        if(!$('[name="link"]').val()) {
            swal({
                title: 'Link Activity Wajib diisi!',
                icon: 'error',
                timer: '3000'
            });
            return '';
        }

        var url = "{{ route('user.history.save') }}";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#myForm').serialize(),
            success: function(data) {
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#myModal').modal('hide');
                        table.ajax.reload();
                        table2.ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                });
            }
        });
    }

    $(function(){
        $('.custom-time').inputmask('**:**:**', { placeholder: 'HH:MM:SS' });
        //Bootstrap datepicker plugin
        $('#custom_bs_datepicker_container input').datepicker({
            autoclose: true,
            container: '#custom_bs_datepicker_container',
            dateFormat: 'yyyy-mm-dd',
            format: 'yyyy-mm-dd',
        });
    })

    function openFilter() {
        $('.modal-title').text('Filter Data');
        $('#filterModal').modal('show');
    }


    
</script>
@endpush
@endsection