@extends('layouts.base')
@section('title','Dasbor')
@push('customCSS')
<style>
    #copy {
        cursor: pointer;
    }

    #bank1 {
        font-size: 1.3em;
        border: none !important;
        outline: none !important;
        background-color: transparent !important;
        font-weight: bolder;
        text-align: center;
        width: 100%;
    }

    input[type=file] {
        background-color: #dedede !important;
        padding: 5px !important;
    }    
</style>
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>Dasbor</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Selamat Datang {{\Auth::user()->name}}!</h2>
                    <small>Status Pembayaran: <b>{!!\App\Utilities\Constants::REGISTRATION_STATUS_COLOR_LIST[\Auth::user()->status_payment]!!}</b></small><br>
                    @if(!empty(@\Auth::user()->resi))
                    <small>Resi Pengiriman Racepack: <b>{!!@\Auth::user()->resi!!}</b></small><br>
                    <small>Ekspedisi: <b><a target="_blank" href="{!!@\Auth::user()->expedition->expedition_tracking_url !!}">{!!@\Auth::user()->expedition->expedition_name !!}</a></b></small> <br>
                    <small>Catatan: <b>{!!@\Auth::user()->delivery_message!!}</b></small><br>
                    @endif
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <div class="alert alert-info alert-block m-t-5">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <i class="fa fa-info-circle fa-lg"></i> 
                                Hai {{\Auth::user()->name}}, selamat atas pencapaianmu di kategori {{@\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[\Auth::user()->registration_category]}}! Silahkan download sertifikatmu <a style="color: darkblue" href="{{route('user.sertif.generate',['token'=>\Auth::user()->profiles[0]->id.'='.\Auth::user()->token]).'='.md5(uniqid(rand(), true))}}">disini</a> yaa!
                            </div>
                            <div class="alert alert-info alert-block m-t-5">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <i class="fa fa-info-circle fa-lg"></i> 
                                Periode challenge telah selesai! Silahkan lihat <a style="color: darkblue" href="{{route('kilasBalik.index')}}">kilas balik</a> kamu selama mengikuti Run and Ride for Palestine 2022.
                            </div>
                            @if(\Auth::user()->status_payment != \App\Utilities\Constants::REGISTRATION_STATUS_PAID)
                            <div class="alert alert-info alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <i class="fa fa-info-circle fa-lg"></i> 
                                Silahkan lengkapi data peserta dibawah ini dan lakukan pembayaran pada langkah akhir (data peserta akan otomatis tersimpan setiap kali berlanjut ke langkah selanjutnya).
                            </div>
                            @include('user-pages.part.dashboard-user.payment-form-wizard')
                            @else
                            <img src="{{asset('assets/images/main-poster.jpg')}}?v=2" alt="" class="img-responsive">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-steps/jquery.steps.js')}}"></script>
<script>
    @if(\Auth::user()->status_pembayaran != \App\Utilities\Constants::REGISTRATION_STATUS_PAID)
    function copy(){
        let text = document.getElementById('bank1');
        console.log(text);
        text.select();
        text.setSelectionRange(0, 99999);

        document.execCommand("copy");
        swal('Nomor rekening tersalin!');
    }

    $('.form-control').css('padding-left','5px');

    function upload() {
        var url = "{{ route('pembayaran.bukti') }}";
        $.ajax({
            url: url,
            type: 'POST',
            data: new FormData($("#myForm")[0]),
            processData: false,
            contentType: false,
            beforeSend: function(e) {
                // showNotification('alert-info', 'Sedang mengupload gambar...', 'top', 'right', null, null);
                swal({text:'Uploading...',button:false})
            },
            success: function(data){
                if(data.success){
                    swal({
                        title: 'Berhasil upload gambar',
                        text: data.message,
                        icon: 'success',
                        timer: '4000'
                    }).then(()=>{
                        $('#statusText').html('{!!\App\Utilities\Constants::REGISTRATION_STATUS_COLOR_LIST[\App\Utilities\Constants::REGISTRATION_STATUS_ON_CONFIRMATION]!!}');
                        $('input[name="bukti_pembayaran"]').val('');
                        $('input[name="bukti_pembayaran"]').prop('disabled',true);
                        $('input[name="bukti_pembayaran"]').addClass('d-none');
                    });
                } else {
                    swal({
                        title: 'Gagal upload gambar',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                });
            }
        });
    }

    $('#wizard_vertical').steps({
        headerTag: 'h2',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        stepsOrientation: 'horizontal',
        enableAllSteps: true,
        enableFinishButton:false,
        labels: {
            finish: "Selesai",
            next: "Selanjutnya",
            previous: "Sebelumnya",
        },
        onInit: function (event, currentIndex) {
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                $('#profile_'+currentIndex).find('.body:eq(' + newIndex + ') label.error').remove();
                $('#profile_'+currentIndex).find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            // form.validate().settings.ignore = ':disabled,:hidden';
            if($('#profile_'+currentIndex).valid()){
                saveData(currentIndex);
                return $('#profile_'+currentIndex).valid();
            }
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        }
    });
    
    function setButtonWavesEffect(event) {
        $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
        $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
    }

    function saveData(index) {
        var url = "{{ route('user.profile.save') }}";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#profile_'+index).serialize(),
            success: function(data) {
                if (data.success) {
                    showNotification('alert-success', data.message, 'bottom', 'right', null, null);
                } else {
                    showNotification('alert-danger', 'Data peserta gagal disimpan! Silahkan hubungi admin!', 'bottom', 'right', null, null);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                });
            }
        });
    }

    $('.validate').validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        },
        // rules: {
        //     password: {
        //         required: true,
        //         minlength: 4
        //     },
        // },
    });
    @endif
</script>
@endpush
@endsection