<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="myForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="user_id" value="{{\Auth::user()->id}}">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <label for="">Kategori Event</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="registration_category" class="form-control grey-mode" autocomplete="off" value="{{\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[\Auth::user()->registration_category]}}" disabled>
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Link Activity</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="link" class="form-control" autocomplete="off" placeholder="https://www.strava.com/activities/6565875631" required>
                                </div>
                                <div id="link_error"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveData()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>