<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="myForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{\Auth::user()->id}}">
                    <input type="hidden" name="result_id" value="">
                    <input type="hidden" name="method" value="">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <label for="">Kategori Event</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="registration_category" class="form-control grey-mode" autocomplete="off" value="{{\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[\Auth::user()->registration_category]}}" disabled>
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Tanggal Aktifitas</label>
                            <div class="form-group">
                                <div class="form-line" id="custom_bs_datepicker_container">
                                    <input type="text" name="activity_date" class="form-control" autocomplete="off" placeholder="Pilih tanggal...">
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Peserta</label>
                            <div class="form-group">
                                <select class="form-control show-tick " name="profile_id">
                                    <option value="">--- Pilih Peserta ---</option>
                                    @foreach($profiles as $profile)
                                    <option value="{{$profile->id}}">{{$profile->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Jarak (KM)</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="distance" class="form-control" autocomplete="off" maxlength="10" placeholder="contoh: 3,52">
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Durasi</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="duration" class="form-control custom-time" autocomplete="off">
                                    <label class="form-label">
                                        contoh: 00:25:47
                                    </label>
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="">Link Strava</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="link" class="form-control" autocomplete="off">
                                </div>
                                <div id="tryout_name_error"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveData()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>