<div class="row">
    <div class="col-md-12">
        <h4 for="" class="m-b-15 col-blue">@if(isset($isPaid)) @if(!$isPaid) Lengkapi @endif @endif Data Profil Peserta: {{@$data->name}}</h4>
        <form id="profile_{{$index}}" class="validate">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{@$data->id}}">
            <input type="hidden" name="json" value="1">
            @if(isset($isPaid)) @if($isPaid)
            <label for="">Nomor BIB Peserta</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">confirmation_number</i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control grey-mode" name="bib_number" required autocomplete="off" value="{{ @$data->bib_number }}" disabled>
                </div>
            </div>
            @endif @endif

            <label for="">Nama Peserta</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="name" placeholder="Contoh : John Doe" required autofocus autocomplete="off" value="{{ @$data->name }}">
                </div>
            </div>

            <label for="">Email</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">email</i>
                </span>
                <div class="form-line">
                    <input type="email" class="form-control" name="email" placeholder="Contoh : name@email.com" required autocomplete="off" value="{{ @$data->email }}">
                </div>
            </div>

            <label for="">Umur</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">today</i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="age" placeholder="Contoh: 29" required autocomplete="off" value="{{ @$data->age }}">
                </div>
            </div>

            <label for="">Jenis Kelamin</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">wc</i>
                </span>
                <select class="form-control show-tick" name="gender" required>
                    @foreach(\App\Helper\SelectHelper::getGender() as $key => $value)
                    <option @if(@$data->gender == $key) selected @endif value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </div>

            <label for="">No HP (WA)</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">phone_android</i>
                </span>
                <div class="form-line">
                    <input type="number" class="form-control" name="phone_number" placeholder="Contoh : 088211112222" required autocomplete="off" value="{{ @$data->phone_number }}">
                </div>
            </div>

            <label for="">Alamat Lengkap</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">home</i>
                </span>
                <div class="form-line">
                    <textarea class="form-control" name="address" placeholder="Contoh: Jl Kenanga V no 96 Atsiri Permai RT 01 RW 13 Kab Bogor 16431" required autocomplete="off">{{ @$data->address }}</textarea>
                </div>
                @if($index == 0 && \Auth::user()->include_jersey != \App\Utilities\Constants::COMMON_STATUS_NO)
                <small class="col-red">*Alamat ini akan digunakan sebagai alamat pengiriman racepack</small>
                @endif
            </div>

            @if(\Auth::user()->include_jersey != \App\Utilities\Constants::COMMON_STATUS_NO)
            <label for="">Ukuran Jersey</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">wc</i>
                </span>
                <select class="form-control show-tick" name="jersey" required>
                    <option value="">- Pilih Ukuran Jersey -</option>
                    @foreach(\App\Helper\SelectHelper::getJerseySize() as $key => $value)
                    <?php
                    $stock = \App\Helper\SelectHelper::getJerseyRemainingStockByKey($key);
                    ?>
                    <option @if(@$data->jersey == $key) selected @endif value="{{$key}}" @if($stock < 1) disabled @endif>{{$value}} (Stok: {{ $stock }})</option>
                    @endforeach
                </select>
            </div>
            <img class="img-responsive m-b-30" src="{{asset('admin/images/jersey_size_chart.jpg')}}">
            @endif

            {{--
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <button class="btn btn-block bg-pink waves-effect" type="submit">Simpan</button>
                    </div>
                </div>
            </div>
            --}}
        </form>
    </div>
</div>