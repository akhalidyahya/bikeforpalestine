<div class="col-md-12 text-center">
    <h4>Yuk, buruan selesaikan pembayaranmu!</h4>    
</div>
<div class="col-md-4 col-md-push-4 text-center">
    <img src="{{asset('assets/images/bsi-logo.png')}}" width="150px">
    <h4 for="">Transfer BSI</h4>
    <div class="m-t-10">
        Nomor Rekening <br>
        <div class="row">
            <div class="col-xs-10">
                <input type="text" id="bank1" value="2020206673" disabled>
            </div>
            <div class="col-xs-2">
                <span id="copy" onclick="copy()" style="margin-left: -40px; font-size:17px"> <i class="fa fa-copy"></i></span>
            </div>
        </div>
        A.n. PEMUDA NUSANTARA JUARA
    </div>
</div>
<div class="col-md-12 m-t-10 text-center">
    Kode Unik <br>
    <b>{{ \App\Utilities\Constants::UNIQUE_CODE_REGISTRATION }}</b>
</div>
<div class="col-md-12 text-center">
    Total Pembayaran <br>
    <b>Rp {{ \App\Helper\SelectHelper::getTicketPrice(\Auth::user(),true,true) }}</b>
</div>
<hr>
<div class="col-md-12 text-center m-t-20">
    <p>Tiket Registrasi: <strong>{{ \App\Utilities\Constants::REGISTRATION_TYPE_LIST[\Auth::user()->registration_type] }}</strong></p>
    <p>Include Racepack: <strong>{{ \App\Utilities\Constants::COMMON_STATUS_BOOLEAN[\Auth::user()->include_jersey] }}</strong></p>
    <p>Status Pembayaran: <b><span id="statusText">{!! \App\Utilities\Constants::REGISTRATION_STATUS_COLOR_LIST[\Auth::user()->status_payment] !!}</span></b></p>
    @if(\Auth::user()->status_payment == \App\Utilities\Constants::REGISTRATION_STATUS_REJECT)
        <p>Catatan: <b><span>{{ \Auth::user()->payment_remark }}</span></b></p>
    @endif

    @if(in_array(\Auth::user()->status_payment,[\App\Utilities\Constants::REGISTRATION_STATUS_UNPAID,\App\Utilities\Constants::REGISTRATION_STATUS_REJECT]))
    <div class="row m-t-30">
        <form id="myForm" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="col-md-4 text-center col-md-push-4">
                <div class="form-group form-float">
                    <label class="form-label">Upload Bukti Bayar</label>
                    <input type="file" name="bukti_pembayaran" class="form-control" value="" onchange="upload()">
                </div>
            </div>
        </form>
    </div>
    @endif
</div>