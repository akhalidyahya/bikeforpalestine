<!-- Basic Example | Vertical Layout -->
<div id="wizard_vertical">
    @foreach($profiles as $profile)
    <h2>Data Peserta</h2>
    <section>
        <p>
            <?php
            $parse = [
                'data' => $profile,
                'index' => $loop->index,
                'isPaid' => \Auth::user()->status_payment == \App\Utilities\Constants::REGISTRATION_STATUS_PAID
            ];
            ?>
            @include('user-pages.part.profile.form-profile',$parse)
        </p>
    </section>
    @endforeach
    <h2>Pembayaran</h2>
    <section>
        <p>
            @include('user-pages.part.dashboard-user.payment-form')
        </p>
    </section>
</div>
<!-- #END# Basic Example | Vertical Layout -->