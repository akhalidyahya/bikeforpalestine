@extends('layouts.base')
@section('title','Leaderboard')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Leaderboard</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h3 class="col-blue">Top 10 Leaderboard {{@\App\Utilities\Constants::REGISTRATION_CATEGORY_LIST[\Auth::user()->registration_category]}}</h3>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($topTen as $profile)
                            <div class="leaderboard-row">
                                <span class="profile-number">{{$loop->index+1}}</span>
                                <span class="profile-image profile-{{$profile->gender}}">{{$profile->initial}}</span>
                                <span class="profile-name">@if(strlen($profile->name) > 9) {{substr($profile->name,0,10)}}... @else {{$profile->name}} @endif</span>
                                <span class="float-right right profile-distance">{{$profile->total_distance_km_format}}KM</span>
                            </div>
                            @endforeach
                            
                            <div class="text-center m-t-25">
                                <a target="_blank" class="btn btn-primary" href="{{route('leaderboard')}}">Selengkapnya</a>
                            </div>

                            <h4 class=" m-t-50">Ranking anda:</h4>
                            @if(!empty($currentUser->total_distance))
                            <div class="leaderboard-row">
                                <span class="profile-number font-bold col-blue">{{@$currentRank}}</span>
                                <span class="profile-image profile-{{$currentUser->gender}}">{{$currentUser->initial}}</span>
                                <span class="profile-name col-blue font-bold">@if(strlen($currentUser->name) > 12) {{substr($currentUser->name,0,13)}}... @else {{$currentUser->name}} @endif</span>
                                <span class="float-right right profile-distance col-blue">{{$currentUser->total_distance_km_format}}KM</span>
                            </div>
                            @else
                            Anda belum memulai challenge
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.user.modal-participant')
@include('pages.part.user.modal-filter-user')
@include('pages.part.user.modal-detail-profile')
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script>
    var table = $('#myTable').DataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': "{{ route('user.leaderboard.dataTableActivity') }}",
            'data': function(d) {
                d.registration_category = "{{ \Auth::user()->registration_category }}";
            }
        },
        'dataType': 'json',
        'paging': true,
        'lengthChange': true,
        'responsive': true,
        'columns': [
            {data: 'DT_RowIndex',name: 'DT_RowIndex',searchable:false,orderable:false},
            {data: 'gender',name: 'gender'},
            {data: 'name',name: 'name'},
            {data: 'total_distance_km_format',name: 'total_distance_km_format',searchable:false,orderable:false},
            {data: 'total_time_time_format',name: 'total_time_time_format',searchable:false,orderable:false},
            {data: 'pace',name: 'pace',searchable:false,orderable:false},
        ],
        'info': true,
        'autoWidth': false
    });

    function filter() {
        table.ajax.reload();
    }

</script>
@endpush
@endsection