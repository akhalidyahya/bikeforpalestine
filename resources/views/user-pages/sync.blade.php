@extends('layouts.base')
@section('title','Data Submit Peserta')
@push('customCSS')
<!-- datatable -->
<link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet" />
<!-- Font Awesome -->
<link href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<style>
    .bukti-img {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>Sync with Strava</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        @if($CONF->startChallenge)
                        <div class="col-md-12">
                            @if(!empty($connected))
                            Berhasil terhubung ke strava!
                            <br>
                            <a class="btn btn-danger waves-effect" href="{{route('user.strava.disconnect')}}"> <i class="fa fa-power-off"></i> Disconnect!</a>
                            @else
                            <a class="btn btn-primary waves-effect" href="{{route('user.strava.auth')}}"> <i class="fa fa-refresh"></i> Sync Now!</a>
                            @endif
                        </div>
                        @else
                        <div class="col-md-12">
                            Sinkron dengan strava akan dibuka saat Technical Meeting tanggal 22 Januari 2022 ya..
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('admin/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
@endpush
@endsection