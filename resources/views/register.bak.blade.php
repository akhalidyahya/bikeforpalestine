﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Register | Run & Bike For Palestine</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="{{url('/')}}">Run & Ride For <b>Palestine</b></a>
            <small>Aplikasi Pendaftaran Online</small>
        </div>
        @if ($errors->has('email'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{route('register.save')}}">
                    {{csrf_field()}}
                    <div class="msg">Pendaftaran Akun!</div>

                    <label for="">Kategori Event</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">directions_run</i>
                        </span>
                        <select class="form-control show-tick" name="registration_category" required>
                            <option value="">- Pilih Kategori Event -</option>
                            @foreach(\App\Helper\SelectHelper::getRegistrationCategory() as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="">Paket Pendaftaran</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">theaters</i>
                        </span>
                        <select class="form-control show-tick" name="registration_type" required>
                            <option value="">- Pilih Paket Pendaftaran -</option>
                            @foreach(\App\Helper\SelectHelper::getRegistrationType() as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="">Include Racepack</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">assignment_ind</i>
                        </span>
                        <select class="form-control show-tick" name="include_jersey" required>
                            @foreach(\App\Helper\SelectHelper::getCommonStatusBooleanList() as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        <small class="col-red">*harga tiket akan menyesuaikan dengan apakah perlu racepack atau tidak</small>
                    </div>

                    <label for="">Nama</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Contoh : John Doe" required autofocus autocomplete="off" value="{{ old('name') }}">
                        </div>
                    </div>

                    <label for="">Email</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Contoh : name@email.com" required autocomplete="off" value="{{old('email')}}">
                        </div>
                    </div>

                    <label for="">Umur</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">today</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="age" placeholder="Contoh: 29" required autocomplete="off" value="{{ old('age') }}">
                        </div>
                    </div>

                    <label for="">Jenis Kelamin</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">wc</i>
                        </span>
                        <select class="form-control show-tick" name="gender" required>
                            @foreach(\App\Helper\SelectHelper::getGender() as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="">No HP (WA)</label>  
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone_android</i>
                        </span>
                        <div class="form-line">
                            <input type="number" class="form-control" name="phone_number" placeholder="Contoh : 088211112222" required autocomplete="off" value="{{ old('phone_number') }}">
                        </div>
                    </div>

                    <label for="">Alamat</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">home</i>
                        </span>
                        <div class="form-line">
                            <textarea class="form-control" name="address" placeholder="Contoh: Jl Kenanga V no 96 Atsiri Permai RT 01 RW 13 Kab Bogor 16431" required autocomplete="off"></textarea>
                        </div>
                    </div>
                    
                    <label for="">Password</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password akun minimal 4 karakter" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Summary</label>
                            <p>Paket Pendaftaran: </p>
                            <p>Racepack: </p>
                            <p>Total harga tiket: </p>
                        </div>
                        <div class="col-xs-12">
                            <div class="text-center">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">Daftar</button>
                            </div>
                        </div>
                    </div>
                    Sudah memiliki akun? <a href={{ route('login.index') }}>Login</a>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>
    <script src="{{asset('admin/js/pages/examples/sign-in.js')}}"></script>
</body>

</html>