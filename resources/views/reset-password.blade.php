﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Reset Password | Run & Ride For Palestine</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <!-- <a href="{{url('/')}}">Run & Ride For <b>Palestine</b></a> -->
            <a href="{{url('/')}}"><img src="{{asset('assets/images/logo-white.png')}}" width="250px" alt="Logo" style="padding-top: 20px;"></a>
            <!-- <small>Aplikasi Pendaftaran Online</small> -->
        </div>
        @error('captcha')
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <span> {{ $message }}. </span>
            </div>
        @enderror
        @if (session('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                {{ session('error') }}
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                {{ session('message') }}
            </div>
        @endif
        <div class="card">
            <div class="body">
                <form id="reset_password" method="POST" action="{{route('resetPassword.process')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{@$id}}">
                    <input type="hidden" name="user_token" value="{{@$user_token}}">
                    <input type="hidden" name="token_id" value="{{@$token_id}}">
                    <h3 class="text-center col-blue m-b-30">Reset Password Akun!</h3>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control grey-mode" name="name" placeholder="Contoh : John Doe" required autofocus autocomplete="off" value="{{ @$email }}" disabled>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" id="password" class="form-control" name="password" placeholder="New Password" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password_confirm" placeholder="New Password Confirm" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="col-md-12 captcha">
                            <span>{!! captcha_img('flat') !!}</span>
                            <button type="button" class="btn btn-primary" class="reload" id="reload">
                            &#x21bb;
                            </button>
                        </div>
                        <div class="col-md-12">
                            <div class="form-line">
                                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">Reset</button>
                            </div>
                        </div>
                    </div>
                    Belum memiliki akun? <a href="{{ 'register' }}">Register </a><br>
                    Sudah memiliki akun? <a href={{ route('login.index') }}>Login</a>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>
    <script src="{{asset('admin/js/pages/examples/sign-in.js')}}"></script>
    <script>
        $("#reload").click(function(){
            $.ajax({
                type:'GET',
                url:'{{route("captcha.reload")}}',
                success:function(data){
                    $(".captcha span").html(data.captcha);
                }
            });
        });
    </script>
</body>

</html>